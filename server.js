const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const passport = require('passport');
const createError = require('http-errors');
const cors = require('cors');
const socketManager = require('./sockets/socketManager');
const path = require('path');

app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

//DB config
const db = require('./config/keys').mongoURI;

mongoose.connect(db, { useNewUrlParser: true, useCreateIndex: true })
        .then( () => console.log('MongoDB connected'))
        .catch( err => console.log(err));

//Pasport
app.use(passport.initialize());
require('./config/passport')(passport);

// ROUTES
app.use(cors());

require('./routes/index')(app);

if (process.env.NODE_ENV.trim() === 'production') {
  app.use(express.static('client/build'));

  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
  });
}

app.use(function(req, res, next) {
  next(createError(404));
});



// error handler
app.use(function(error, req, res, next) {
  res.status(error.status || 500);
  res.json(error);
});

const port = process.env.PORT || 5000;
app.listen(port, () => console.log(`Server is running on port ${port}`));

io.on('connection', socketManager);

server.listen(5080, function(){
  console.log('listening on *:5080');
});

module.exports.io = io;
