import { shallow } from 'enzyme';
import React from 'react';
import ConversationList from '../../components/messages/ConversationList';

describe('Test ConversationList component', () => {
  const mockGetInterlocutor = jest.fn(() => {
    return {
      user: {
        avatar: 'http://'
      },
      personal: {
        firstname: 'test',
        lastname: 'lastname'
      }
    }
  });

  const mockConversations = [
    { _id: 1, messages: [{ avatar: 'http://', content: '1'}]},
    { _id: 2, messages: [{ avatar: 'http://', content: '2'}]},
    { _id: 3, messages: [{ avatar: 'http://', content: '3'}]}
  ];

  const props = {
    getInterlocutor: mockGetInterlocutor,
    conversations: mockConversations
  }

  const component = shallow(<ConversationList { ...props } />)

  it ('renders 3 conversations', () => {
    expect(component.find('.conversations__item')).toHaveLength(3)
  });

  it ('tests lastMessages func', () => {
    expect(component.find('.conversations__last-message')
                    .at(1).text()).toEqual('2')
  });

  it ('should contains fullname', () => {
    expect(component.find('.conversations__header')
                    .at(1).text()).toEqual('Lastname Test')
  });
});
