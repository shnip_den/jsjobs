import { shallow } from 'enzyme';
import React from 'react';
import ChatInput from '../../components/messages/ChatInput';

describe('Test ConversationList component', () => {
  const mocksendMessage = jest.fn((message) => {});

  const props = {
    sendMessage: mocksendMessage
  }

  const initialState = {
    message: ''
  }

  const component = shallow(<ChatInput { ...props } />)

  it ('equals initialState', () => {
    expect(component.state()).toEqual(initialState);
  });

  it ('changes state when typing input', () => {
    component.find('.chat__input').simulate('change', {
      preventDefault: jest.fn(),
      target: {
        value: 'test'
      }
    });

    expect(component.state()).toEqual({ message: 'test' });
  });

  it ('should not call sendMessage when input is empty', () => {
    component.find('.chat__btn').simulate('click', { preventDefault: jest.fn()})

    expect(mocksendMessage).toHaveBeenCalledTimes(0)
  });

  it ('should call sendMessage when have some message', () => {
    component.setState({ message: 'text'});
    component.find('.chat__form').simulate('submit', { preventDefault: jest.fn()})

    expect(mocksendMessage).toHaveBeenCalledTimes(1);
  });

  afterEach(() => {
    mocksendMessage.mockClear();
    component.setState(initialState);
  })
});
