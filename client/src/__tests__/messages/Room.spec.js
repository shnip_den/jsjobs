import { shallow } from 'enzyme';
import React from 'react';
import { Room } from '../../components/messages/Room';

describe('Test Room component', () => {
  const mockGetOwnProfile = jest.fn();

  const props = {
    profile: {
      isLoading: false,
      ownProfile: {
        _id: 1,
        personal: {
          firstname: 'Test',
          lastname: 'test'
        },
        user: {
          avatar: 'http://'
        }
      }
    },
    getOwnProfile: mockGetOwnProfile,
    location: {}
  }

  describe('initial', () => {
    const initialProps = { ...props,
                           profile: { ...props.profile, ownProfile: null }
                          }

    const component = shallow(<Room {...initialProps}/>)

    it ('renders Preloader when ownProfile is not loaded', () => {
      expect(component.find('Preloader')).toHaveLength(1);
    });

    it ('calls getOwnProfile', () => {
      expect(mockGetOwnProfile).toHaveBeenCalledTimes(1);
    });

    afterAll(() => {
      mockGetOwnProfile.mockClear();
    });
  });
  describe('when ownProfile is loaded', () => {
    const component = shallow(<Room {...props} />);

    it ('renders ConversationList compnent', () => {
      expect(component.find('ConversationList')).toHaveLength(1);
    });

    it ('renders ChatHeader compnent', () => {
      expect(component.find('ChatHeader')).toHaveLength(1);
    });

    it ('renders ChatContent compnent', () => {
      expect(component.find('ChatContent')).toHaveLength(1);
    });

    it ('renders ChatInput compnent', () => {
      expect(component.find('ChatInput')).toHaveLength(1);
    });
  })

})
