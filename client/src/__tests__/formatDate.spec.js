import formatDate from '../utils/formatDate';

describe('Test formatDate function', () => {
  it ('gets date and returns string', () => {
    const date = new Date('02-01-1999');
    const expected = 'February 1999';
    expect(formatDate(date)).toEqual(expected);
  });

  it ('gets string date and returns string', () => {
    const date = '02-01-1999';
    const expected = 'February 1999';
    expect(formatDate(date)).toEqual(expected);
  });

  it('returns new string date when undefined/null passed to func', () => {
    const date = new Date();
    let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    let month = months[date.getMonth()];
    const expected = `${month} ${date.getFullYear()}`
    expect(formatDate(undefined)).toEqual(expected);
  });
});
