import React from 'react';
import { shallow } from 'enzyme';
import ApplicationJobList from '../../components/Job/ApplicationJobList';

class Job {
  constructor(id) {
    this._id = id,
    this.title = 'JobTest',
    this.created_at = '02-11-2019',
    this.qualification = 'Junior',
    this.skills = [],
    this.location = 'Moscow'
  }
}

const jobs = [];

for (let i = 0; i < 3; i++) {
  let job = new Job(i);
  jobs.push(job);
}

const mockCancelApplication = jest.fn();

const props = {
  jobs,
  cancelApplication: mockCancelApplication
}

describe('Test ApplicationJobList component', () => {
  const component = shallow(<ApplicationJobList { ...props }/>);

  it ('renders 3 jobs', () => {
    expect(component.find('.job__item')).toHaveLength(3);
  })

  it ('calls cancelApplication', () => {
    component.find("[data-id=1]").simulate('click', {});
    expect(mockCancelApplication).toHaveBeenCalledTimes(1)
  })
})
