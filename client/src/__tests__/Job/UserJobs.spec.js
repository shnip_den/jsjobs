import React from 'react';
import { shallow } from 'enzyme';
import { UserJobs } from '../../components/Job/UserJobs';

describe('Test Jobs component', () => {
  const props = {
    user: {
      id: 1
    },
    profile_id: "1"
  }

  const initialState = {
    jobs: null,
    errors: {}
  }

  const component = shallow(<UserJobs { ...props }/>)

  it ('renders message when jobs are not loaded', () => {
    expect(component.find('.profile__user-info').text()).toEqual("User haven't any jobs")
  })

  it ('renders message when jobs are empty array', () => {
    component.setState({ jobs: [] });
    expect(component.find('.profile__user-info').text()).toEqual("User haven't any jobs")
  })

  it ('renders JobList', () => {
    component.setState({ jobs: [{_id: 1}, {_id: 2}] });
    expect(component.find('JobList')).toHaveLength(1);
  })

  afterEach(() => {
    component.setState(initialState);
  })
});
