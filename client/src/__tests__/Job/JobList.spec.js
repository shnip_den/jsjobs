import React from 'react';
import { shallow } from 'enzyme';
import JobList from '../../components/Job/JobList';

class Job {
  constructor(id) {
    this._id = id,
    this.title = 'JobTest',
    this.created_at = '02-11-2019',
    this.qualification = 'Junior',
    this.skills = [],
    this.location = 'Moscow',
    this.user = {
      _id: 2
    }
  }
}

const jobs = [];

for (let i = 0; i < 3; i++) {
  let job = new Job(i);
  jobs.push(job);
}

const mockHandleDelete = jest.fn();

const props = {
  jobs,
  user: {
    id: 1
  },
  editable: false,
  handleDelete: mockHandleDelete
}

describe('Test JobList component', () => {
  const component = shallow(<JobList { ...props }/>);

  it ('renders 3 jobs', () => {
    expect(component.find('.job__item')).toHaveLength(3);
  })

  it ('doesnt render Delete button when editable is false and job doesnt belongs to current user', () => {
    //3 title 3 view link
    expect(component.find('Link')).toHaveLength(6);
    expect(component.find('[data-id]')).toHaveLength(0);
  })

  describe('when editable is true and job belongs to current user', () => {
    beforeEach(() => {
      component.setProps({ editable: true, user: { id: 2 } });
    })

    it ('renders Delete buttons', () => {
      expect(component.find('Link')).toHaveLength(9);
      expect(component.find('[data-id]')).toHaveLength(3);
    });

    it ('calls handleDelete', () => {
      component.find("[data-id=1]").simulate('click', {});
      expect(mockHandleDelete).toHaveBeenCalledTimes(1)
    });

    afterAll(() => {
      component.setProps(props);
    })
  })
})
