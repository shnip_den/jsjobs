import React from 'react';
import { shallow } from 'enzyme';
import { JobEdit } from '../../components/Job/JobEdit';

describe('Test JobEdit component', () => {
  const props = {
    location: {
      query: {
        id: 1
      }
    }
  }

  const invalidJob = {
    title: '',
    qualification: '',
    skills: [],
    description: '',
    location: ''
  }

  const initialState = {
    job: invalidJob,
    errors: {}
  }

  const component = shallow(<JobEdit { ...props } />);

  it ('renders Preloader when job is null', () => {
    component.setState({ job: null });
    expect(component.find('Preloader')).toHaveLength(1);
  })

  it ('should set errors when called submit with invalid data', () => {
    component.find('form.job__form').simulate('submit', { preventDefault: jest.fn() });
    expect(Object.keys(component.state().errors).length > 0).toBeTruthy();
  })

  it ('renders FormJob', () => {
    expect(component.find('FormJob')).toHaveLength(1);
  })

  afterEach(() => {
    component.setState(initialState)
  })
})
