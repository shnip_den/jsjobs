import React from 'react';
import { shallow } from 'enzyme';
import { JobCreate } from '../../components/Job/JobCreate';

describe('Test JobCreate component', () => {
  const mockGetOwnProfile = jest.fn();

  const props = {
    getOwnProfile: mockGetOwnProfile ,
    ownProfile: { _id: 1 }
  }

  const initialState = {
    job: {
      title: '',
      qualification: '',
      skills: [],
      description: '',
      location: ''
    },
    errors: {}
  }

  const component = shallow(<JobCreate { ...props } />);

  it ('renders FormJob', () => {
    expect(component.find('FormJob')).toHaveLength(1);
  })

  it ('should set errors when called submit with invalid data', () => {
    component.find('form.job__form').simulate('submit', { preventDefault: jest.fn() });
    expect(Object.keys(component.state().errors).length > 0).toBeTruthy();
  })

  describe('when ownProfile is null', () => {
    const newProps = { ...props, ownProfile: null }
    const component = shallow(<JobCreate {...newProps} />);

    it ('calls getOwnProfile when ownProfile is null', () => {
      expect(mockGetOwnProfile).toHaveBeenCalledTimes(1);
    })

    it ('calls getOwnProfile when ownProfile is null', () => {
      expect(component.find('Preloader')).toHaveLength(1);
    })
  });

})
