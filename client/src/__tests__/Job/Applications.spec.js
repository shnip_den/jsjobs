import React from 'react';
import { shallow } from 'enzyme';
import { Applications } from '../../components/Job/Applications';


describe('Test Applications component', () => {
  const mockGetOwnProfile = jest.fn();

  const props = {
    getOwnProfile: mockGetOwnProfile ,
    ownProfile: { _id: 1 }
  }

  const initialState = {
    jobs: null,
    errors: {}
  }

  const component = shallow(<Applications { ...props } />);

  it ('renders Preloader when jobs equal null', () => {
    expect(component.find('Preloader')).toHaveLength(1);
  })

  it ('calls getOwnProfile when ownProfile is null', () => {
    const newProps = { ...props, ownProfile: null }
    const component = shallow(<Applications {...newProps} />);
    expect(mockGetOwnProfile).toHaveBeenCalledTimes(1);
  })

  it ('renders ApplicationJobList when jobs are not null', () => {
    const jobs = [{_id: 1}];
    component.setState({ jobs });
    expect(component.find('ApplicationJobList')).toHaveLength(1);
  })
})
