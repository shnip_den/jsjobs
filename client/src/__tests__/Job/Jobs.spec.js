import React from 'react';
import { shallow } from 'enzyme';
import { Jobs } from '../../components/Job/Jobs';

describe('Test Jobs component', () => {
  const props = {
    user: {
      id: 1
    }
  }

  const initialState = {
    jobs: null,
    search: '',
    errors: {}
  }

  const component = shallow(<Jobs { ...props }/>);

  it ('equals initial state', () => {
    expect(component.state()).toEqual(initialState);
  })

  it ('renders Preloader when jobs is null', () => {
    expect(component.find('Preloader')).toHaveLength(1);
  })

  describe('when jobs are loaded', () => {
    beforeAll(() => {
      component.setState({ jobs: [{_id: 1}] });
    })

    it ('renders SearchForm', () => {
      expect(component.find('SearchForm')).toHaveLength(1);
    });

    it ('renders JobList', () => {
      expect(component.find('JobList')).toHaveLength(1);
    });

    afterAll(() => {
      component.setState(initialState);
    })
  })
});
