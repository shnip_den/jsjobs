import React from 'react';
import { shallow } from 'enzyme';
import { Job } from '../../components/Job/Job';

describe('Test Job component', () => {
  const mockGetOwnProfile = jest.fn();
  const props = {
    user: {id: 2, avatar: "http:/"},
    ownProfile: { _id: 1 },
    getOwnProfile: mockGetOwnProfile,
    history: {
      goBack: jest.fn()
    },
    computedMatch: {
      params: {
        id: 1
      }
    }
  }

  const invalidJob = {
    title: '',
    qualification: '',
    skills: [],
    description: '',
    location: '',
    created_at: '',
    applications: [{ profile: { _id: 1} }],
    user: 2
  }

  const initialState = {
    job: invalidJob,
    errors: {}
  }

  const component = shallow(<Job { ...props }/>)

  it ('renders Preloader when jobs equal null', () => {
    expect(component.find('Preloader')).toHaveLength(1);
  });

  it ('calls getOwnProfile when ownProfile is null', () => {
    const newProps = { ...props, ownProfile: null }
    const component = shallow(<Job { ...newProps }/>);
    expect(mockGetOwnProfile).toHaveBeenCalledTimes(1);
  });

  describe('when props user id equals current user id', () => {
    const component = shallow(<Job { ...props }/>);
    component.setState(initialState);

    it ('renders ApplicationList', () => {
      expect(component.find('ApplicationList')).toHaveLength(1);
    });

    it ('renders Edit button', () => {
      expect(component.find('button.btn').at(1).text()).toEqual('Edit');
    });
  });

  describe('test isApplied function', () => {
    const newProps = {}
    const component = shallow(<Job { ...props }/>);
    component.setState({ ...initialState,
                          job: { ...initialState.job, user: 4 }
                      });
    it ('renders Cancel button', () => {
      expect(component.find('button.btn').at(1).text()).toEqual('Cancel');
    });

    it ('renders Apply for job button', () => {
      component.setProps({ ownProfile: { _id: 2} });
      expect(component.find('button.btn').at(1).text()).toEqual('Apply for job');
    });
  });

});
