import React from 'react';
import { shallow } from 'enzyme';
import ApplicationList from '../../components/Job/ApplicationList';

class Application {
  constructor(id) {
    this._id = id,
    this.profile = {
      user: {
        avatar: 'http://'
      },
      handle: 'test',
      personal: {
        firstname: 'First',
        lastname: 'Last',
        location: 'Moscow'
      },
      specialization: {
        skills: []
      }
    }
  }
}

const applications = [];

for (let i = 0; i < 3; i++) {
  let app = new Application(i);
  applications.push(app);
}

const mockHandleDelete = jest.fn();

const props = {
  applications,
  handleDelete: mockHandleDelete
}

describe('Test ApplicationList component', () => {
  const component = shallow(<ApplicationList { ...props }/>);

  it ('renders 3 applications', () => {
    expect(component.find('.job__application')).toHaveLength(3);
  })

  it ('calls handleDelete', () => {
    component.find("[data-id=1]").simulate('click', {});
    expect(mockHandleDelete).toHaveBeenCalledTimes(1)
  })
})
