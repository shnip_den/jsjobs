import { shallow } from 'enzyme'
import React from 'react';
import Homepage from '../components/static_pages/homepage';

describe('Test Homepage', () => {
  it('renders properly', () => {
    const renderedValue = shallow(<Homepage />);
    expect(renderedValue).toMatchSnapshot();
  });
});
