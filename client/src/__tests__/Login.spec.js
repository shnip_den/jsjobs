import React from 'react';
import { Login } from '../components/auth/login';
import { shallow } from 'enzyme';

describe('Test Login component', () => {
  const mockLoginUser = jest.fn();
  const props = {
    loginUser: mockLoginUser,
    auth: {
      isAuthenticated: false,
      user: {}
    },
    errors: {},
    clearReduxErrors: jest.fn()
  }

  const initialState = {
    email: '',
    password: '',
    errors: {}
  }

  const component = shallow(<Login { ...props } />);

  it ('should equal initialState', () => {
    expect(component.state()).toEqual(initialState);
  });


  describe('should change state', () => {
    it ('should change email', () => {
      component.find('#email').simulate('change', {
        preventDefault: () => {},
        target: {
          id: 'email',
          value: 'test@example.com'
        }
      })

      expect(component.state().email).toEqual('test@example.com');
    });

    it ('should change password', () => {
      component.find('#password').simulate('change', {
        preventDefault: () => {},
        target: {
          id: 'password',
          value: 'foobar'
        }
      })

      expect(component.state().password).toEqual('foobar');
    });

    afterEach(() => {
      component.setState(initialState);
    });
  });

  describe('should call loginUser', () => {
    it ('should called from form submit', () => {
      component.find('form').simulate('submit', {
        preventDefault: () => {}
      });

      expect(mockLoginUser).toHaveBeenCalledTimes(1);
    });

    afterEach(() => {
      mockLoginUser.mockClear();
    })
  })

  it ('renders properly', () => {
    expect(component).toMatchSnapshot();
  });
});
