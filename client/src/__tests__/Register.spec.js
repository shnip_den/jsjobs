import React from 'react';
import { shallow } from 'enzyme';
import { Register } from '../components/auth/register';

describe('test', () => {
  const initialState = {
    email: '',
    password: '',
    password_confirmation: '',
    errors: {}
  }
  const mockRegister = jest.fn();
  const props = {
    registerUser: mockRegister,
    auth: {
      isAuthenticated: false,
      user: {}
    },
    errors: {},
    clearReduxErrors: jest.fn()
  }

  const component = shallow(<Register {...props}/>);

  it ('should equal initialState', () => {
    expect(component.state()).toEqual(initialState);
  });

  describe('should call registerUser', () => {
    it ('should called from form submit', () => {
      component.find('form').simulate('submit', {
        preventDefault: () => {}
      });

      expect(mockRegister).toHaveBeenCalledTimes(1);
    });

    afterEach(() => {
      mockRegister.mockClear();
    })
  })

  describe('should change state', () => {
    it ('should change email', () => {
      component.find('#email').simulate('change', {
        preventDefault: () => {},
        target: {
          name: 'email',
          value: 'test@example.com'
        }
      })

      expect(component.state().email).toEqual('test@example.com');
    });

    it ('should change password', () => {
      component.find('#password').simulate('change', {
        preventDefault: () => {},
        target: {
          name: 'password',
          value: 'foobar'
        }
      })

      expect(component.state().password).toEqual('foobar');
    });

    it ('should change password_confirmation', () => {
      component.find('#password_confirmation').simulate('change', {
        preventDefault: () => {},
        target: {
          name: 'password_confirmation',
          value: 'foobar'
        }
      })

      expect(component.state().password_confirmation).toEqual('foobar');
    });

    afterEach(() => {
      component.setState(initialState);
    });
  });

  it ('renders properly', () => {
    expect(component).toMatchSnapshot();
  });
});
