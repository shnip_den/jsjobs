import axios from 'axios';
import setAuthToken from '../utils/setAuthToken';

describe('setAuthToken function', () => {
  beforeEach(() => {
    const token = "some token";
    setAuthToken(token);
  });

  it('sets token to axios', () => {
    expect(axios.defaults.headers.common['Authorization']).toEqual("some token");
  });

  it('removes token from axios', () => {
    setAuthToken(false);
    expect(axios.defaults.headers.common['Authorization']).toBeUndefined();
  });
});
