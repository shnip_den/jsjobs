import authReducer from '../../redux/reducers/authReducer';
import { SET_CURRENT_USER } from '../../redux/actions/types';

describe('Test authReducer', () => {
  const initialState = {
    isAuthenticated: false,
    user: {}
  }

  it('SET_CURRENT_USER', () => {
    const action = {
        type: SET_CURRENT_USER,
        payload: { id: 1 }
    }

    const state = authReducer(initialState, action);
    expect(state).toEqual({
      isAuthenticated: true,
      user: { id: 1 }
    });
  });

  it('returns default state', () => {
    const state = authReducer(initialState, { type: "default" });
    expect(state).toEqual({
      isAuthenticated: false,
      user: {}
    });
  });
});
