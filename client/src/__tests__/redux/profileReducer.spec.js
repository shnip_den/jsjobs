import profileReducer from '../../redux/reducers/profileReducer';
import {  SET_CURRENT_PROFILE,
          UPDATE_PROFILE,
          PROFILE_LOADING,
          CLEAR_ALL_PROFILES,
          PROFILE_ERROR,
          GET_PROFILES,
          SET_PROFILES } from '../../redux/actions/types';

describe('Test profile reducer', () => {
  const initialState = {
    ownProfile: null,
    current: null,
    profiles: null,
    isLoading: false
  };

  it ('tests SET_CURRENT_PROFILE', () => {
    const action = {
      type: SET_CURRENT_PROFILE,
      payload: { profile: 'somedata'}
    }

    const state = profileReducer(initialState, action);
    expect(state).toEqual({
      ownProfile: null,
      current: { profile: 'somedata'},
      profiles: null,
      isLoading: false
    })
  });

  it ('tests UPDATE_PROFILE', () => {
    const action = {
      type: UPDATE_PROFILE,
      payload: { profile: 'somedata'}
    }

    const state = profileReducer(initialState, action);
    expect(state).toEqual({
      ownProfile: { profile: 'somedata'},
      current: null,
      profiles: null,
      isLoading: false
    })
  });

  it ('tests PROFILE_LOADING', () => {
    const action = {
      type: PROFILE_LOADING
    }

    const state = profileReducer(initialState, action);
    expect(state).toEqual({
      ownProfile: null,
      current: null,
      profiles: null,
      isLoading: true
    })
  });

  it ('tests CLEAR_ALL_PROFILES', () => {
    const action = {
      type: CLEAR_ALL_PROFILES
    }

    const state = profileReducer(initialState, action);
    expect(state).toEqual({
      ownProfile: null,
      current: null,
      profiles: null,
      isLoading: false
    })
  });

  it ('tests PROFILE_ERROR', () => {
    const action = {
      type: PROFILE_ERROR
    }

    const state = profileReducer(initialState, action);
    expect(state).toEqual({
      ownProfile: null,
      current: null,
      profiles: null,
      isLoading: false
    })
  });

  it ('returns default state', () => {
    const action = { type: 'default'};

    const state = profileReducer(initialState, action);
    expect(state).toEqual({
      ownProfile: null,
      current: null,
      profiles: null,
      isLoading: false
    })
  });

  it ('tests GET_PROFILES', () => {
    const action = { type: GET_PROFILES };

    const state = profileReducer(initialState, action);

    expect(state).toEqual({
      ...initialState,
      isLoading: true
    })
  });

  it ('tests SET_PROFILES', () => {
    const action = { type: SET_PROFILES, payload: ["profiles"] };

    const state = profileReducer(initialState, action);

    expect(state).toEqual({
      ...initialState,
      isLoading: false,
      profiles: ["profiles"]
    })
  })
});
