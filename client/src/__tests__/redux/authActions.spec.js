import {
  registerUser,
  loginUser,
  setCurrentUser,
  logoutUser
} from "../../redux/actions/authActions";
import { SET_CURRENT_USER } from '../../redux/actions/types';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import jwt from 'jsonwebtoken';
import jwt_decode from 'jwt-decode';
const axios = require('axios');
const MockAdapter = require('axios-mock-adapter');
const axiosMock = new MockAdapter(axios);
const mockStore = configureMockStore([ thunk ]);

describe("Test auth actions", () => {
  it ('tests currentUser action', () => {
    const expectedAction = {
      type: SET_CURRENT_USER,
      payload: { _id: 1 }
    }

    expect(setCurrentUser({ _id: 1 })).toEqual(expectedAction);
  });

  it ('test logoutUser action', ()=>{
    const store = mockStore({ auth: { user: "somedata"} });
    const expectedAction = [{
      type: SET_CURRENT_USER,
      payload: {}
    }];
    localStorage.setItem('jwt', 'sometoken');
    store.dispatch(logoutUser());

    expect(localStorage.getItem('jwt')).toBeNull();
    expect(store.getActions()).toEqual(expectedAction);
  });

  it ('test loginUser action', async () => {
    const token = jwt.sign({ _id: 1, firstname: "test" },
                            "test",
                            {expiresIn: "1d"}
                          );
    const decoded = jwt_decode(token);
    const expectedActions = [{
      type: "SET_CURRENT_USER",
      payload: decoded
    }];

    axiosMock.onPost('/api/login').reply(200, { token });
    const store = mockStore({ auth: null });

    await store.dispatch(loginUser("Somedata"));
    expect(store.getActions()).toEqual(expectedActions);
  });

  afterEach(() => {
    axiosMock.reset();
  })
});
