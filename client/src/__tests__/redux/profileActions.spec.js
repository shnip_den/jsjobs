import {  GET_ERRORS,
          GET_CURRENT_PROFILE,
          GET_OWN_PROFILE,
          PROFILE_LOADING,
          CLEAR_ALL_PROFILES,
          SET_CURRENT_PROFILE,
          UPDATE_PROFILE,
          UPDATE_PERSONAL_PROFILE,
          UPDATE_ADDITIONAL_INFO_PROFILE,
          UPDATE_SPECIALIZATION_PROFILE,
          UPDATE_EXPERIANCE_PROFILE,
          UPDATE_EDUCATION_PROFILE,
          UPDATE_CONTACTS_PROFILE,
          PROFILE_ERROR,
          GET_PROFILES,
          SET_PROFILES,
          SET_OWN_PROFILE
        } from '../../redux/actions/types';

import {  createProfile,
          getCurrentProfile,
          updatePersonal,
          updateAdditionalInfo,
          updateSpecialization,
          updateExperiance,
          updateEducation,
          updateContacts,
          profileLoading,
          clearAllProfiles,
          setProfile,
          updateProfile,
          setProfiles,
          getProfiles } from '../../redux/actions/profileActions';

import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
const mockStore = configureMockStore([ thunk ]);
const MockAdapter = require('axios-mock-adapter');
const axios = require('axios');
const axiosMock = new MockAdapter(axios);


describe('Test profile actions', () => {
  const store = mockStore({ profile: {} });

  it ('tests createProfile', async () => {
    const expectedActions = [{
      type: SET_OWN_PROFILE,
      payload: {profile: "somedata"}
    }];
    const historyMock = { push: jest.fn() };

    axiosMock.onPost('/api/profile').reply(200, {profile: "somedata"});
    await store.dispatch(createProfile({personal: 'somedata'}, historyMock));

    expect(store.getActions()).toEqual(expectedActions);
    expect(historyMock.push).toHaveBeenCalledTimes(1);
  });

  it ('tests getCurrentProfile', async () => {
    const expectedActions = [
      { type: GET_CURRENT_PROFILE },
      { type: PROFILE_LOADING },
      { type: SET_CURRENT_PROFILE, payload: {data: "somedata"} }
    ];

    axiosMock.onGet('/api/profile').reply(200, {data: "somedata"});
    await store.dispatch(getCurrentProfile());

    expect(store.getActions()).toEqual(expectedActions);
  });

  it ('tests updatePersonal', async () => {
    const expectedActions = [
      { type: UPDATE_PERSONAL_PROFILE },
      { type: PROFILE_LOADING },
      { type: UPDATE_PROFILE, payload: {profile: "data"} }
    ];

    axiosMock.onPost('/api/profile/personal').reply(200, {profile: "data"});
    await store.dispatch(updatePersonal({personal: 'somedata'}));

    expect(store.getActions()).toEqual(expectedActions);
  });

  it ('tests updateAdditionalInfo', async () => {
    const expectedActions = [
      { type: UPDATE_ADDITIONAL_INFO_PROFILE },
      { type: PROFILE_LOADING },
      { type: UPDATE_PROFILE, payload: {profile: "data"} }
    ];

    axiosMock.onPost('/api/profile/additionalInfo').reply(200, {profile: "data"});
    await store.dispatch(updateAdditionalInfo({addInfo: 'somedata'}));

    expect(store.getActions()).toEqual(expectedActions);
  });

  it ('tests updateSpecialization', async () => {
    const expectedActions = [
      { type: UPDATE_SPECIALIZATION_PROFILE },
      { type: PROFILE_LOADING },
      { type: UPDATE_PROFILE, payload: {profile: "data"} }
    ];

    axiosMock.onPost('/api/profile/specialization').reply(200, {profile: "data"});
    await store.dispatch(updateSpecialization({specialization: 'somedata'}));

    expect(store.getActions()).toEqual(expectedActions);
  });

  it ('tests updateExperiance', async () => {
    const expectedActions = [
      { type: UPDATE_EXPERIANCE_PROFILE },
      { type: PROFILE_LOADING },
      { type: UPDATE_PROFILE, payload: {profile: "data"} }
    ];

    axiosMock.onPost('/api/profile/experiance').reply(200, {profile: "data"});
    await store.dispatch(updateExperiance({experiance: 'somedata'}));

    expect(store.getActions()).toEqual(expectedActions);
  });

  it ('tests updateEducation', async () => {
    const expectedActions = [
      { type: UPDATE_EDUCATION_PROFILE },
      { type: PROFILE_LOADING },
      { type: UPDATE_PROFILE, payload: {profile: "data"} }
    ];

    axiosMock.onPost('/api/profile/education').reply(200, {profile: "data"});
    await store.dispatch(updateEducation({education: 'somedata'}));

    expect(store.getActions()).toEqual(expectedActions);
  });

  it ('tests updateContacts', async () => {
    const expectedActions = [
      { type: UPDATE_CONTACTS_PROFILE },
      { type: PROFILE_LOADING },
      { type: UPDATE_PROFILE, payload: {profile: "data"} }
    ];

    axiosMock.onPost('/api/profile/contacts').reply(200, {profile: "data"});
    await store.dispatch(updateContacts({contacts: 'somedata'}));

    expect(store.getActions()).toEqual(expectedActions);
  });

  it ('tests profileLoading', () => {
    const expectedAction = {
      type: PROFILE_LOADING
    }

    expect(profileLoading()).toEqual(expectedAction);
  });

  it ('tests clearAllProfiles', () => {
    const expectedAction = {
      type: CLEAR_ALL_PROFILES
    }

    expect(clearAllProfiles()).toEqual(expectedAction);
  });

  it ('tests setProfile', () => {
    const expectedAction = {
      type: SET_CURRENT_PROFILE,
      payload: { profile: "somedata"}
    }

    expect(setProfile({ profile: "somedata"})).toEqual(expectedAction);
  });

  it ('tests updateProfile', () => {
    const expectedAction = {
      type: UPDATE_PROFILE,
      payload: { profile: "somedata"}
    }

    expect(updateProfile({ profile: "somedata"})).toEqual(expectedAction);
  });

  it ('tests setProfiles', () => {
    const expectedAction = {
      type: SET_PROFILES,
      payload: [{profile: "1"}, {profile: "2"}]
    }

    expect(setProfiles([{profile: "1"}, {profile: "2"}])).toEqual(expectedAction);
  });


  it ('tests getProfiles', async () => {
    const data = [{profile: "1"}, {profile: "2"}];
    const expectedActions = [
      { type: GET_PROFILES },
      { type: SET_PROFILES, payload: data }
    ];

    axiosMock.onGet('/api/profile/all').reply(200, data);
    await store.dispatch(getProfiles());

    expect(store.getActions()).toEqual(expectedActions);
  });

  afterEach(() => {
    store.clearActions();
  })
})
