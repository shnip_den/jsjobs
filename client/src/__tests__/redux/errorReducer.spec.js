import errorReducer from '../../redux/reducers/errorReducer';
import { GET_ERRORS } from '../../redux/actions/types';

describe('Test errorReducer', () => {
  it('GET_ERRORS', () => {
    const initialState = {};
    const action = {
      type: GET_ERRORS,
      payload: { err_type: "some message" }
    }
    const state = errorReducer(initialState, action);
    expect(state).toEqual({ err_type: "some message" });
  });
})
