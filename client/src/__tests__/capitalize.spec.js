import capitalize from '../utils/capitalize';

it ('return capitalized string', () => {
  const expectedString = 'Hello';

  expect(capitalize('hello')).toEqual(expectedString);
})
