import React from 'react';
import { shallow } from 'enzyme';
import Contacts from '../../../components/profile/steps/contacts';


describe('Test Contacts', () => {
  const mockChange = jest.fn();
  const props = {
    contacts: {
      phone: '',
      email: '',
      twitter: '',
      instagram: '',
      facebook: '',
      github: ''
    },
    errors: {},
    onChange: mockChange
  }

  const component = shallow(<Contacts {...props} />);


  it('should initial render', () => {
    expect(component.find('h2').text()).toEqual('Contacts');
    expect(component.find('Field')).toHaveLength(6);
  });

  it ('should call onChange', () => {
    component.find('#phone').simulate('change', {
      preventDefault: () => {},
      target: {
        name: 'phone',
        value: '777'
      }
    });

    expect(mockChange).toBeCalled();
  });

  it('renders properly', () => {
    expect(component).toMatchSnapshot();
  })
});
