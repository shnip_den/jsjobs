import { Personal } from '../../../components/profile/steps/personal';
import React from 'react';
import { shallow } from 'enzyme';

describe('Test Personal component', () => {
  const mockChange = jest.fn();
  const mockhandleChangeDate = jest.fn();
  const mockConvertToDate = jest.fn();

  const props = {
    personal: {
      firstname: '',
      lastname: '',
      gender: "male",
      birthday: new Date(),
    },
    onChange: mockChange,
    handleChangeDate: mockhandleChangeDate,
    errors: {},
    convertToDate: mockConvertToDate
  }

  const component = shallow(<Personal {...props} />);

  it('called onChange func', () => {
    component.find('#firstname').simulate('change', {
      preventDefault: () => {},
      target: {
        name: 'firstname',
        value: 'John'
      }
    });

    expect(mockChange).toHaveBeenCalledTimes(1);
  });

  describe('when birthday input changed', () => {
    component.find('#birthday').simulate('change', {
      preventDefault: () => {},
      target: {
        name: 'birthday',
        value: new Date()
      }
    });

    it('called handleChangeDate func', () => {
      expect(mockhandleChangeDate).toHaveBeenCalledTimes(1);
    });

    it('called ConvertToDate', () => {
      expect(mockConvertToDate).toHaveBeenCalledTimes(1);
    });
  });

  afterAll(() => {
    mockChange.mockClear();
    mockhandleChangeDate.mockClear();
    mockConvertToDate.mockClear();
  });

  it('renders properly', () => {
    expect(component).toMatchSnapshot();
  })
});
