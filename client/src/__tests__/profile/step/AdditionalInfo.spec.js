import { AdditionalInfo } from '../../../components/profile/steps/additional-information';
import { shallow } from 'enzyme';
import React from 'react';

describe('Test additional information component', () => {
  const mockChange = jest.fn();
  const props = {
    handle: '',
    account_type: 'personal',
    errors: {},
    onChange: mockChange
  }

  const component = shallow(<AdditionalInfo {...props}/>);

  it ('should call onChange', () => {
    component.find('#handle').simulate('change', {
      target: {
        name: 'field',
        value: 'value'
      }
    })
    expect(mockChange).toBeCalled();
  });

  it ('renders properly', () => {
    expect(component).toMatchSnapshot()
  });
})
