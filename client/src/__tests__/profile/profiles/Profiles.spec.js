import { Profiles } from '../../../components/profile/profiles/Profiles';
import { shallow } from 'enzyme';
import React from 'react';

describe('Test Profiles', () => {
  const mockGetProfiles = jest.fn();
  const props = {
    profiles: [
      { personal: { firstname: 'John', lastname: 'Connor', bio: 'bio'},
        handle: 'john'
      }
    ],
    isLoading: false,
    getProfiles: mockGetProfiles
  }

  const initialState = {
    filterText: ''
  }

  describe('Render Initial', () => {
    const newProps = { ...props, profiles: null }
    const component = shallow(<Profiles {...newProps} />);

    it ('state equals initialState', () => {
      expect(component.state()).toEqual(initialState);
    });

    it ('shows Preloader when profiles not loaded', () => {
      expect(component.find('Preloader')).toHaveLength(1);
    });

    mockGetProfiles.mockClear();
  });

  describe('when profiles is loaded', () => {
    const component = shallow(<Profiles {...props} />);

    it ('calls getProfiles', () => {
      expect(mockGetProfiles).toHaveBeenCalledTimes(1);
    });

    it ('renders SearchForm', () => {
      expect(component.find('SearchForm')).toHaveLength(1);
    });

    it ('renders PersonalList', () => {
      expect(component.find('ProfileList')).toHaveLength(1);
    });
  });
});
