import { ProfileByHandle } from '../../components/profile/ProfileByHandle';
import React from 'react';
import { shallow } from 'enzyme';

describe('Test CurrentProfile component', () => {
  const birthday = new Date();
  const mockGetProfileByHandle = jest.fn();
  const props = {
    getProfileByHandle: mockGetProfileByHandle,
    profile: {
      current: { data: "someData"},
      isLoading: false
    },
    computedMatch: {
      params: {
        handle: 'handle'
      }
    },
    user: {
      id: '1'
    }
  }

  describe('render initial without profile', () => {
    const initialProps = {
            ...props,
            profile: {
              ...props.profile, current: null
            }
          }
    const component = shallow(<ProfileByHandle {...initialProps} />);

    it ('calls getProfileByHandle', () => {
      expect(mockGetProfileByHandle).toHaveBeenCalledTimes(1);
    });

    it ('renders Preloader component', () => {
      expect(component.find('Preloader')).toHaveLength(1);
    })

    mockGetProfileByHandle.mockClear();
  });

  describe('render component with profile', () => {
    const component = shallow(<ProfileByHandle {...props} />);

    it ('renders Profile component', () => {
      expect(component.find('Profile')).toHaveLength(1);
    })
  });
});
