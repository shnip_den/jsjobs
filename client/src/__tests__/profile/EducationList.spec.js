import EducationList from '../../components/profile/EducationList';
import React from 'react';
import { shallow } from 'enzyme';

describe('Test EducationList', () => {
  const props = {
    eduList: [
      {schoolTitle: 'someschool', location: 'Moscow', from: "02-02-2018", to: "07-02-2018"},
      {schoolTitle: 'someschool', location: 'Moscow', from: "02-02-2018", to: "07-02-2018"},
      {schoolTitle: 'someschool', location: 'Moscow', from: "02-02-2018", to: "07-02-2018"}
    ]
  }

  const component = shallow(<EducationList { ... props} />);

  it ('renders elements', () => {
    expect(component.find('.profile__item-list')).toHaveLength(3);
  });

  it ('doesnt render elements when eduList equals empty array', () => {
    const newProps = {
            eduList: []
          }
    const component = shallow(<EducationList { ... newProps} />);

    expect(component.find('.profile__item-list')).toHaveLength(0);
  });
});
