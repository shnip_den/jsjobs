import { CurrentProfile } from '../../components/profile/CurrentProfile';
import React from 'react';
import { shallow } from 'enzyme';

describe('Test CurrentProfile component', () => {
  const birthday = new Date();
  const mockGetOwnProfile = jest.fn();
  const props = {
    getOwnProfile: mockGetOwnProfile,
    profile: {
      ownProfile: { data: "someData"},
      isLoading: false
    },
    user: {
      id: '1'
    }
  }

  describe('render initial without profile', () => {
    const initialProps = { ...props, profile: { ...props.profile, ownProfile: null}}

    const component = shallow(<CurrentProfile {...initialProps} />);

    it ('calls getOwnProfile when ownProfile is null', () => {
      expect(mockGetOwnProfile).toHaveBeenCalledTimes(1);
    });

    it ('renders Preloader component', () => {
      expect(component.find('Preloader')).toHaveLength(1);
    })

    afterEach(() => {
      mockGetOwnProfile.mockClear();
    })
  });

  describe('render component with profile', () => {
    const component = shallow(<CurrentProfile {...props} />);

    it ('renders Profile component', () => {
      expect(component.find('Profile')).toHaveLength(1);
    })
  });
});
