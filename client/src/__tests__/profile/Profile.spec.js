import Profile from '../../components/profile/Profile';
import React from 'react';
import { shallow, mount } from 'enzyme';

describe('Test Profile component', () => {
  const birthday = new Date();
  const props = {
    profile: {
      personal: {
        firstname: "Paul",
        lastname: "Adams",
        gender: "male",
        birthday: birthday
      },
      handle: "paul",
      account_type: "personal",
      specialization: {
        status: "Not looking for a job",
        qualification: "Intern",
        skills: ['html', 'css'],
        readyToRelocate: false,
        remoteJob: false
      },
      experiance: [{position: 'Junior', company: 'Netflix', from: "02-02-2018", to: "07-02-2018"}],
      education: [{schoolTitle: 'someschool', location: 'Moscow', from: "02-02-2018", to: "07-02-2018"}],
      contacts: {twitter: 'paul'},
      user: {
        id: '1',
        avatar: 'http://test.com'
      }
    },
    user: {
      id: '1'
    }
  }

  describe('render component with profile', () => {
    const component = shallow(<Profile {...props} />);

    it ('should render elements', () => {
      expect(component.find('PersonalTypeProps')).toHaveLength(1);
      expect(component.find('.infoblock__header').at(2).text()).toEqual('Experiance');
    });

    it ('tests result off calculateExperiance function', () => {
      expect(component.find('.infoblock__content').text()).toEqual('5 months');
    });

    it ('renders propperly', () => {
      expect(component).toMatchSnapshot();
    });
  });
});
