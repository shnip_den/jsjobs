import { ProfileCreate } from '../../../components/profile/create/ProfileCreate';
import React from 'react';
import { shallow } from 'enzyme';

describe('Test ProfileCreate', () => {
  const mockCreateProfile = jest.fn();

  const withFormState = {
    personal: {
      firstname: "",
      lastname: "",
      gender: "male",
      birthday: "12-12-2012"
    },
    handle: "",
    account_type: "personal",
    specialization: {
      status: "Not looking for a job",
      qualification: "Intern",
      readyToRelocate: false,
      remoteJob: false
    },
    experiance: [],
    education: [],
    contacts: {},
  }

  const props = {
    reducerErrors: {},
    auth: {
      isAuthenticated: false,
      user: {}
    },
    createProfile: mockCreateProfile,
    profileState: withFormState,
    convertToDate: jest.fn(),
    profile: {
      ownProfile: null
    },
    getOwnProfile: jest.fn()
  }

  const component = shallow(<ProfileCreate {...props} />);

  const initialState = {
    step: 1,
    errors: {}
  }

  it ('initializes with state', () => {
    expect(component.state()).toEqual(initialState);
  });

  it('should render with initial state', () => {
    expect(component.find('Progressbar')).toHaveLength(1);
    expect(component.find('Personal')).toHaveLength(1);
    expect(component.find('button').prop('disabled')).toBeTruthy();
  });

  describe('Test button states', () => {
    it ('should be enable when personal data filled and hasnt errors', () => {
    component.setProps({ profileState: {
                            ...props.profileState,
                            personal: {
                              ...props.profileState.personal,
                              firstname: "Ivan",
                              lastname: "Ivanov"
                            }
                          }
                        });

      expect(component.find('button').prop('disabled')).toBeFalsy();
    });

    it ('shouldnt be enable when has errors', () => {
      component.setProps({ profileState: withFormState });
      component.setState({
        errors: {
          personal: {
            firstname: {
              empty: "some error"
            }
          }
        }
      });
      expect(component.find('button').prop('disabled')).toBeTruthy();
    });

    it ('should show Create button instead of Next on the last step', () => {
      component.setState({ step: 6 });
      expect(component.find('.profile-step-creator__btn_right').text()).toEqual('Create');
    })

    afterAll(() => {
      component.setState(initialState);
    });
  });

  describe('Render components when the state changes', () => {
    it ('render AdditionalInfo component when state equals 2', () => {
      component.setState({ step: 2 });
      expect(component.find('AdditionalInfo')).toHaveLength(1);
    });

    it ('render Specialization when state equals 3', () => {
      component.setState({ step: 3 });
      expect(component.find('Specialization')).toHaveLength(1);
    });

    it ('render Experiance when state equals 4', () => {
      component.setState({ step: 4 });
      expect(component.find('Experiance')).toHaveLength(1);
    });

    it ('render Education when state equals 5', () => {
      component.setState({ step: 5 });
      expect(component.find('Education')).toHaveLength(1);
    });

    it ('render Contacts when state equals 6', () => {
      component.setState({ step: 6 });
      expect(component.find('Contacts')).toHaveLength(1);
    });

    afterAll(() => {
      component.setState(initialState);
    });
  })

  describe('submit form', () => {
    it ('should call createProfile action when hasnt errors', () => {
      component.setProps({ profileState: {
                            ...props.profileState,
                            personal: {
                              ...props.profileState.personal,
                              firstname: "Ivan",
                              lastname: "Ivanov"
                            },
                            handle: "test"
                          }
                        });
      component.setState({ step: 6});

      component.find('.profile-step-creator__btn_right').simulate('click', {
          preventDefault: () => {},
        });
      expect(mockCreateProfile).toHaveBeenCalledTimes(1);
    });

    it ('shouldnt call createProfile when state has errors', () => {
      component.setState({
        errors: {
          personal: {
            firstname: {
              empty: "some error"
            }
          }
        }
      });

      component.find('.profile-step-creator__btn_right').simulate('click', {
          preventDefault: () => {},
        });
      expect(mockCreateProfile).not.toBeCalled();
    });

    afterEach(() => {
      mockCreateProfile.mockClear();
    })

    afterAll(() => {
      component.setState(initialState);
    });
  })

  it('renders properly', () => {
    expect(component).toMatchSnapshot();
  });
});
