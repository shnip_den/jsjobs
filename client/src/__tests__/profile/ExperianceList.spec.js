import ExperianceList from '../../components/profile/ExperianceList';
import React from 'react';
import { shallow } from 'enzyme';

describe('Test EducationList', () => {
  const props = {
    expList: [
      {position: 'Junior', company: 'Netflix', from: "02-02-2018", to: "07-02-2018"},
      {position: 'Junior', company: 'Netflix', from: "02-02-2018", to: "07-02-2018"},
      {position: 'Junior', company: 'Netflix', from: "02-02-2018", to: "07-02-2018"}
    ]
  }

  const component = shallow(<ExperianceList { ... props} />);

  it ('renders elements', () => {
    expect(component.find('.profile__item-list')).toHaveLength(3);
  });

  it ('doesnt render elements when expList equals empty array', () => {
    const newProps = {
            expList: []
          }
    const component = shallow(<ExperianceList { ... newProps} />);

    expect(component.find('.profile__item-list')).toHaveLength(0);
  });
});
