import { ExperianceEdit } from '../../../components/profile/edit/ExperianceEdit';
import React from 'react';
import { shallow } from 'enzyme';

describe('Test PersonalEdit component', () => {
  const mockUpdateExperiance = jest.fn();
  const props = {
    profileState: {
      experiance: [
        {position: 'Junior', company: 'Netflix', from: "02-02-2018", to: "07-02-2018"}
      ]
    },
    reduxErrors: {},
    clearReduxErrors: jest.fn(),
    updateExperiance: mockUpdateExperiance
  }

  const initialState = { errors: {} }

  const component = shallow(<ExperianceEdit {...props} />);

  it('set state from props', () => {
    expect(component.state()).toEqual(initialState);
  })

  it('renders Experiance component', () => {
    expect(component.find('Experiance')).toHaveLength(1);
  });

  it('renders enabled button Update', () => {
    expect(component.find('button').prop('disabled')).toBeFalsy();
  });

  it('doesnt call updateExperiance when has error', () => {
    component.setState({
      errors: { experiance: { position: 'error'}}
    });
    component.find("form[name='profile-edit']").simulate('submit', {
      preventDefault: jest.fn()
    });

    expect(mockUpdateExperiance).toHaveBeenCalledTimes(0);
    component.setState(initialState);
  });

  it('calls updateExperiance when state has not errors', () => {
    component.find("form[name='profile-edit']").simulate('submit', {
      preventDefault: jest.fn()
    });

    expect(mockUpdateExperiance).toHaveBeenCalledTimes(1);
  })
});
