import { SpecializationEdit } from '../../../components/profile/edit/SpecializationEdit';
import React from 'react';
import { shallow } from 'enzyme';

describe('Test PersonalEdit component', () => {
  const mockUpdateSpecialization = jest.fn();
  const props = {
    profileState: {
      specialization: {
        status: "Not looking for a job",
        qualification: "Intern",
        skills: ['html', 'css'],
        readyToRelocate: false,
        remoteJob: false
      },
    },
    reduxErrors: {},
    clearReduxErrors: jest.fn(),
    updateSpecialization: mockUpdateSpecialization
  }

  const initialState = { errors: {} }

  const component = shallow(<SpecializationEdit {...props} />);

  it('set state from props', () => {
    expect(component.state()).toEqual(initialState);
  })

  it('renders Specialization component', () => {
    expect(component.find('Specialization')).toHaveLength(1);
  });

  it('renders enabled button Update', () => {
    expect(component.find('button').prop('disabled')).toBeFalsy();
  });

  it('doesnt call updateSpecialization when has error', () => {
    component.setState({
      errors: { specialization: { someError: 'error'}}
    });
    component.find("form[name='profile-edit']").simulate('submit', {
      preventDefault: jest.fn()
    });

    expect(mockUpdateSpecialization).toHaveBeenCalledTimes(0);
    component.setState(initialState);
  });

  it('calls updateSpecialization when state has not errors', () => {
    component.find("form[name='profile-edit']").simulate('submit', {
      preventDefault: jest.fn()
    });

    expect(mockUpdateSpecialization).toHaveBeenCalledTimes(1);
  })
});
