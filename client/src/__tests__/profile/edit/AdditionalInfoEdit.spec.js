import { AdditionalInfoEdit } from '../../../components/profile/edit/AddtionalInfoEdit';
import React from 'react';
import { shallow } from 'enzyme';

describe('Test PersonalEdit component', () => {
  const mockUpdateAdditionalInfo = jest.fn();
  const props = {
    profileState: {
      handle: 'handle',
      account_type: 'personal'
    },
    reduxErrors: {},
    clearReduxErrors: jest.fn(),
    updateAdditionalInfo: mockUpdateAdditionalInfo
  }

  const initialState = { errors: {} }

  const component = shallow(<AdditionalInfoEdit {...props} />);

  it('set state from props', () => {
    expect(component.state()).toEqual(initialState);
  })

  it('renders AdditionalInfo component', () => {
    expect(component.find('AdditionalInfo')).toHaveLength(1);
  });

  it('renders enabled button Update', () => {
    expect(component.find('button').prop('disabled')).toBeFalsy();
  });

  it('doesnt call updateAdditionalInfo when has error', () => {
    component.setState({
      errors: { handle: { empty: 'message'}}
    });
    component.find("form[name='profile-edit']").simulate('submit', {
      preventDefault: jest.fn()
    });

    expect(mockUpdateAdditionalInfo).toHaveBeenCalledTimes(0);
    component.setState(initialState);
  });

  it('calls updateAdditionalInfo when state has not errors', () => {
    component.find("form[name='profile-edit']").simulate('submit', {
      preventDefault: jest.fn()
    });

    expect(mockUpdateAdditionalInfo).toHaveBeenCalledTimes(1);
  })
});
