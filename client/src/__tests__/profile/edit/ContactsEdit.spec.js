import { ContactsEdit } from '../../../components/profile/edit/ContactsEdit';
import React from 'react';
import { shallow } from 'enzyme';

describe('Test PersonalEdit component', () => {
  const mockUpdateContacts = jest.fn();
  const props = {
    profileState: {
      contacts: {
        twitter: 'nickname'
      }
    },
    updateContacts: mockUpdateContacts
  }

  const initialState = { errors: {} }

  const component = shallow(<ContactsEdit {...props} />);

  it('set state from props', () => {
    expect(component.state()).toEqual(initialState);
  })

  it('renders Contacts component', () => {
    expect(component.find('Contacts')).toHaveLength(1);
  });

  it('renders enabled button Update', () => {
    expect(component.find('button').prop('disabled')).toBeFalsy();
  });

  it('doesnt call updateContacts when has error', () => {
    component.setState({
      errors: { contacts: { someError: 'error'}}
    });
    component.find("form[name='profile-edit']").simulate('submit', {
      preventDefault: jest.fn()
    });

    expect(mockUpdateContacts).toHaveBeenCalledTimes(0);
    component.setState(initialState);
  });

  it('calls updateContacts when state has not errors', () => {
    component.find("form[name='profile-edit']").simulate('submit', {
      preventDefault: jest.fn()
    });

    expect(mockUpdateContacts).toHaveBeenCalledTimes(1);
  })
});
