import React from 'react';
import { shallow } from 'enzyme';
import {PersonalEdit} from '../../../components/profile/edit/PersonalEdit';

describe('Test PersonalEdit component', () => {
  const birthday = new Date();
  const mockUpdatePersonal = jest.fn();
  const mockClearReduxErrors = jest.fn();

  const props = {
    profileState: {
      personal: {
        firstname: "Paul",
        lastname: "Adams",
        gender: "male",
        birthday
      }
    },
    reduxErrors: {},
    clearReduxErrors: mockClearReduxErrors,
    updatePersonal: mockUpdatePersonal
  }

  const initialState = { errors: {} }

  const component = shallow(<PersonalEdit {...props} />);

  it('set state from props', () => {
    expect(component.state()).toEqual(initialState);
  })

  it('renders Personal component', () => {
    expect(component.find('Personal')).toHaveLength(1);
  });

  it('renders enabled button Update', () => {
    expect(component.find('button').prop('disabled')).toBeFalsy();
  });

  it('doesnt call updatePersonal when has error', () => {
    component.setState({
      errors: { personal: { firstname: 'error'}}
    });
    component.find("form[name='profile-edit']").simulate('submit', {
      preventDefault: jest.fn()
    });

    expect(mockUpdatePersonal).toHaveBeenCalledTimes(0);
    component.setState(initialState);
  });

  it('calls updatePersonal when state has not errors', () => {
    component.find("form[name='profile-edit']").simulate('submit', {
      preventDefault: jest.fn()
    });

    expect(mockUpdatePersonal).toHaveBeenCalledTimes(1);
  })

  it('doesnt call clearReduxErrors in componentWillUnmount when reduxErrors is empty', () => {
    component.unmount();

    expect(mockClearReduxErrors).toHaveBeenCalledTimes(0);
  });

  it('calls clearReduxErrors in componentWillUnmount when reduxErrors isn\'t empty', () => {
    component.setProps({ reduxErrors: { someError: "error"}})
    component.unmount();

    expect(mockClearReduxErrors).toHaveBeenCalledTimes(1);
  });
});
