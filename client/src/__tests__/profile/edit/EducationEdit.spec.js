import { EducationEdit } from '../../../components/profile/edit/EducationEdit';
import React from 'react';
import { shallow } from 'enzyme';

describe('Test PersonalEdit component', () => {
  const mockUpdateEducation = jest.fn();
  const props = {
    profileState: {
      education: [
        {position: 'Junior', company: 'Netflix', from: "02-02-2018", to: "07-02-2018"}
      ]
    },
    reduxErrors: {},
    clearReduxErrors: jest.fn(),
    updateEducation: mockUpdateEducation
  }

  const initialState = { errors: {} }

  const component = shallow(<EducationEdit {...props} />);

  it('set state from props', () => {
    expect(component.state()).toEqual(initialState);
  })

  it('renders Education component', () => {
    expect(component.find('Education')).toHaveLength(1);
  });

  it('renders enabled button Update', () => {
    expect(component.find('button').prop('disabled')).toBeFalsy();
  });

  it('doesnt call updateEducation when has error', () => {
    component.setState({
      errors: { education: { position: 'error'}}
    });
    component.find("form[name='profile-edit']").simulate('submit', {
      preventDefault: jest.fn()
    });

    expect(mockUpdateEducation).toHaveBeenCalledTimes(0);
    component.setState(initialState);
  });

  it('calls updateEducation when state has not errors', () => {
    component.find("form[name='profile-edit']").simulate('submit', {
      preventDefault: jest.fn()
    });

    expect(mockUpdateEducation).toHaveBeenCalledTimes(1);
  })
});
