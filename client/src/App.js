import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from './redux/store';
import jwt_decode from 'jwt-decode';
import { setCurrentUser, logoutUser } from './redux/actions/authActions';
import { clearAllProfiles } from './redux/actions/profileActions';
import setAuthToken from './utils/setAuthToken';
import PrivateRouter from './components/helpers/PrivateRouter';

/* CSS */
import 'bootstrap/dist/css/bootstrap-grid.min.css';
import './css/index.css';

/* Import COMPONENTS */
import Navbar from './components/layouts/navbar';
import Footer from './components/layouts/footer';
import Homepage from './components/static_pages/homepage';
import Login from './components/auth/login';
import Register from './components/auth/register';
import ProfileCreate from './components/profile/create/ProfileCreate';
import CurrentProfile from './components/profile/CurrentProfile';
import ProfileEdit from './components/profile/edit/ProfileEdit';
import Profiles from './components/profile/profiles/Profiles';
import ProfileByHandle from './components/profile/ProfileByHandle';
import Room from './components/messages/Room';
import JobCreate from './components/Job/JobCreate';
import JobEdit from './components/Job/JobEdit';
import Job from './components/Job/Job';
import Jobs from './components/Job/Jobs';
import Applications from './components/Job/Applications';


/* Add fontawesome */
import { library } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
library.add(fab);


if (localStorage.jwt){
  setAuthToken(localStorage.jwt);

  const decoded = jwt_decode(localStorage.jwt);

  store.dispatch(setCurrentUser(decoded));

  const currentTime = Date.now() / 1000;
  if(decoded.exp < currentTime){
    store.dispatch(logoutUser());
    store.dispatch(clearAllProfiles());
    window.location.href = '/login';
  }
}

class App extends Component {
  render() {
    return (
      <Provider store = { store } >
        <Router>
          <div className="page-wrapper">
            <Navbar />
            <Route exact path="/" component={ Homepage } />
            <div className="container page-wrapper__content">
              <Route exact path="/login" component={ Login } />
              <Route exact path="/signup" component={ Register } />

              <Switch>
                <PrivateRouter exact path="/profile"
                               component={ CurrentProfile } />
                <PrivateRouter exact path="/profile/new"
                               component={ ProfileCreate } />
                <PrivateRouter exact path="/profile/edit"
                               component={ ProfileEdit } />
                <PrivateRouter exact path="/profiles"
                               component = { Profiles } />
                <PrivateRouter exact path="/profile/:handle"
                               component = { ProfileByHandle } />
                <PrivateRouter exact path="/messages"
                               component = { Room } />
                <PrivateRouter exact path="/job/create"
                               component = { JobCreate } />
                <PrivateRouter exact path="/job/edit"
                               component = { JobEdit } />
                <PrivateRouter exact path="/jobs"
                               component = { Jobs } />
                <PrivateRouter exact path="/job/:id"
                               component = { Job } />
                <PrivateRouter exact path="/applications"
                               component = { Applications } />
              </Switch>
            </div>
            <Footer />
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
