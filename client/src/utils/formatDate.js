const formatDate = (date, format = null) => {
  if (!date) date = new Date();
  if (typeof date === 'string') date = new Date(date);

  let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  let month = months[date.getMonth()];

  if (format) {
    return `${date.getDate()} ${month} ${date.getFullYear()}`; 
  }

  return `${month} ${date.getFullYear()}`;
}

export default formatDate;
