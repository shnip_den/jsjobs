const redirectWithCondition = (props, condition, successRedirect = '/', failureRedirect = '/', auth = true) => {
  if (auth && !props.auth.isAuthenticated) return null;

  if (condition) return props.history.push(successRedirect);
  
  props.history.push(failureRedirect);
}

export default redirectWithCondition;
