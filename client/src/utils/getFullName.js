import capitalize from './capitalize';

const getFullName = (profile) => {
  const {firstname, lastname} = profile.personal;
  return `${capitalize(lastname)} ${capitalize(firstname)}`
}

export default getFullName;
