const debounce = (func, interval) => {
  let timer;
  return (...args) => {
      clearTimeout(timer);
      timer = setTimeout(() => { func.apply(this, args) }, interval);
  };
}

export default debounce
