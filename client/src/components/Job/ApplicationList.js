import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const ApplicationList = (props) => {
  const {applications, handleDelete } = props;

  return (
      <ul className="ul">
        {
          applications.map(application => {
            return (
              <li className="job__application" key={application._id}>
                <div className="profiles__user-info">
                  <img src={application.profile.user.avatar} alt="avatar" className="img-circle profiles__avatar"/>
                </div>
                <div className="profiles__content">
                  <h3 className="profiles__username">
                    <Link to={`/profile/${application.profile.handle}`}>
                      {`${application.profile.personal.firstname} ${application.profile.personal.lastname}`}
                    </Link>
                  </h3>
                  <div className="profiles__description">
                    { application.profile.personal.location }
                  </div>
                  <div className="profiles__description">
                    <ul className="ul">
                      {
                        application.profile.specialization.skills.map((item, index) => {
                          return (
                            <li className="tag" key={index}> {item} </li>
                          )
                        })
                      }
                    </ul>
                  </div>
                </div>
                <div className="profiles__buttons">
                  <Link to={`/profile/${application.profile.handle}`}>
                    <button className="btn btn_pelorous-theme profiles__btn">View</button>
                  </Link>
                  <Link to='#' >
                    <button className="btn btn_pelorous-theme profiles__btn"
                            onClick = { handleDelete}
                            data-id = {application._id}>
                      Reject
                    </button>
                  </Link>
                </div>
              </li>
            )
          })
        }
      </ul>
  );
}

ApplicationList.propTypes = {
  applications: PropTypes.array.isRequired,
  handleDelete: PropTypes.func.isRequired
};

export default ApplicationList;
