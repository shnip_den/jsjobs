import React from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import axios from 'axios';
import formatDate from '../../utils/formatDate';
import Preloader from '../helpers/Preloader';
import ApplicationList from './ApplicationList';
import { getOwnProfile } from '../../redux/actions/profileActions';


export class Job extends React.Component {
  state = {
    job: null,
    errors: {}
  }

  componentDidMount() {
    const id = this.props.computedMatch.params.id;
    if (!id) this.setState({ errors: { message: "Id is missing"}})
    if (!this.props.ownProfile) {
      return this.getProfileAndJob(id)
    }
    this.getJob(id);
  }

  getJob = (id) => {
    axios.get(`/job/${id}`).then(res => this.setState({ job:  res.data }))
                          .catch(err => this.setState({ errors: err }))
  }

  getProfileAndJob = async (id) => {
    await this.props.getOwnProfile();
    this.getJob(id);
  }

  addApplication = (e) => {
    const job_id = this.state.job._id;
    const profile_id = this.props.ownProfile._id;

    axios.post('/job/applications', { _id: job_id, profile_id }).then(res => {
      this.setState({ job: {
                       ...this.state.job, applications: res.data.applications
                     }
                  });
    })
    .catch(e => {
      this.setState({ errors: e });
    });
  }

  handleDelete = (e) => {
    const app_id = e.target.dataset.id;

    this.deleteApplication(app_id);
  }

  deleteApplication = (app_id) => {
    const job_id = this.state.job._id;
    const filtredApplication = this.state.job.applications.filter(item => {
      return item._id !== app_id;
    });
    this.setState({ job: {
                     ...this.state.job, applications: filtredApplication
                   }
                });

    axios.patch('/job/applications', { job_id, app_id })
         .catch(err => this.setState({ errors: err }));
  }

  isApplied = () => {
    const index = this.getAppliedIndex();
    return index >= 0 ? true : false
  }

  getAppliedIndex = () => {
    const profile_id = this.props.ownProfile._id;

    const index = this.state.job.applications.findIndex(item => {
      return item.profile._id === profile_id
    });

    return index
  }

  cancelApplication = () => {
    const index = this.getAppliedIndex();
    const app_id = this.state.job.applications[index]._id;

    this.deleteApplication(app_id);
  }

  render() {
    const { job } = this.state;

    return (
        <>
        {  job ?
          <div className="col-12 col-md-6 offset-md-3">
            <div className="job-wrapper">
              <div className="job">
                <div className="job__item-header">
                  <h3 className="job__item-title">{job.title}</h3>
                  <span className="text-grey">{formatDate(job.created_at)}</span>
                </div>
                <div className="job__item-container">
                  <div><strong>{ job.qualification || '' }</strong></div>
                  <div className="job__skills">
                    <ul className="ul">
                      {job.skills.map((skill, index) => {
                        return (
                          <li className="job__skills-item" key={index}>{skill}</li>
                        )
                      })}
                    </ul>
                  </div>
                  <div className="job__country">
                    { job.location }
                  </div>
                </div>
              </div>

              <section className="job">
                <div className="job__item-header">
                  <h3 className="job__item-title">Description</h3>
                  <hr/>
                </div>
                <div className="job__description">
                  { job.description }
                </div>
              </section>

              { this.props.user.id === job.user &&
                <section className="job">
                  <h3 className="job__item-title">Applications</h3>
                  <hr/>
                  <ApplicationList applications = { job.applications }
                                   handleDelete = {this.handleDelete} />
                </section>
              }

              <section className="btn__row">
                <button className="btn btn_pelorous-theme"
                        onClick={() => this.props.history.goBack()}>
                  Back
                </button>
                { this.props.user.id !== job.user ?
                  <>
                    {
                      this.isApplied() ?
                      <button className="btn btn_pelorous-theme" onClick = {this.cancelApplication}>
                        Cancel
                      </button>
                      :
                      <button className="btn btn_pelorous-theme" onClick = {this.addApplication}>
                        Apply for job
                      </button>
                    }
                  </>
                  :
                  <Link to={{ pathname: '/job/edit', query: { id: job._id } }}>
                    <button className="btn btn_pelorous-theme">Edit</button>
                  </Link>
                }
              </section>
            </div>
          </div>
          :
          <Preloader />
        }
      </>

    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    ownProfile: state.profile.ownProfile
  }
}
export default connect(mapStateToProps, { getOwnProfile })(withRouter(Job));
