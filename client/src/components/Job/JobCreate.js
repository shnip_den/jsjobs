import React from 'react';
import FormJob from './FormJob';
import validate from './jobValidation';
import axios from 'axios';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { getOwnProfile } from '../../redux/actions/profileActions';
import Preloader from '../helpers/Preloader';


export class JobCreate extends React.Component {
  state = {
    job: {
      title: '',
      qualification: '',
      skills: [],
      description: '',
      location: ''
    },
    errors: {}
  }

  componentDidMount() {
    if (!this.props.ownProfile) this.props.getOwnProfile();
  }

  onSubmit = (e) => {
    e.preventDefault();
    const job = { ...this.state.job, profile: this.props.ownProfile._id }
    const { isValid, errors } = validate(job);
    if (!isValid) return this.setState({ errors: errors });

    this.createJob(job);
  }

  createJob = (job) => {
    axios.post('/job', job)
         .then(res => this.props.history.push('/profile'))
         .catch(e => this.setState({ errors: e }))
  }

  onChange = (e) => {
    const { name, value } = e.target;
    this.setState({ job: { ...this.state.job, [name]: value } })
  }

  handleSkills = (value) => {
    this.setState({ job: { ...this.state.job, skills: value } })
  }

  render() {
    const { job, errors } = this.state;
    const { ownProfile } = this.props;
    return (
      <>
      { ownProfile ?
      <div className="col-12 col-md-6 offset-md-3">
        <form className="job__form" onSubmit={this.onSubmit}>
          <h2 className="form__title">Create Job</h2>
          <hr className="hr"/>
          <FormJob  job={job}
                    errors={errors}
                    onChange = { this.onChange }
                    handleSkills = { this.handleSkills } />

          <button className="btn btn_pelorous-theme">Create</button>
        </form>
      </div>
      :
      <Preloader />
      }
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    ownProfile: state.profile.ownProfile
  }
}

export default connect(mapStateToProps, { getOwnProfile })(withRouter(JobCreate));
