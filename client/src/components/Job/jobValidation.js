import validator from 'validator';

const validate = (data) => {
  const { title, description }= data;

  const errors = {};

  if (validator.isEmpty(title, {ignore_whitespace: true})) {
    errors.title = { empty: "Title cant be empty" };
  }

  if (validator.isEmpty(description, {ignore_whitespace: true})) {
    errors.description = { empty: "Description cant be empty" };
  }

  return { errors, isValid: Object.keys(errors).length === 0}
}

export default validate;
