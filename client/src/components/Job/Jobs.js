import React from 'react';
import {connect} from 'react-redux';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import pluralize from 'pluralize';
import Preloader from '../helpers/Preloader'
import JobList from './JobList';
import SearchForm from '../search/Search';


export class Jobs extends React.Component {
  state = {
    jobs: null,
    search: '',
    errors: {}
  }

  componentDidMount() {
    this.getJobs();
  }

  getJobs = () => {
    axios.get('job/all').then(res => this.setState({ jobs: res.data }))
                          .catch(err => this.setState({ errors: err }))
  }

  handleDelete = (e) => {
    const isAgree = window.confirm('Do you confirm deletion?');
    const id = e.target.dataset.id;
    if (isAgree && id) {
      this.deleteJob(id);
    }
  }

  deleteJob = (id) => {
    axios.delete(`/job/${id}`).then(res => {
      const filtredJobs = this.state.jobs.filter(item => {
        return item._id !== id;
      })
      this.setState({ jobs: filtredJobs })
      this.props.history.push('/profile')
    })
    .catch(err => this.setState({ errors: err }))
  }

  onChange = (e) => {
    this.setState({ search: e.target.value });
  }

  render() {
    const { jobs, search } = this.state;

    return (
      <>
        {  jobs ?
            <div className="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2">
              <div className="job-wrapper">
                <main className="job">
                  <strong>Found {pluralize('job', jobs.length, true)}</strong>
                  <section className="job-search">
                    <SearchForm value = {search} onChange = {this.onChange}/>
                    <small>todo: full text search</small>
                  </section>
                  <JobList  jobs = { jobs }
                            user = {this.props.user}
                            handleDelete={this.handleDelete.bind(this)}
                            editable = {false} />
                </main>
              </div>
            </div>
            :
            <Preloader />
        }
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user
  }
}

export default connect(mapStateToProps)(withRouter(Jobs));
