import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import JobList from './JobList';


export class UserJobs extends React.Component {
  state = {
    jobs: null,
    errors: {}
  }

  componentDidMount() {
    this.getJobs();
  }

  getJobs = () => {
    const { profile_id } = this.props;

    axios.get(`/job/user/${profile_id}`)
         .then(res => this.setState({ jobs: res.data }))
         .catch(err => this.setState({ errors: err }))
  }

  handleDelete = (e) => {
    const isAgree = window.confirm('Do you confirm deletion?');
    const id = e.target.dataset.id;
    if (isAgree && id) {
      this.deleteJob(id);
    }
  }

  deleteJob = (id) => {
    axios.delete(`/job/${id}`).then(res => {
      const filtredJobs = this.state.jobs.filter(item => {
        return item._id !== id;
      })
      this.setState({ jobs: filtredJobs })
      this.props.history.push('/profile')
    })
    .catch(err => this.setState({ errors: err }))
  }

  render() {
    const { jobs } = this.state;

    return (
      <>
        {  jobs && jobs.length > 0 ?
            <JobList  jobs = { jobs }
                      user = {this.props.user}
                      handleDelete={this.handleDelete.bind(this)} />
          :
          <div className="profile__user-info">
            User haven't any jobs
          </div>
        }
      </>
    );
  }
}

UserJobs.propTypes = {
  user: PropTypes.object.isRequired,
  profile_id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object
  ])
}

const mapStateToProps = state => {
  return {
    user: state.auth.user
  }
}

export default connect(mapStateToProps)(withRouter(UserJobs));
