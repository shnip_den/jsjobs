import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import formatDate from '../../utils/formatDate';

const JobList = (props) => {
  const { jobs, user, handleDelete = () => {}, editable= true } = props;

  return (
    <ul className="ul">
      { jobs.map(job => {
        return (
          <li className="job__item" key={job._id}>
            <div className="job__item-header">
              <Link to={`/job/${job._id}`}>
                <h3 className="job__item-title">{job.title}</h3>
              </Link>
              <span className="text-grey">{formatDate(job.created_at)}</span>
            </div>
            <div className="job__item-body">
              <div className="job__item-left">
                <div><strong>{ job.qualification || '' }</strong></div>
                <div className="job__skills">
                  <ul className="ul">
                    {job.skills.map((skill, index) => {
                      return (
                        <li className="job__skills-item" key={index}>{skill}</li>
                      )
                    })}
                  </ul>
                </div>
                <div className="job__country">
                  <span className="text-grey">{ job.location }</span>
                </div>
              </div>
              <div className="job__item-right">
                <Link to={`job/${job._id}`}>
                  <button className="btn btn_pelorous-theme job__btns">View</button>
                </Link>
                { editable && user.id === job.user._id &&
                  <Link to="#" onClick={ handleDelete } data-id={job._id}>
                    Delete
                  </Link>
                }
              </div>
            </div>
          </li>
        )
      }) }
    </ul>
  );
}

JobList.propTypes = {
  jobs: PropTypes.array.isRequired,
  user: PropTypes.object.isRequired,
  handleDelete: PropTypes.func
};

export default JobList;
