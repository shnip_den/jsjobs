import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import axios from 'axios';
import Preloader from '../helpers/Preloader';
import ApplicationJobList from './ApplicationJobList';
import { getOwnProfile } from '../../redux/actions/profileActions';


export class Applications extends React.Component {
  state = {
    jobs: null,
    errors: {}
  }

  componentDidMount() {
    if (!this.props.ownProfile) {
     return this.getProfileAndJobs();
    }

    this.getJobs();
  }

  getProfileAndJobs = async () => {
    await this.props.getOwnProfile();
    this.getJobs();
  }

  getJobs = () => {
    if (!this.props.ownProfile) return this.setState({ errors: { message: "job loading is faliled"}});
    const profile_id = this.props.ownProfile._id;

    axios.get(`/job/applications/${profile_id}`).then(res => this.setState({ jobs: res.data }))
                          .catch(err => this.setState({ errors: err }))
  }

  cancelApplication = (e) => {
    const jobs = this.state.jobs;
    const job_id = e.target.dataset.id;
    const jobIndex = jobs.findIndex(job => {
      return job._id === job_id;
    });
    const profile_id = this.props.ownProfile._id;
    const appIndex = jobs[jobIndex].applications.findIndex(app => {
      return app.profile === profile_id;
    })
    const app_id = jobs[jobIndex].applications[appIndex]._id;
    jobs.splice(jobIndex, 1);
    this.setState({ jobs });
    axios.patch('/job/applications', {job_id, app_id}).then(res => {
      console.log('delete ok')
    }).catch(err => {
      this.setState({ errors: err });
    })
  }

  render() {
    const { jobs } = this.state;

    return (
      <>
        {  jobs ?
            <div className="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2">
              <div className="job-wrapper">
                <main className="job">
                  <h2 className="job__item-title">My applications</h2>
                  <hr/>
                  <ApplicationJobList  jobs = { jobs }
                                       cancelApplication={this.cancelApplication}/>
                </main>
              </div>
            </div>
            :
            <Preloader />
        }
      </>
    );
  }
}

Applications.propTypes = {
  getOwnProfile: PropTypes.func.isRequired
}

const mapStateToProps = state => {
  return {
    ownProfile: state.profile.ownProfile
  }
}

export default connect(mapStateToProps, { getOwnProfile })(Applications);
