import React from 'react';
import PropTypes from 'prop-types';
import Field from '../helpers/forms/field';
import TagsInput from 'react-tagsinput';

const FormJob = (props) => {
  const { title, qualification, skills, description, location } = props.job;
  const { errors, handleSkills, onChange } = props;

  return (
    <>
      <Field  component = "input"
              name="title"
              id = "title"
              className="input form__input"
              value = { title }
              placeholder="title"
              onChange = { onChange }
              classNamesParams ={ { "input__invalid-input": errors.title } }
              required = { true }
              errors = { errors.title }
              classLabel = "label label_required form__label"/>

      <Field  component = "select"
              name = "qualification"
              id = "qualification"
              className = "input form__input"
              classLabel = "label form__label"
              errors = { errors.qualification }
              value = { qualification }
              onChange = { onChange }
              options = {[
                ["", ""],
                ["Intern", "Intern"],
                ["Junior", "Junior"],
                ["Middle", "Middle"],
                ["Senior", "Senior"],
                ["Lead", "Lead"]
              ]}
      />
      <label htmlFor = "skills" className = "label form__label">
        Job skills
      </label>
      <small>Write a tag and press Enter or Tab to add it to the field</small>
      <TagsInput  onChange = { handleSkills }
                  value = { skills }
                  className = "tagsinput"
                  inputProps = {{
                      className: 'tagsinput__input',
                      placeholder: 'Add a tag'
                  }}
                  focusedClassName = "tagsinput_focused"
                />

      <Field  component = "input"
              name="location"
              id = "location"
              value = { location }
              onChange = { onChange }
              className="input form__input"
              classNamesParams ={ { "input__invalid-input": errors.location } }
              required = { true }
              errors = { errors.location }
              classLabel = "label label_required form__label"/>

      <Field  component = "textarea"
              name="description"
              id = "description"
              cols = "30"
              rows =  "8"
              maxLength = "800"
              value = { description }
              onChange = { onChange }
              className="input form__input"
              classNamesParams ={ { "input__invalid-input": errors.description } }
              required = { true }
              errors = { errors.description }
              classLabel = "label label_required form__label"/>
    </>
  );
}

FormJob.propTypes = {
  job: PropTypes.object,
  errors: PropTypes.object
};

export default FormJob;
