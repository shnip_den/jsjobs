import React from 'react';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import FormJob from './FormJob';
import validate from './jobValidation';
import Preloader from '../helpers/Preloader';


export class JobEdit extends React.Component {
  state = {
    job: null,
    errors: {}
  }

  componentDidMount() {
    let id;
    if(this.props.location.query && this.props.location.query.id) {
      id = this.props.location.query.id;
    }

    if (!id) this.props.history.push('/profile')
    this.getJob(id);
  }

  getJob = (id) => {
    axios.get(`/job/${id}`).then(res => {
                                  this.setState({ job: res.data })
                                })
                          .catch(err => this.setState({ errors: err }))
  }

  onSubmit = (e) => {
    e.preventDefault();
    const job = this.state.job;
    const { isValid, errors } = validate(job);
    if (!isValid) return this.setState({ errors: errors })

    this.updateJob(job);
  }

  updateJob = (job) => {
    axios.patch('/job', job)
         .then(res => this.props.history.push('/profile'))
         .catch(e => this.setState({ errors: e }))
  }

  onChange = (e) => {
    const { name, value } = e.target;
    this.setState({ job: { ...this.state.job, [name]: value } })
  }

  handleSkills = (value) => {
    this.setState({ job: { ...this.state.job, skills: value } })
  }

  render() {
    const { job, errors } = this.state;

    return (
      <>
        {  job ?
          <div className="col-12 col-md-6 offset-md-3">
            <form className="job__form" onSubmit={this.onSubmit}>
              <h2 className="form__title">Update Job</h2>
              <hr className="hr"/>
              <FormJob  job={job}
                        errors={errors}
                        onChange = { this.onChange }
                        handleSkills = { this.handleSkills } />

              <button className="btn btn_pelorous-theme">Update</button>
            </form>
          </div>
          :
          <Preloader />
        }
      </>
    );
  }
}

export default withRouter(JobEdit);
