const validator = require('validator');

const checkField = (field, value, errors) => {
  switch (field) {
    case "firstname":
      if(validator.isEmpty(value, {ignore_whitespace: true})) {
        errors.firstname = { empty: "Firstname cant be empty" };
      }
      break;
    case "lastname":
      if(validator.isEmpty(value, {ignore_whitespace: true})) {
        errors.lastname = { empty: "Lastname cant be empty" };
      }
      break;
    default:
      return { errors: {}, isValid: false }
  }
}

export function validatePersonalField(field, value) {
  let errors = {};

  if (value.trim() && this.state.errors.personal && this.state.errors.personal[field]) {
    const personalErr = Object.assign({}, this.state.errors.personal);
    delete personalErr[field];

    if (Object.keys(personalErr).length === 0) {
      const otherErr = Object.assign({}, this.state.errors);
      delete otherErr.personal
      return this.setState({ errors: otherErr });
    } else {
      this.setState({ errors: {...this.state.errors,
                        personal: personalErr
                      }
                    })
    }
  }

  checkField.call(this, field, value, errors);

  if (Object.keys(errors).length > 0) {
    this.setState({
      errors: { ...this.state.errors,
                personal: { ...this.state.errors.personal,
                                [field]: errors[field] }
              }
    });
  }

};

export function validatePersonalForm(personal) {
  let errors = {};
  errors.personal = {};
  const { firstname = "", lastname = "" } = personal;

  checkField.call(this, "firstname", firstname, errors);
  checkField.call(this, "lastname", lastname, errors);

  if (Object.keys(errors.personal).length === 0) delete errors.personal;

  const result = { errors, isValid: Object.keys(errors).length === 0 };

  return !result.isValid;
}

export function clearPersonalErrors(errors, field) {
  if (!errors.personal) return null;

  if(errors.personal[field]) {
    delete errors.personal[field];

    if (Object.keys(errors.personal).length === 0) {
      delete errors.personal
    }

    this.setState({
      errors: errors
    });
  }
}
