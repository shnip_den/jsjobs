const validator = require('validator');

export function validateEducation(education){
  const { schoolTitle = "", location = "", from = "" } = education;
  const errors = {};

  if (validator.isEmpty(schoolTitle, {ignore_whitespace: true})) {
    errors.schoolTitle = { empty: "Title cant be empty" }
  }

  if (validator.isEmpty(location, {ignore_whitespace: true})) {
    errors.location = { empty: "Location cant be empty" }
  }

  if (validator.isEmpty(from.toString(), {ignore_whitespace: true})) {
    errors.from = { empty: "From field cant be empty" }
  }

  if (Object.keys(errors).length > 0) {
    this.setState({ errors: { ...this.state.errors,
                              education: errors
                            }
                  })
  }

  return { isValid: Object.keys(errors).length === 0 }
};

export function clearEducationErrors(errors, field) {

  if (!errors.education) return null;

  if (errors.education[field]) {
    delete errors.education[field];
    this.setState({ errors: { ...this.state.errors,
                              education: errors.education} })
  }
};

export function clearFieldEducationErrors(field, value) {
  const errors = this.state.errors;

  if (!errors.education) return null;

  if (value.trim() && this.state.errors.education && this.state.errors.education[field]) {
    const eduErr = Object.assign({}, this.state.errors.education);
    delete eduErr[field];

    if (Object.keys(eduErr).length === 0) {
      const otherErr = Object.assign({}, this.state.errors);
      delete otherErr.education
      return this.setState({ errors: otherErr });
    } else {
      this.setState({ errors: {...this.state.errors,
                        education: eduErr
                      }
                    })
    }
  }
}
