const validator = require('validator');

export function validateExperiance(experiance){
  const { position = "", company ="", from ="" } = experiance;
  const errors = {};

  if (validator.isEmpty(position, { ignore_whitespace: true })){
    errors.position = { empty: "Position cant be empty" }
  }

  if (validator.isEmpty(company, { ignore_whitespace: true })){
    errors.company = { empty: "Company cant be empty" }
  }

  if (validator.isEmpty(from.toString(), { ignore_whitespace: true })){
    errors.from =  { empty: "From field cant be empty" }
  }

  if (Object.keys(errors).length > 0) {
    this.setState({ errors: { ...this.state.errors,
                              experiance: errors
                            }
                  })
  }

  return { isValid: Object.keys(errors).length === 0 }
};

export function clearExperianceErrors(errors, field) {

  if (!errors.experiance) return null;

  if (errors.experiance[field]) {
    delete errors.experiance[field];
    this.setState({ errors: { ...this.state.errors,
                              experiance: errors.experiance} })
  }
}

export function clearFieldExperianceErrors(field, value) {
  const errors = this.state.errors;

  if (!errors.experiance) return null;

  if (value.trim() && this.state.errors.experiance && this.state.errors.experiance[field]) {
    const expErr = Object.assign({}, this.state.errors.experiance);
    delete expErr[field];

    if (Object.keys(expErr).length === 0) {
      const otherErr = Object.assign({}, this.state.errors);
      delete otherErr.experiance
      return this.setState({ errors: otherErr });
    } else {
      this.setState({ errors: {...this.state.errors,
                        experiance: expErr
                      }
                    })
    }
  }
}
