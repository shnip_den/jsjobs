const validator = require('validator');
const axios = require('axios');
/*to do: move to config */
const PATH_BASE = 'http://localhost:5000';
const PATH_CHECK_HANDLE = '/api/profile/check-handle';
const PARAM_REQUEST = 'handle=';


const checkField = (field, value, errors) => {
  switch (field) {
    case "handle":
      if(validator.isEmpty(value, {ignore_whitespace: true})) {
        errors.handle = { empty: "Handle cant be empty" }
      }
      break;
    case "account_type":
      if(validator.isEmpty(value, {ignore_whitespace: true})) {
        errors.account_type = { empty: "Account  type cant be empty" }
      }
      break;
    default:
      return { errors: {}, isValid: false }
  }
}

export async function validateAdditionalInfoField(field, value) {
  const errors = {};

  if( field === "handle") {
    if (value.trim() && this.state.errors.handle) this.setState({ errors: {} });

    checkField(field, value, errors);

    await axios(`${PATH_BASE}${PATH_CHECK_HANDLE}?${PARAM_REQUEST}${value}`)
          .then(res => {
            if(!res.data.isFree) {
              return errors.handle = { unique: "The handle already exists"}
            }
          })
          .catch(err => {
            errors.handle = { empty: "Cant check handle. Something went wrong..."}
          })
  }

  if (Object.keys(errors).length > 0) {
    this.setState({
      errors: { ...this.state.errors, [field]: errors[field] }
    });
  }
}

export function validateAdditionalInfoForm(data) {
  const { handle = "", account_type = "" } = data;
  const errors = {};

  checkField.call(this, "handle", handle, errors);
  checkField.call(this, "account_type", account_type, errors);

  const isUnique = !(data.errors.handle);

  const result = { errors, isValid: Object.keys(errors).length === 0 && isUnique };

  return !result.isValid;
}
