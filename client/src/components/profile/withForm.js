import React from 'react';


export const withForm = (Component, initialState, props) => {
  return class FormWrapper extends React.Component {
    state = initialState

    onChange = (e, parent = false, validateCallback = () => {}) => {
      const { name, value } = e.target;

      if(!parent) {
        return this.setState({ [name]: value },
                              () => validateCallback(name, value));
      }

      this.setState({
        [parent]: { ...this.state[parent], [name]: value }
      }, () => validateCallback(name, value));
    }

    handleSpecializationCheckBox = e => {
      this.setState({
        specialization: { ...this.state.specialization,
                          [e.target.name]: e.target.checked
                        }
      })
    }

    /* for date input and tag input */
    handleObjectProp = (value, property, parent) => {
      if(!parent) {
        return this.setState({ [property]: value })
      }

      this.setState({
        [parent]: { ...this.state[parent], [property]: value }
      })
    }

    /* For education and experiance */
    addToArrayState = (value, parent) => {
      this.setState({ [parent]: [...this.state[parent], value] });
    }

    deleteFromArrayState = (value, parent) => {
      this.setState({ [parent]: value });
    }

    /* for personal, education and experiance */
    convertToDate = date => {
      if (typeof date === 'object') return date;
      return new Date(date);
    }

    render() {
      const formHandlers = {
        onChange: this.onChange,
        handleCheckBox: this.handleSpecializationCheckBox,
        handleObjectProp: this.handleObjectProp,
        addToArrayState: this.addToArrayState,
        deleteFromArrayState: this.deleteFromArrayState,
        convertToDate: this.convertToDate
      }

      return (
        <Component profileState={this.state} {...this.props} {...formHandlers} />
      )
    }
  }
}
