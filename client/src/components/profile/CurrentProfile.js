import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getOwnProfile } from '../../redux/actions/profileActions';
import Preloader from '../helpers/Preloader';
import Profile from './Profile';

export class CurrentProfile extends React.Component {
  componentDidMount() {
    if (!this.props.profile.ownProfile) {
      this.props.getOwnProfile();
    }
  }

  render() {
    const { ownProfile, isLoading } = this.props.profile;

    const preLoad = () => {
      if (ownProfile === null || isLoading){
        return <Preloader />
      }
      return null
    }

    return (
      <>
        { preLoad() || <Profile profile={ownProfile} user={this.props.user} />}
      </>
    );
  }
}

CurrentProfile.propTypes = {
  getOwnProfile: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired
};

const mapStateToProps = (state) => {
  return {
    profile: state.profile,
    user: state.auth.user
  }
}

export default connect(mapStateToProps, { getOwnProfile })(CurrentProfile);
