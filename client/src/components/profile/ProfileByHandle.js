import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getProfileByHandle } from '../../redux/actions/profileActions';
import Preloader from '../helpers/Preloader';
import Profile from './Profile';

export class ProfileByHandle extends React.Component {
  componentDidMount() {
    const handle = this.props.computedMatch.params.handle;

    if (handle) return this.props.getProfileByHandle(handle);
  }

  render() {
    const { current, isLoading } = this.props.profile;

    const preLoad = () => {
      if (current === null || isLoading){
        return <Preloader />
      }
      return null
    }

    return (
      <>
        { preLoad() || <Profile profile={current} user={this.props.user} />}
      </>
    );
  }
}

ProfileByHandle.propTypes = {
  getProfileByHandle: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired
};

const mapStateToProps = (state) => {
  return {
    profile: state.profile,
    user: state.auth.user
  }
}

export default connect(mapStateToProps, {getProfileByHandle})(ProfileByHandle);
