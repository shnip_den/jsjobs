import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Education from '../steps/education';
import { validateEducation,
         clearFieldEducationErrors } from '../validations/education';
import { updateEducation } from '../../../redux/actions/profileActions';
import debounce from '../../../utils/debounce'
import { clearReduxErrors } from '../../../redux/actions/errorActions';


export class EducationEdit extends React.Component {
  state = {
    errors: {}
  }

  componentDidUpdate(prevProps) {
    if(prevProps.reduxErrors !== this.props.reduxErrors) {
      this.setState({errors: this.props.reduxErrors})
    }
  }

  componentWillUnmount() {
    const { reduxErrors, clearReduxErrors } = this.props;
    if(Object.keys(reduxErrors).length > 0) clearReduxErrors();
  }

  clearFieldEducationErrors = debounce(clearFieldEducationErrors.bind(this), 400);

  onSubmit = (e) => {
    e.preventDefault();
    if (Object.keys(this.state.errors).length > 0) return null;

    const {education} = this.props.profileState;
    this.props.updateEducation({education});
  }

  render() {
    const { convertToDate,
            addToArrayState,
            deleteFromArrayState,
            profileState } = this.props;

    return (
      <form name="profile-edit" onSubmit = {this.onSubmit}>
        <Education  education = { profileState.education }
                    errors = { this.state.errors.education }
                    addEducation = { addToArrayState }
                    deleteEducation = { deleteFromArrayState }
                    validateEducation = { validateEducation.bind(this) }
                    clearFieldEducationErrors = { this.clearFieldEducationErrors }
                    convertToDate = { convertToDate }
                  />
        <button className="btn btn_pelorous-theme profile-step-creator__btn-group">
          Update
        </button>
      </form>
    );
  }
}

EducationEdit.propTypes = {
  profile: PropTypes.object,
  updateEducation: PropTypes.func.isRequired,
  clearReduxErrors: PropTypes.func.isRequired,
  reduxErrors: PropTypes.object
};

const mapStateToProps = state => {
  return {
    profile: state.profile.ownProfile,
    reduxErrors: state.errors
  }
}

export default connect(mapStateToProps,
                       { updateEducation, clearReduxErrors } )(EducationEdit);
