import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Experiance from '../steps/experiance';
import { validateExperiance,
         clearFieldExperianceErrors } from '../validations/experiance';
import { updateExperiance } from '../../../redux/actions/profileActions';
import debounce from '../../../utils/debounce';
import { clearReduxErrors } from '../../../redux/actions/errorActions';


export class ExperianceEdit extends React.Component {
  state = {
    errors: {}
  }

  componentDidUpdate(prevProps) {
    if(prevProps.reduxErrors !== this.props.reduxErrors) {
      this.setState({errors: this.props.reduxErrors})
    }
  }

  componentWillUnmount() {
    const { reduxErrors, clearReduxErrors } = this.props;
    if(Object.keys(reduxErrors).length > 0) clearReduxErrors();
  }

  clearFieldExperianceErrors = debounce(clearFieldExperianceErrors.bind(this), 400);

  onSubmit = (e) => {
    e.preventDefault();
    if (Object.keys(this.state.errors).length > 0) return null;

    const {experiance} = this.props.profileState;
    this.props.updateExperiance({experiance});
  }

  render() {
    const { convertToDate,
            addToArrayState,
            deleteFromArrayState,
            profileState } = this.props;

    return (
      <form name="profile-edit" onSubmit = {this.onSubmit}>
        <Experiance experiance = { profileState.experiance }
                    errors = { this.state.errors.experiance }
                    addExperiance = { addToArrayState }
                    deleteExperiance = { deleteFromArrayState }
                    validateExperiance = { validateExperiance.bind(this) }
                    clearFieldExperianceErrors = { this.clearFieldExperianceErrors }
                    convertToDate = { convertToDate }
                    />
        <button className="btn btn_pelorous-theme profile-step-creator__btn-group" >
          Update
        </button>
      </form>
    );
  }
}

ExperianceEdit.propTypes = {
  profileState: PropTypes.object,
  updateExperiance: PropTypes.func.isRequired,
  clearReduxErrors: PropTypes.func.isRequired,
  reduxErrors: PropTypes.object
};

const mapStateToProps = state => {
  return {
    reduxErrors: state.errors
  }
}

export default connect(mapStateToProps,
                       { updateExperiance, clearReduxErrors } )(ExperianceEdit);
