import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Contacts from '../steps/contacts';
import { updateContacts } from '../../../redux/actions/profileActions';


export class ContactsEdit extends React.Component {
  state = {
    errors: {}
  }

  onSubmit = e => {
    e.preventDefault();
    if (Object.keys(this.state.errors).length > 0) return null;

    const { contacts } = this.props.profileState;
    this.props.updateContacts({contacts});
  }

  render() {
    const { onChange, profileState } = this.props;

    return (
      <form name="profile-edit" onSubmit = {this.onSubmit}>
        <Contacts onChange = { e => onChange(e, "contacts") }
                  errors = { this.state.errors.contacts }
                  contacts = { profileState.contacts }
                />
        <button className="btn btn_pelorous-theme profile-step-creator__btn-group">
          Update
        </button>
      </form>
    );
  }
}

ContactsEdit.propTypes = {
  profileState: PropTypes.object,
  updateContacts: PropTypes.func.isRequired,
  reduxErrors: PropTypes.object
};

export default connect(null, { updateContacts })(ContactsEdit);
