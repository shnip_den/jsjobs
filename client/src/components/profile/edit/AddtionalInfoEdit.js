import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { AdditionalInfo } from '../steps/additional-information';
import debounce from '../../../utils/debounce';
import { validateAdditionalInfoField,
         validateAdditionalInfoForm } from '../validations/additional-information';
import { updateAdditionalInfo } from '../../../redux/actions/profileActions';
import { clearReduxErrors } from '../../../redux/actions/errorActions';


export class AdditionalInfoEdit extends React.Component {
  state = {
    errors: {}
  }

  componentDidUpdate(prevProps) {
    if(prevProps.reduxErrors !== this.props.reduxErrors) {
      this.setState({errors: this.props.reduxErrors})
    }
  }

  componentWillUnmount() {
    const { reduxErrors, clearReduxErrors } = this.props;
    if(Object.keys(reduxErrors).length > 0) clearReduxErrors();
  }

  validateAdditionalInfoField = debounce(validateAdditionalInfoField.bind(this), 700);

  onSubmit = (e) => {
    e.preventDefault();

    if (Object.keys(this.state.errors).length > 0) return null;
    const {handle, account_type} = this.props.profileState;
    this.props.updateAdditionalInfo({handle, account_type});
  }

  render() {
    const { onChange, profileState } = this.props;
    return (
      <form name="profile-edit" onSubmit = {this.onSubmit}>
        <AdditionalInfo errors = {  this.state.errors }
                        onChange = { (e) => {
                          onChange(e, null,  this.validateAdditionalInfoField)
                        }}
                        handle = { profileState.handle }
                        account_type = { profileState.account_type}/>
        <button className="btn btn_pelorous-theme profile-step-creator__btn-group"
                disabled = { validateAdditionalInfoForm(
                                { errors: this.state.errors,
                                   ...profileState }
                            )}>
          Update
        </button>
      </form>
    );
  }
}

AdditionalInfoEdit.propTypes = {
  profileState: PropTypes.object,
  updateAdditionalInfo: PropTypes.func.isRequired,
  clearReduxErrors: PropTypes.func.isRequired,
  reduxErrors: PropTypes.object
};

const mapStateToProps = state => {
  return {
    reduxErrors: state.errors
  }
}

export default connect(mapStateToProps,
                        { updateAdditionalInfo, clearReduxErrors } )(AdditionalInfoEdit);
