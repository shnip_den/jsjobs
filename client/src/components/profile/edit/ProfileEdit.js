import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Preloader from '../../helpers/Preloader';
import { getOwnProfile } from '../../../redux/actions/profileActions';
import PersonalEdit from './PersonalEdit';
import AdditionalInfoEdit from './AddtionalInfoEdit';
import SpecializationEdit from './SpecializationEdit';
import ExperianceEdit from './ExperianceEdit';
import EducationEdit from './EducationEdit';
import ContactsEdit from './ContactsEdit';
import {withForm} from '../withForm';


export class ProfileEdit extends React.Component {
  state = {
    errors: {},
    step: 1
  }

  componentDidMount() {
    if (!this.props.profile.ownProfile) {
      this.props.getOwnProfile();
    }

    this.handleClickMenu();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.profile.ownProfile === null || prevProps.profile.isLoading) {
      this.handleClickMenu();
    }
    this.setCurrentMenuItem();
  }

  setCurrentMenuItem = () => {
    const menu = document.getElementById('profile__menu');
    if (!menu) return null;

    menu.querySelector('.menu__item_active').classList.toggle('menu__item_active');
    menu.children[this.state.step-1].classList.toggle('menu__item_active');
  }

  handleClickMenu = () => {
    const menu = document.getElementById('profile__menu');
    if (!menu) return null;
    menu.addEventListener('click', this.changeMenuStep);
  }

  changeMenuStep = (e) => {
    if (!e.target.hasAttribute('data-step')) return null;

    const menu = document.getElementById('profile__menu');
    const nextElem = e.target;
    const nextStep = nextElem.dataset.step;
    const activeElem = menu.querySelector('.menu__item_active')
    const activeStep = activeElem.dataset.step;

    if (activeStep === nextStep) return null;

    activeElem.classList.toggle('menu__item_active');
    nextElem.classList.toggle('menu__item_active');
    this.setState({ ...this.state, step: nextStep });
  }

  chooseComponent() {
    const step = parseInt(this.state.step);
    const initialState = this.props.profile.ownProfile;

    switch (step) {
      case 1:
        let PersonalWithForm = withForm(PersonalEdit, initialState)
        return <PersonalWithForm />
      case 2:
        let AdditionalInfoWithForm = withForm(AdditionalInfoEdit, initialState)
        return <AdditionalInfoWithForm />
      case 3:
        let SpecializationWithForm = withForm(SpecializationEdit, initialState)
        return <SpecializationWithForm />
      case 4:
        let ExperianceWithForm = withForm(ExperianceEdit, initialState)
        return <ExperianceWithForm />
      case 5:
        let EducationWithForm = withForm(EducationEdit, initialState)
        return <EducationWithForm />
      case 6:
        let ContactsWithForm = withForm(ContactsEdit, initialState)
        return <ContactsWithForm />
      default:
        return <h2>Something went wrong</h2>
    }
  }

  render() {
    const { ownProfile: profile, isLoading } = this.props.profile;
    const user = this.props.user;

    const preLoad = () => {
      if (profile === null || isLoading){
        return <Preloader />
      }
      return null
    }

    return (
      <>
      { preLoad() ||
      <div className="profile row">
        <div className="col-lg-4">
          <div className="profile__info-sidebar">
            <div className="profile__avatar">
              <img src={user.avatar} alt="Your avatar" className="profile__img" />
            </div>
            <div className="profile__user-info">
              <div className="infoblock__header">
                <div>
                  {`${profile.personal.firstname} ${profile.personal.lastname}`}
                </div>
              </div>
              <div className="text-grey">
                { profile.handle }
              </div>
            </div>

            <div className="profile__buttons infoblock">
              <Link to="/profile">
                <button className="btn btn_pelorous-theme profile__btn">
                  Profile
                </button>
              </Link>
            </div>
          </div>

          <div className="menu profile__menu" id="profile__menu">
            <div className="menu__item menu__item_active" data-step = "1">Personal</div>
            <div className="menu__item" data-step = "2">Additional Information</div>
            <div className="menu__item" data-step = "3">Specialization</div>
            <div className="menu__item" data-step = "4">Experiance</div>
            <div className="menu__item" data-step = "5">Education</div>
            <div className="menu__item" data-step = "6">Contacts</div>
          </div>
        </div>

        <div className="col-lg-8">
          <div className="item-block profile__item-block">
            { this.chooseComponent() }
          </div>
        </div>
      </div>
      }
      </>
    );
  }
}

ProfileEdit.propTypes = {
  getOwnProfile: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired
};

const mapStateToProps = (state) => {
  return {
    profile: state.profile,
    user: state.auth.user
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getOwnProfile: () => dispatch(getOwnProfile())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileEdit);
