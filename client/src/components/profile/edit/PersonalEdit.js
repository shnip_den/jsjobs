import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Personal } from '../steps/personal';
import { validatePersonalField,
         validatePersonalForm } from '../validations/personal';
import debounce from '../../../utils/debounce';
import { updatePersonal } from '../../../redux/actions/profileActions';
import { clearReduxErrors } from '../../../redux/actions/errorActions';


export class PersonalEdit extends React.Component {
  state = {
    errors: {}
  }

  componentDidUpdate(prevProps) {
    if(prevProps.reduxErrors !== this.props.reduxErrors) {
      this.setState({errors: this.props.reduxErrors})
    }
  }

  componentWillUnmount() {
    const { reduxErrors, clearReduxErrors } = this.props;
    if(Object.keys(reduxErrors).length > 0) clearReduxErrors();
  }

  validatePersonalField = debounce(validatePersonalField.bind(this), 500);

  onSubmit = (e) => {
    e.preventDefault();
    if (Object.keys(this.state.errors).length > 0) return null;

    const {personal} = this.props.profileState;
    this.props.updatePersonal({personal});
  }

  render() {
    const { convertToDate, onChange, handleObjectProp, profileState } = this.props;

    return (
      <form name="profile-edit" onSubmit={this.onSubmit}>
        <Personal personal = { profileState.personal }
                  handleChangeDate = { date => {
                    handleObjectProp(date, "birthday", "personal")
                  }}
                  onChange = { (e) => {
                    onChange(e, "personal", this.validatePersonalField)
                  }}
                  errors = { this.state.errors.personal }
                  convertToDate = { convertToDate } />
        <button className="btn btn_pelorous-theme profile-step-creator__btn-group" disabled = { validatePersonalForm(profileState.personal) }>
          Update
        </button>
      </form>
    );
  }
}

PersonalEdit.propTypes = {
  profileState: PropTypes.object,
  updatePersonal: PropTypes.func.isRequired,
  clearReduxErrors: PropTypes.func.isRequired,
  reduxErrors: PropTypes.object
};

const mapStateToProps = (state) => {
  return {
    reduxErrors: state.errors
  }
}

export default connect(mapStateToProps,
                        { updatePersonal, clearReduxErrors })(PersonalEdit);
