import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Specialization } from '../steps/specialization';
import { updateSpecialization } from '../../../redux/actions/profileActions';
import { clearReduxErrors } from '../../../redux/actions/errorActions';


export class SpecializationEdit extends React.Component {
  state = {
    errors: {}
  }

  componentDidUpdate(prevProps) {
    if(prevProps.reduxErrors !== this.props.reduxErrors) {
      this.setState({errors: this.props.reduxErrors})
    }
  }

  componentWillUnmount() {
    const { reduxErrors, clearReduxErrors } = this.props;
    if(Object.keys(reduxErrors).length > 0) clearReduxErrors();
  }

  onSubmit = (e) => {
    e.preventDefault();

    if (Object.keys(this.state.errors).length > 0) return null;
    const {specialization} = this.props.profileState;
    this.props.updateSpecialization({specialization});
  }

  render() {
    const { onChange,
            handleCheckBox,
            handleObjectProp,
            profileState } = this.props;

    return (
      <form name="profile-edit" onSubmit = {this.onSubmit}>
        <Specialization errors= { this.state.errors.specialization }
                        onChange = { e => onChange(e, "specialization") }
                        specialization = { profileState.specialization }
                        handleCheckBox = { handleCheckBox }
                        handleTagField = {
                          e => handleObjectProp(e, "skills", "specialization")
                        }
                      />
        <button className="btn btn_pelorous-theme profile-step-creator__btn-group" >
          Update
        </button>
      </form>
    );
  }
}

SpecializationEdit.propTypes = {
  profileState: PropTypes.object,
  updateSpecialization: PropTypes.func.isRequired,
  clearReduxErrors: PropTypes.func.isRequired,
  reduxErrors: PropTypes.object.isRequired
};

const mapStateToProps = state => {
  return {
    reduxErrors: state.errors
  }
}

export default connect(mapStateToProps,
                        { updateSpecialization, clearReduxErrors } )(SpecializationEdit);
