import React from 'react';
import { connect } from 'react-redux';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import Experiance from '../steps/experiance';
import Education from '../steps/education';
import Contacts from '../steps/contacts';
import { Progressbar } from './Progressbar';
import { Personal } from '../steps/personal';
import { AdditionalInfo } from '../steps/additional-information';
import { Specialization } from '../steps/specialization';
import { validatePersonalField,
         validatePersonalForm,
         clearPersonalErrors } from '../validations/personal';
import { validateAdditionalInfoField,
         validateAdditionalInfoForm } from '../validations/additional-information';
import { validateExperiance,
         clearFieldExperianceErrors,
         clearExperianceErrors } from '../validations/experiance';
import { validateEducation,
         clearEducationErrors,
         clearFieldEducationErrors} from '../validations/education';
import { withRouter } from 'react-router-dom';
import { createProfile, getOwnProfile } from '../../../redux/actions/profileActions';
import { withForm } from '../withForm';
import debounce from '../../../utils/debounce';


export class ProfileCreate extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      step: 1,
      errors: {}
    }

    this.prevStep = this.prevStep.bind(this);
    this.nextStep = this.nextStep.bind(this);

    this.onSubmit = this.onSubmit.bind(this);
    this.validatePersonalField = debounce(validatePersonalField.bind(this), 400);

    this.validateAdditionalInfoField = debounce(validateAdditionalInfoField.bind(this), 500);

    this.clearFieldExperianceErrors = debounce(clearFieldExperianceErrors.bind(this), 400);

    this.clearFieldEducationErrors = debounce(clearFieldEducationErrors.bind(this), 400);

    this.checkForm = this.checkForm.bind(this);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (Object.keys(nextProps.reducerErrors).length > 0) {
      return { errors: nextProps.reducerErrors };
    }

    return null;
  }

  componentDidMount() {
    if(!this.props.profile.ownProfile) this.props.getOwnProfile();

    this.clearFieldErrors();
  }

  componentDidUpdate() {
    if(this.props.profile.ownProfile) {
      window.location.pathname = "/profile";
    }
    this.clearFieldErrors();
  }

  componentWillUnmount() {

  }

  prevStep(e) {
    e.preventDefault();
    const {step} = this.state;

    if (step === 1) return null;

    this.toggleProgressbarStep(step-1);

    this.setState( (previousState, props) => {
      return { ...previousState, step: previousState.step - 1 }
    })
  }

  nextStep(e) {
    e.preventDefault();
    const { step } = this.state;

    this.toggleProgressbarStep(step);

    this.setState( (previousState, props) => {
      return { ...previousState, step: previousState.step + 1 }
    })
  }

  toggleProgressbarStep(step) {
    const progressbar = document.getElementById("progressbar");
    if (!progressbar) return null;

    const lis = progressbar.children;
    lis[step].classList.toggle('progressbar__step_active');
  }

  checkForm() {
    const { step } = this.state;
    const { profileState: profile } = this.props
    switch (step) {
      case 1:
        return validatePersonalForm(profile.personal)
      case 2:
        return validateAdditionalInfoForm({errors: this.state.errors, ...profile})
      default:
        return false;
    }
  }

  clearFieldErrors() {
    const form = document.forms["profile-step-creator"];

    if (!form) return null;

    form.addEventListener("focus", (e) => {
      const { step, errors } = this.state;
      const field = e.target.name;
      const value = e.target.value;

      switch (step) {
        case 1:
          clearPersonalErrors.call(this, errors, field);
          break;
        case 2:
          if (errors.handle && !value.trim()) {
            delete errors.handle;
            this.setState({ errors });
          }
          break;
        case 4:
          clearExperianceErrors.call(this, errors, field);
          break;
        case 5:
          clearEducationErrors.call(this, errors, field);
          break;
        default:
          return null;
      }
    }, true)
  }

  onSubmit(event) {
    event.preventDefault();
    const { errors } = this.state;
    const { profileState: profile } = this.props;

    if (Object.keys(errors).length > 0) {
      let errorStepName = Object.keys(errors)[0];

      if (errorStepName === "personal") {
        this.rollbackProgressbar(2);
        return this.setState({ step: 1 });
      }

      if (errorStepName === "handle" || errorStepName === "account_type") {
        this.rollbackProgressbar(3);
        return this.setState({ step: 2 })
      }
    }

    this.props.createProfile(profile, this.props.history);
  }

  rollbackProgressbar(step) {
    while(step <= 6) {
      this.toggleProgressbarStep(step-1)
      step++
    }
  }

  render() {
    const { step } = this.state;
    const { personal, handle, account_type, specialization, experiance, education, contacts } = this.props.profileState;
    const {
      onChange,
      handleCheckBox,
      handleObjectProp,
      addToArrayState,
      deleteFromArrayState,
      convertToDate
    } = this.props;

    const stepsSwitcher = (step) => {
      switch (step) {
        case 1:
          return <Personal  errors = { this.state.errors.personal }
                            onChange = { e => onChange(e, "personal", this.validatePersonalField)
                            }
                            personal = { personal }
                            handleChangeDate = {date =>
                              handleObjectProp(date, "birthday", "personal")}
                            convertToDate = { convertToDate }
                          />
        case 2:
          return  <AdditionalInfo errors = { this.state.errors }
                                  onChange = { e =>
                                    onChange(e, null, this.validateAdditionalInfoField)
                                  }
                                  handle = {handle}
                                  account_type = {account_type}
                                />
        case 3:
          return <Specialization  errors= { this.state.errors.specialization }
                                  onChange = { e => onChange(e, "specialization") }
                                  specialization = { specialization }
                                  handleCheckBox = { handleCheckBox }
                                  handleTagField = { e =>
                                    handleObjectProp(e, "skills", "specialization")
                                  }
                                />
        case 4:
          return <Experiance  experiance = { experiance }
                              errors = { this.state.errors.experiance }
                              addExperiance = { addToArrayState }
                              deleteExperiance = { deleteFromArrayState }
                              convertToDate = { convertToDate }
                              validateExperiance = {
                                                 validateExperiance.bind(this)
                                                 }
                              skip = { true }
                              clearFieldExperianceErrors = { this.clearFieldExperianceErrors }
                            />
        case 5:
          return <Education education = { education }
                            errors = { this.state.errors.education }
                            addEducation = { addToArrayState }
                            deleteEducation = { deleteFromArrayState }
                            convertToDate = { convertToDate }
                            validateEducation = { validateEducation.bind(this) }
                            skip = { true }
                            clearFieldEducationErrors = { this.clearFieldEducationErrors }
                          />
        case 6:
          return <Contacts  onChange = { onChange }
                            errors = { this.state.errors.contacts }
                            contacts = { contacts }
                            skip = { true }
                          />
        default:
          return (
            <h2>Something went wrong...</h2>
          )
      }
    }

    return (
        <div className="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 profile-step-creator">
          <Progressbar />

          <TransitionGroup className="animation">
            <CSSTransition  key = { step }
                            appear = { true }
                            timeout = { 1000 }
                            classNames = "animation_slide">

            <form name = 'profile-step-creator' className = "profile-step-creator__form">
              { stepsSwitcher(step) }
              <div className="profile-step-creator__btn-group">
                { step !== 1 ?
                    <button className="btn btn_pelorous-theme profile-step-creator__btn" onClick= { this.prevStep } type="submit"> Back </button>
                  : null
                }

                { step !== 6 ?
                  <button className="btn btn_pelorous-theme profile-step-creator__btn profile-step-creator__btn_right"
                          onClick= { this.nextStep }
                          disabled = { this.checkForm() }
                          type="submit">
                    Next
                  </button>
                :
                  <button className="btn btn_pelorous-theme profile-step-creator__btn profile-step-creator__btn_right"
                          onClick= { this.onSubmit }
                          type="submit">
                    Create
                  </button>
                }
              </div>
            </form>
            </CSSTransition>
          </TransitionGroup>
        </div>

    )
  }
}

const mapStateToProps = state => {
  return {
    reducerErrors: state.errors,
    auth: state.auth,
    profile: state.profile
  }
};

const mapDispatchToProps = dispatch => {
  return {
    createProfile: (data, history) => dispatch(createProfile(data, history)),
    getOwnProfile: () => dispatch(getOwnProfile())
  };
}

const initialState = {
  personal: {
    firstname: "",
    lastname: "",
    gender: "male",
    birthday: new Date()
  },
  handle: "",
  account_type: "personal",
  specialization: {
    skills: [],
    status: "Not looking for a job",
    qualification: "Intern",
    readyToRelocate: false,
    remoteJob: false
  },
  experiance: [],
  education: [],
  contacts: {}
}

export default connect(mapStateToProps,
                        mapDispatchToProps)       (withRouter(withForm(ProfileCreate, initialState)));
