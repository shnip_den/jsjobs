import React from 'react';

export const Progressbar = () => {
  return (
    <ul className = "progressbar profile-step-creator__progressbar"
        id = "progressbar">
      <li className = "progressbar__step progressbar__step_active">Personal</li>
      <li className = "progressbar__step">Additional information</li>
      <li className = "progressbar__step">Specialization</li>
      <li className = "progressbar__step">Experiance</li>
      <li className = "progressbar__step">Education</li>
      <li className = "progressbar__step">Contacts</li>
    </ul>
  );
}
