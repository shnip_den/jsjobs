import React from 'react';
import PropTypes from 'prop-types';
import formatDate from '../../utils/formatDate';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUniversity } from '@fortawesome/free-solid-svg-icons';


const EducationList = (props) => {
  const eduList = props.eduList;

  return (
    <ul className="ul" id = "eduList">
      { eduList.map( (item, index) => {
          return (
            <li className = "item-list profile__item-list" key = { index } data-index = { index }>
              <div className = "item-list__header">
                <small className = "text-grey">
                  { formatDate(item.from) } -
                  { formatDate(item.to) }
                </small>
              </div>
              <div className="item-list__container">
                <div className="item-list__icon">
                  <FontAwesomeIcon icon={faUniversity} size="2x"/>
                </div>
                <div className="item-list__content">
                  <div><strong>{ item.schoolTitle }</strong></div>
                  <small>{ item.location }</small>
                </div>
              </div>
            </li>
          )
      })}
    </ul>
  );
}

EducationList.propTypes = {
  eduList: PropTypes.array
};

export default EducationList;
