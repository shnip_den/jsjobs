import React from 'react';
import Field from '../../helpers/forms/field';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimesCircle } from '@fortawesome/free-regular-svg-icons';
import formatDate from '../../../utils/formatDate';

const initialState = {
  position: "",
  company: "",
  from: new Date(),
  to: new Date()
};

class Experiance extends React.Component {
  constructor(props) {
    super(props)

    this.state = initialState;

    this.onSubmit = this.onSubmit.bind(this);
    this.deleteItem = this.deleteItem.bind(this);
  }

  componentDidMount() {
    const ul = document.getElementById('experiance');
    ul.addEventListener('click', this.deleteItem);
  }

  onSubmit(e) {
    e.preventDefault();

    const experiance = this.props.validateExperiance(this.state);

    if(!experiance.isValid) return false;

    this.props.addExperiance(this.state, "experiance");
    this.setState(initialState);
  }

  deleteItem(e) {
    const ul = e.currentTarget;
    let target = e.target;

    while (target !== ul) {
      if (target.hasAttribute('data-item-delete')) {
        const experiance = this.props.experiance;
        const li = target.closest('li');
        const index = li.dataset.index;
        experiance.splice(index, 1);
        return this.props.deleteExperiance(experiance, "experiance");
      }
      target = target.parentNode;
    }
  }

  onChange = e => {
    const {name, value} = e.target;
    this.setState({ [name]: value }, this.props.clearFieldExperianceErrors)
  }

  handleDate = (date, field) => {
    this.setState({ [field]: date })
  }

  render() {
    const { experiance, errors = {}, convertToDate, skip = false } = this.props;
    const { position, company, from, to } = this.state;

    return (
      <React.Fragment>
        { skip &&
          <small className = "profile-step-creator__notice">
            You can skip this step and fill in later.
          </small>
        }
        <h2 className="profile-step-creator__title"> Experiance </h2>
        <hr className="hr form__hr"/>
        <div className="experiance">
          <div className="experiance__form">
            <Field  component = "input"
                    name = "position"
                    id = "position"
                    className = "input form__input"
                    value = { position }
                    placeholder = "Your position in company"
                    onChange = { this.onChange }
                    classNamesParams ={ { "input__invalid-input": errors.position } }
                    errors = { errors.position }
                    classLabel = "label form__label"
                  />

            <Field  component = "input"
                    name = "company"
                    id = "company"
                    className = "input form__input"
                    value = { company }
                    placeholder = "Company name"
                    onChange = { this.onChange }
                    classNamesParams ={ { "input__invalid-input": errors.company } }
                    errors = { errors.company }
                    classLabel = "label form__label"
                  />

            <div className="form-group">
              <div className="form-group__input-group">
                <Field  component = "input"
                        type = "date"
                        name = "from"
                        id = "from"
                        className = "input form__input"
                        value = { convertToDate(from) }
                        onChange = { date => this.handleDate(date, "from") }
                        classNamesParams ={ { "input__invalid-input": errors.from } }
                        errors = { errors.from }
                        classLabel = "label form__label form-group__label"
                />
              </div>
              <div className="form-group__input-group">
                <Field  component = "input"
                        type = "date"
                        name = "to"
                        id = "to"
                        className = "input form__input"
                        value = { convertToDate(to) }
                        onChange = { date => this.handleDate(date, "to") }
                        classNamesParams ={ { "input__invalid-input": errors.to } }
                        errors = { errors.to }
                        classLabel = "label form__label form-group__label"
                />
              </div>
            </div>
            <button onClick = { this.onSubmit }
                    className = "btn btn_purple-theme">
              Create experiance
            </button>
          </div>

          <ul className="ul" id = "experiance">
            <h3 className = "profile__list-title">Experiance list</h3>
            <hr className = "profile__hr"/>
            { experiance.map( (item, index) => {
                return (
                  <li className = "profile__item profile_slide-animation"
                      key = { index }
                      data-index = { index }>
                    <div className = "profile__item-header">
                      <small className = "text-grey">
                        { formatDate(item.from) } -
                        { formatDate(item.to) }
                      </small>
                      <span data-item-delete>
                        <FontAwesomeIcon icon={ faTimesCircle } size="1x" />
                      </span>
                    </div>
                    <div><strong>{ item.company }</strong></div>
                    <small>{ item.position }</small>
                  </li>
                )
            })}
          </ul>

        </div>
      </React.Fragment>
    );
  }

}

export default Experiance;
