import React from 'react';
import PropTypes from 'prop-types';
import Field from '../../helpers/forms/field';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimesCircle } from '@fortawesome/free-regular-svg-icons';
import formatDate from '../../../utils/formatDate';

const initialState = {
  schoolTitle: "",
  location: "",
  from: new Date(),
  to: new Date()
};

class Education extends React.Component {
  constructor(props) {
    super(props);

    this.state = initialState;

    this.onSubmit = this.onSubmit.bind(this);
    this.deleteItem = this.deleteItem.bind(this);
  }

  componentDidMount() {
    const ul = document.getElementById('education');
    ul.addEventListener('click', this.deleteItem);
  }

  onSubmit(e) {
    e.preventDefault();

    const education = this.props.validateEducation(this.state);

    if(!education.isValid) return false;

    this.props.addEducation(this.state, "education");
    this.setState(initialState);
  }

  deleteItem(e) {
    const ul = e.currentTarget;
    let target = e.target;

    while (target !== ul) {
      if (target.hasAttribute('data-item-delete')) {
        const education = this.props.education;
        const li = target.closest('li');
        const index = li.dataset.index;
        education.splice(index, 1);
        return this.props.deleteEducation(education, "education");
      }
      target = target.parentNode;
    }
  }

  onChange = e => {
    const {name, value} = e.target;
    this.setState({ [name]: value }, this.props.clearFieldEducationErrors)
  }

  handleDate = (date, field) => {
    this.setState({ [field]: date })
  }

  render() {
    const { schoolTitle, location, from, to } = this.state;
    const { education, errors = {}, convertToDate, skip = false } = this.props;

    return (
      <React.Fragment>
        { skip &&
          <small className = "profile-step-creator__notice">
            You can skip this step and fill in later.
          </small>
        }
        <h2 className="profile-step-creator__title">Education</h2>
        <hr className="hr form__hr"/>

        <div className="education">
          <div className="education__form">
            <Field  component = "input"
                    name = "schoolTitle"
                    id = "schoolTitle"
                    className = "input form__input"
                    value = { schoolTitle }
                    placeholder = "Name of educational institution"
                    onChange = { this.onChange }
                    classNamesParams ={ { "input__invalid-input": errors.schoolTitle } }
                    errors = { errors.schoolTitle }
                    classLabel = "label form__label"
                    labelText = "Title"
                  />

            <Field  component = "input"
                    name = "location"
                    id = "location"
                    className = "input form__input"
                    value = { location }
                    placeholder = "place where you studied"
                    onChange = { this.onChange }
                    classNamesParams ={ { "input__invalid-input": errors.location } }
                    errors = { errors.location }
                    classLabel = "label form__label"
                  />
            <div className="form-group">
              <div className="form-group__input-group">
                <Field  component = "input"
                        type = "date"
                        name = "from"
                        id = "from"
                        className = "input form__input"
                        value = { convertToDate(from) }
                        onChange = { date => this.handleDate(date, "from") }
                        classNamesParams ={ { "input__invalid-input": errors.from } }
                        errors = { errors.from }
                        classLabel = "label form__label"
                />
              </div>
              <div className="form-group__input-group">
                <Field  component = "input"
                        type = "date"
                        name = "to"
                        id = "to"
                        className = "input form__input"
                        value = { convertToDate(to) }
                        onChange = { date => this.handleDate(date, "to") }
                        classNamesParams ={ { "input__invalid-input": errors.to } }
                        errors = { errors.to }
                        classLabel = "label form__label"
                />
              </div>
            </div>
            <button onClick = { this.onSubmit }
                    className = "btn btn_purple-theme">
              Create education
            </button>
          </div>

          <ul className="ul" id = "education">
            <h3 className = "profile__list-title">Education list</h3>
            <hr className = "profile__hr"/>
            { education.map( (item, index) => {
                return (
                  <li className = "profile__item profile_slide-animation"
                      key = { index }
                      data-index = { index }>
                    <div className = "profile__item-header">
                      <small className = "text-grey">
                        { formatDate(item.from) } -
                        { formatDate(item.to) }
                      </small>
                      <span data-item-delete>
                        <FontAwesomeIcon icon={ faTimesCircle } size="1x" />
                      </span>
                    </div>
                    <div><strong>{ item.schoolTitle }</strong></div>
                    <small>{ item.location }</small>
                  </li>
                )
            })}
          </ul>
        </div>
      </React.Fragment>
    );
  }
}

Education.propTypes = {
  education: PropTypes.array,
  errors: PropTypes.object,
  formatDate: PropTypes.func,
  onChange: PropTypes.func,
  handleObjectProp: PropTypes.func
}

export default Education;
