import React from 'react';
import PropTypes from 'prop-types';
import Field from '../../helpers/forms/field';


export const Personal = (props) => {
  const { firstname, lastname, gender, birthday, location, bio, website } = props.personal;

  const { onChange, handleChangeDate, convertToDate, errors = {} } = props;

  return (
        <React.Fragment>
          <h2 className="profile-step-creator__title"> Personal information </h2>
          <hr className="hr form__hr"/>

          <Field  component = "input"
            name = "firstname"
            id = "firstname"
            className = "input form__input"
            value = { firstname }
            placeholder = "Firstname"
            onChange = { onChange }
            classNamesParams ={ { "input__invalid-input": errors.firstname } }
            required = { true }
            errors = { errors.firstname }
            classLabel = "label label_required form__label"
          />

          <Field  component = "input"
                  name = "lastname"
                  id = "lastname"
                  className = "input form__input"
                  value = { lastname }
                  placeholder = "Lastname"
                  onChange = { onChange }
                  classNamesParams ={ { "input__invalid-input": errors.lastname } }
                  required = { true }
                  errors = { errors.lastname }
                  classLabel = "label label_required form__label"
          />
          <div className="form-group">
            <div className="form-group__input-group">
              <Field  component = "select"
                      name = "gender"
                      className ="input form__input"
                      classLabel = "label form__label form-group__label"
                      options = { [["male", "male"], ["female", "female"]] }
                      onChange = { onChange }
                      value = { gender }
              />
            </div>

            <div className="form-group__input-group">
              <Field  component = "input"
                      type = "date"
                      name = "birthday"
                      id = "birthday"
                      className = "input form__input"
                      value = { convertToDate(birthday) }
                      placeholder = "Birthday"
                      onChange = { handleChangeDate }
                      classNamesParams ={ { "input__invalid-input": errors.birthday } }
                      errors = { errors.birthday }
                      classLabel = "label form__label form-group__label"
              />
            </div>

          </div>

          <Field  component = "input"
                  name = "location"
                  id = "location"
                  className = "input form__input"
                  value = { location }
                  placeholder = "Location"
                  onChange = { onChange }
                  classNamesParams ={ { "input__invalid-input": errors.location } }
                  errors = { errors.location }
                  classLabel = "label form__label"
          />

          <Field  component = "textarea"
                  id = "bio"
                  name = "bio"
                  cols = "30"
                  rows =  "5"
                  maxLength = "500"
                  className = "input form__input"
                  classLabel = "label form__label"
                  classNamesParams ={ { "input__invalid-input": errors.bio } }
                  onChange = { onChange }
                  value = { bio }
                  />

          <Field  component = "input"
                  name = "website"
                  id = "website"
                  className ="input form__input"
                  value = { website }
                  placeholder = "Website"
                  onChange = { onChange }
                  classNamesParams = { { "input__invalid-input": errors.website } }
                  errors = { errors.website }
                  classLabel = "label form__label"
          />

        </React.Fragment>

  );
}

Personal.propTypes = {
  firstname: PropTypes.string,
  lastname: PropTypes.string,
  gender: PropTypes.string,
  birthday: PropTypes.object,
  location: PropTypes.string,
  bio: PropTypes.string,
  website: PropTypes.string,
  errors: PropTypes.object,
  onChange: PropTypes.func,
  handleChangeDate: PropTypes.func
}
