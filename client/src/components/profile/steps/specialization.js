import React from 'react';
import PropTypes from 'prop-types';
import Field from '../../helpers/forms/field';
import TagsInput from 'react-tagsinput';

import 'react-tagsinput/react-tagsinput.css';

export const Specialization = (props) => {
  const { status, skills = [], qualification, remoteJob, readyToRelocate } = props.specialization;
  const { errors = {}, handleCheckBox, handleTagField, onChange, skip = false } = props;

  return (
    <React.Fragment>
      { skip &&
        <small className = "profile-step-creator__notice">
          You can skip this step and fill in later.
        </small>
      }
      <h2 className="profile-step-creator__title">Specialization</h2>
      <hr className="hr form__hr"/>

      <Field  component = "select"
              name = "status"
              id = "status"
              options = {[
                ["Not looking for a job", "Not looking for a job"],
                ["Looking for a job", "Looking for a job"],
                ["Open to suggestions", "Open to suggestions"]
              ]}
              className = "input form__input"
              classLabel = "label form__label"
              value = { status }
              onChange = { onChange }
              errors = { errors.status }
      />

      <label htmlFor = "skills" className = "label form__label">
        Your skills
      </label>
      <small>Write a tag and press Enter or Tab to add it to the field</small>
      <TagsInput  onChange = { handleTagField }
                  value = { skills }
                  className = "tagsinput"
                  inputProps = {{
                      className: 'tagsinput__input',
                      placeholder: 'Add a tag'
                  }}
                  focusedClassName = "tagsinput_focused"
                />

      <Field  component = "select"
              name = "qualification"
              id = "qualification"
              className = "input form__input"
              classLabel = "label form__label"
              errors = { errors.qualification }
              value = { qualification }
              onChange = { onChange }
              options = {[
                ["Intern", "Intern"],
                ["Junior", "Junior"],
                ["Middle", "Middle"],
                ["Senior", "Senior"],
                ["Lead", "Lead"]
              ]}
      />

      <div>
        <Field  component = "input"
                type = "checkbox"
                name = "remoteJob"
                labelText = "Ready to work remotely"
                id = "remoteJob"
                className = "form__input"
                checked = { remoteJob }
                onChange = { handleCheckBox }
                classLabel = "label form__label"
        />
      </div>

      <div>
        <Field  component = "input"
                type = "checkbox"
                name = "readyToRelocate"
                labelText = "Ready to relocate"
                id = "readyToRelocate"
                className = "form__input"
                checked = { readyToRelocate }
                onChange = { handleCheckBox }
                classLabel = "label form__label"
        />
      </div>

    </React.Fragment>
  );
}

Specialization.propTypes = {
  status: PropTypes.string,
  skills: PropTypes.array,
  qualification: PropTypes.string,
  remoteJob: PropTypes.bool,
  readyToRelocate: PropTypes.bool,
  onChange: PropTypes.func,
  errors: PropTypes.object
};
