import React from 'react';
import PropTypes from 'prop-types';
import Field from '../../helpers/forms/field';

export const AdditionalInfo = (props) => {
  const { errors, handle, account_type, onChange } = props;

  return (
    <React.Fragment>
      <h2 className="profile-step-creator__title"> Additional information </h2>
      <hr className="hr form__hr"/>

      <Field  component = "input"
              name = "handle"
              id = "handle"
              className = "input form__input"
              value = { handle }
              placeholder = "Handle"
              onChange = { onChange }
              classNamesParams ={ { "input__invalid-input": errors.handle } }
              required = { true }
              errors = { errors.handle }
              classLabel = "label label_required form__label"
      />

      <Field  component = "select"
              name = "account_type"
              id = "account_type"
              className = "input form__input"
              classLabel = "label label_required form__label"
              value = { account_type }
              onChange = { onChange }
              classNamesParams ={ { "input__invalid-input": errors.account_type } }
              required = { true }
              errors = { errors.account_type }
              options = { [["personal", "personal"], ["company", "company"]] }
      />
    </React.Fragment>
  );
}

AdditionalInfo.propTypes = {
  errors: PropTypes.object,
  handle: PropTypes.string,
  account_type: PropTypes.string,
  onChange: PropTypes.func
};
