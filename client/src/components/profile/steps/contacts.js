import React from 'react';
import PropTypes from 'prop-types';
import Field from '../../helpers/forms/field';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPhone } from '@fortawesome/free-solid-svg-icons';
import { faEnvelope } from '@fortawesome/free-regular-svg-icons';
import { faTwitter, faFacebook, faInstagram, faGithub } from '@fortawesome/free-brands-svg-icons';


const Contacts = (props) => {
  const { contacts = {}, errors ={}, skip = false } = props;
  const { phone, email, facebook, twitter, instagram, github } = contacts;

  const onChange = (e) => props.onChange(e, "contacts");

  return (
    <React.Fragment>
      { skip &&
        <small className = "profile-step-creator__notice">
          You can skip this step and fill in later.
        </small>
      }
      <h2 className="profile-step-creator__title">Contacts</h2>
      <hr className="hr form__hr"/>
      <Field  component = "input"
              name = "phone"
              id = "phone"
              className = "input form__input"
              value = { phone }
              placeholder = "Your phone"
              onChange = { onChange }
              classNamesParams ={ { "input__invalid-input": errors.phone } }
              errors = { errors.phone }
              classLabel = "label form__label"
              icon = { <FontAwesomeIcon icon={faPhone} className="profile__contact-icon" />}
            />

      <Field  component = "input"
              type = "email"
              name = "email"
              id = "email"
              className = "input form__input"
              value = { email }
              placeholder = "Email"
              onChange = { onChange }
              classNamesParams ={ { "input__invalid-input": errors.email } }
              errors = { errors.email }
              classLabel = "label form__label"
              icon = { <FontAwesomeIcon icon={faEnvelope} className="profile__contact-icon" />}
            />

      <Field  component = "input"
              name = "facebook"
              id = "facebook"
              className = "input form__input"
              value = { facebook }
              placeholder = "Facebook"
              onChange = { onChange }
              classNamesParams ={ { "input__invalid-input": errors.facebook } }
              errors = { errors.facebook }
              classLabel = "label form__label"
              icon = { <FontAwesomeIcon icon={faFacebook} className="profile__contact-icon" />}
            />

      <Field  component = "input"
              name = "twitter"
              id = "twitter"
              className = "input form__input"
              value = { twitter }
              placeholder = "Twitter"
              onChange = { onChange }
              classNamesParams ={ { "input__invalid-input": errors.twitter } }
              errors = { errors.twitter }
              classLabel = "label form__label"
              icon = { <FontAwesomeIcon icon={faTwitter} className="profile__contact-icon" />}
            />

      <Field  component = "input"
              name = "instagram"
              id = "instagram"
              className = "input form__input"
              value = { instagram }
              placeholder = "Instagram"
              onChange = { onChange }
              classNamesParams ={ { "input__invalid-input": errors.instagram } }
              errors = { errors.instagram }
              classLabel = "label form__label"
              icon = { <FontAwesomeIcon icon={faInstagram} className="profile__contact-icon" />}
            />

      <Field  component = "input"
              name = "github"
              id = "github"
              className = "input form__input"
              value = { github }
              placeholder = "Github"
              onChange = { onChange }
              classNamesParams ={ { "input__invalid-input": errors.github } }
              errors = { errors.github }
              classLabel = "label form__label"
              icon = { <FontAwesomeIcon icon={faGithub} className="profile__contact-icon" />}
            />

    </React.Fragment>
  );
}

Contacts.propTypes = {
  phone: PropTypes.string,
  email: PropTypes.string,
  facebook: PropTypes.string,
  twitter: PropTypes.string,
  instagram: PropTypes.string,
  github: PropTypes.string
};

export default Contacts;
