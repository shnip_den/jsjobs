import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Preloader from '../../helpers/Preloader';
import { getProfiles } from '../../../redux/actions/profileActions';
import ProfileList from './ProfileList';
import SearchForm from '../../search/Search';

export class Profiles extends React.Component {
  state = {
    filterText: ''
  }

  componentWillMount() {
    this.props.getProfiles();
  }

  filter = (filterText) => {
    if (!filterText) return this.props.profiles;

    const profiles = this.props.profiles;
    filterText = filterText.toLowerCase();

    const updated = profiles.filter(profile => {
      const { firstname, lastname } = profile.personal;
      return `${firstname} ${lastname} ${profile.handle}`.toLowerCase().includes(filterText)
    })

    return updated;
  }

  handleChange = e => {
    this.setState({ filterText: e.target.value })
  }

  render() {
    const { profiles, isLoading, user } = this.props;
    const filterText = this.state.filterText;
    const preLoad = () => {
      if (profiles === null || isLoading){
        return <Preloader />
      }
      return null
    }

    const filteredProfiles = this.filter(filterText);
    return (
      <>
        { preLoad() ||
          <div className="row profiles">
            <div className="col-lg-8 offset-lg-2">
              <h2>Profiles</h2>
              <SearchForm onChange={this.handleChange} value={filterText}
                          placeholder = "search by handle or name" />
              <ProfileList profiles={filteredProfiles} user={user}/>
            </div>
          </div>
        }
      </>
    );
  }
}

Profiles.propTypes = {
  profiles: PropTypes.array
};

const mapStateToProps = (state) => {
  return {
    profiles: state.profile.profiles,
    isLoading: state.profile.isLoading,
    user: state.auth.user
  }
}

export default connect(mapStateToProps, { getProfiles })(Profiles);
