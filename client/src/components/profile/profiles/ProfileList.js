import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';


const ProfileList = (props) => {
  const { profiles, user } = props;

  return (
    <div className="profiles__list">
      <ul className="ul">
        {
          profiles.map(profile => {
            return (
              <li className="profiles__item" key={profile._id}>
                <div className="profiles__user-info">
                  <img src={profile.user.avatar} alt="avatar" className="img-circle profiles__avatar"/>
                </div>
                <div className="profiles__content">
                  <h3 className="profiles__username">
                    <Link to={`/profile/${profile.handle}`}>
                      {`${profile.personal.firstname} ${profile.personal.lastname}`}
                    </Link>
                  </h3>
                  <div className="profiles__description">
                    { profile.personal.bio }
                  </div>
                  <div className="profiles__description">
                    { profile.personal.location }
                  </div>
                  <div className="profiles__description">
                    <ul className="ul">
                      {
                        profile.specialization.skills.map((item, index) => {
                          return (
                            <li className="tag" key={index}> {item} </li>
                          )
                        })
                      }
                    </ul>
                  </div>
                </div>
                <div className="profiles__buttons">
                  <Link to={`/profile/${profile.handle}`}>
                    <button className="btn btn_pelorous-theme profiles__btn">View</button>
                  </Link>
                  { user.id !== profile.user._id &&
                    <Link to={{ pathname: '/messages', query: { to: profile._id } }}>
                      <button className="btn btn_pelorous-theme profiles__btn">
                        Send Message
                      </button>
                    </Link>
                  }
                </div>
              </li>
            )
          })
        }
      </ul>
    </div>
  );
}

ProfileList.propTypes = {
  profiles: PropTypes.array,
  user: PropTypes.object
};

export default ProfileList;
