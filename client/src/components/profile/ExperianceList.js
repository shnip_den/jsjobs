import React from 'react';
import PropTypes from 'prop-types';
import formatDate from '../../utils/formatDate';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBriefcase} from '@fortawesome/free-solid-svg-icons';


const ExperianceList = (props) => {
  const expList = props.expList;

  return (
    <ul className="ul" id = "expList">
      { expList.map( (item, index) => {
          return (
            <li className = "item-list profile__item-list" key = { index } data-index = { index }>
              <div className = "item-list__header">
                <small className = "text-grey">
                  { formatDate(item.from) } -
                  { formatDate(item.to) }
                </small>
              </div>
              <div className="item-list__container">
                <div className="item-list__icon">
                  <FontAwesomeIcon icon={faBriefcase} size="2x"/>
                </div>
                <div className="item-list__content">
                  <div><strong>{ item.company }</strong></div>
                  <small>{ item.position }</small>
                </div>
              </div>
            </li>
          )
      })}
    </ul>
  );
}

ExperianceList.propTypes = {
  expList: PropTypes.array
};

export default ExperianceList;
