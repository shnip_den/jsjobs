import React from 'react';
import PropTypes from 'prop-types';
import ExperianceList from './ExperianceList';
import EducationList from './EducationList';

export const PersonalTypeProps = (props) => {
  const profile = props.profile;

  return (
    <>
    {
      profile.specialization.skills.length > 0 ?

      <div className="item-block profile__item-block">
        <div className="item-block__header">
          <h3 className="item-block__h3">Skills</h3>
          <hr className="item-block__hr"/>
        </div>
        <div className="item-block__container">
          <ul className="ul">
            { profile.specialization.skills.map((item) => {
                return (
                  <li className="item-block__list-item" key={item}>{item}</li>
                )
              })
            }
          </ul>
        </div>
      </div>

      : null
    }

    <div className="item-block profile__item-block">
      <div className="item-block__header">
        <h3 className="item-block__h3">Experiance</h3>
        <hr className="item-block__hr"/>
      </div>
      <div className="item-block__container">
        { profile.experiance.length > 0 ?
          <ExperianceList expList = { profile.experiance } />
          : "Experiance doesn't exist"
        }
      </div>
    </div>

    <div className="item-block profile__item-block">
      <div className="item-block__header">
        <h3 className="item-block__h3">Education</h3>
        <hr className="item-block__hr"/>
      </div>
      <div className="item-block__container">
        { profile.education.length > 0 ?
          <EducationList eduList = { profile.education } />
          : "Education doesn't exist"
        }
      </div>
    </div>
    </>
  );
}

PersonalTypeProps.propTypes = {
  profile: PropTypes.object.isRequired
};

export default PersonalTypeProps;
