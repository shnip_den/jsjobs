import React from 'react';
import PropTypes from 'prop-types';
import pluralize from 'pluralize';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope, faEdit } from '@fortawesome/free-regular-svg-icons';
import capitalize from '../../utils/capitalize';
import PersonalTypeProps from './PersonalTypeProps';
import UserJobs from '../Job/UserJobs';


const Profile = (props) => {
  const calculateExperiance = experiance => {
    if (!experiance) return null;

    const timeDiff = experiance.reduce((sum, item) => {
                        if (!item.to) item.to = new Date();
                        let to = new Date(item.to);
                        let from = new Date(item.from);
                        let currentTimeDiff = to.getTime() - from.getTime();
                        return sum + currentTimeDiff;
                      }, 0);
    const months = Math.round(timeDiff/(1000*60*60*24*30));

    if (months > 12) {
      const year = Math.floor(months / 12);
      const month = months % 12;
      return `${pluralize('year', year, true)} ${pluralize('month', month, true)}`
    }
    return months === 1 ? `1 month` : `${months} months`;
  }

  const readyToRelocate = profile => {
    if (profile.specialization.readyToRelocate) return "Ready to relocate"
    return "Not ready to relocate";
  }

  const readyRemoteJob = profile => {
    if (profile.specialization.readyToRelocate) return "Ready to work remotely"
    return "Not ready to work remotely";
  }

  const parseContacts = contacts => {
    if (!contacts) return null;

    return Object.entries(contacts).map(item => {
      return (
        <div className='infoblock__item' key = {item[0]}>
          <span className='infoblock__item-header'>{capitalize(item[0])}:</span>
          <span className="inifoblock__content">{item[1]}</span>
        </div>
      )
    });
  }

  const { profile, user } = props;

  return (
    <div className="profile row">
      <div className="col-lg-4 profile__info-sidebar">
        <div className="profile__avatar">
          <img src={profile.user.avatar} alt="Your avatar" className="profile__img" />
        </div>
        <div className="profile__user-info">
          <div className="infoblock__header">
            <div>
              {`${profile.personal.firstname} ${profile.personal.lastname}`}
            </div>
          </div>
          <div className="text-grey">
            handle: { profile.handle }
          </div>
          <div className="text-grey">
            { profile.specialization.qualification }
          </div>
          <div className="text-grey">
            { profile.specialization.status }
          </div>
        </div>

        <div className="profile__buttons infoblock">
          { user.id === profile.user._id ?
            <>
              <Link to="/profile/edit">
                <button className="btn btn_pelorous-theme profile__btn">
                  <span className="btn__icon">
                    <FontAwesomeIcon icon={ faEdit } size="1x" />
                  </span>
                  Edit
                </button>
              </Link>
              <Link to="/messages">
              <button className="btn btn_pelorous-theme profile__btn">
                <span className="btn__icon">
                <FontAwesomeIcon icon={ faEnvelope } size="1x" />
                </span>
                Messages
              </button>
              </Link>
              { profile.account_type === 'company' &&
                <Link to="/job/create">
                  <button className="btn btn_pelorous-theme profile__btn">
                    Create Job
                  </button>
                </Link>
              }
            </>
          :
            <Link to={{ pathname: '/messages', query: { to: profile._id } }}>
              <button className="btn btn_pelorous-theme profile__btn">
                <span className="btn__icon">
                <FontAwesomeIcon icon={ faEnvelope } size="1x" />
                </span>
                Send message
              </button>
            </Link>
          }
        </div>
        <div className="infoblock">
          <div className="infoblock__header">Location</div>
          <div>{ profile.personal.location }</div>
          <div>{ readyToRelocate(profile) }</div>
          <div>{ readyRemoteJob(profile) }</div>
        </div>

        <div className="infoblock">
          <div className="infoblock__header">Experiance</div>
          <div className="infoblock__content">
            { profile.experiance.length > 0 ?
              calculateExperiance(profile.experiance)
              : "Person hasn't any experiance"
            }

          </div>
        </div>

        <div className="infoblock">
          <div className="infoblock__header"><strong>Contacts</strong></div>
          { profile.contacts ?
             parseContacts(profile.contacts)
           : "Person hasn't contacts"
           }
        </div>
      </div>

      <div className="col-lg-8">

        {
          profile.personal.bio ?

          <div className="item-block profile__item-block">
            <div className="item-block__header">
              <h3 className="item-block__h3">About me</h3>
              <hr className="item-block__hr"/>
            </div>
            <div className="item-block__container">
              {profile.personal.bio}
            </div>
          </div>

          : null
        }
        { profile.account_type === 'personal' ?
          <PersonalTypeProps profile={profile} />
          :
          <div className="item-block profile__item-block">
            <div className="item-block__header">
              <h3 className="item-block__h3">Actual Vacancies</h3>
            </div>
            <div className="item-block__container">
              <UserJobs profile_id={profile._id}/>
            </div>
          </div>

        }

      </div>
    </div>
  );
}

Profile.propTypes = {
  profile: PropTypes.object.isRequired
};

export default Profile;
