import React from 'react';
import PropTypes from 'prop-types';
import searchIcon from './search.svg';

const SearchForm = (props) => {
  const { onChange, value, placeholder = "" } = props;
  return (
    <div className="search">
      <input  type="text"
              className="search__input"
              placeholder={placeholder} onChange={onChange}
              value = {value} />
      <img src={searchIcon} alt="search-icon" className="search__icon"></img>
    </div>
  );
}

SearchForm.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired
};

export default SearchForm;
