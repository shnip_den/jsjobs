import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

export class ChatContent extends React.Component {
  componentDidMount() {
		this.scrollDown()
	}

	componentDidUpdate(prevProps, prevState) {
		this.scrollDown()
	}

  scrollDown = () => {
		const { container } = this.refs
		container.scrollTop = container.scrollHeight
	}

  render() {
    const { profile, messages } = this.props;

    return (
      <section className="chat__messages" ref='container'>
        <ul className="ul">
          { messages && messages.map((message, index) => {
            return (
              <li className={classnames("message",
                                        {"message_sender": profile._id === message.profile_id })}
                  key={message._id}>
                <img src={ message.avatar } alt="avatar" className="img-circle message__avatar"/>

                <p className="message__content">
                  {  message.content }
                </p>
              </li>
            )
          })}
        </ul>
      </section>
    );
  }
}

ChatContent.propTypes = {
  messages: PropTypes.array,
  profile: PropTypes.object
};

export default ChatContent;
