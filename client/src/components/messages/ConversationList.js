import React from 'react';
import PropTypes from 'prop-types';
import getFullName from '../../utils/getFullName';

const ConversationList = (props) => {
  const { conversations, getInterlocutor } = props;

  const lastMessage = (messages) => {
    if (messages.length === 0)  return "Haven't messages";
    const lastMessage = messages[messages.length-1];
    return (
      <div className="conversations__last-message">
        <img src={lastMessage.avatar} className="img-circle conversations__last-avatar" alt="small avatar"/>
        <div className="conversations__text">
          {lastMessage.content}
        </div>
      </div>
    )
  }

  const sortedArr = conversations.sort((a, b) => {
    if (typeof a === 'string') a = new Date(a);
    if (typeof b === 'string') b = new Date(b);
    return a.updated_at - b.updated_at
  });

  return (
    <ul className="ul conversations">
      { conversations.length > 0 && sortedArr.map((item, index) => {
        return (
          <li className="conversations__item" key={item._id} data-index={index}>
            <div>
              <img src={ getInterlocutor(item).user.avatar } alt="avatar" className="img-circle conversations__avatar"/>
            </div>
            <div className="conversations__content">
              <div className="conversations__header">
                {getFullName(getInterlocutor(item))}
              </div>
              {
                lastMessage(item.messages)
              }
            </div>
          </li>
        )
      })}
    </ul>
  );
}

ConversationList.propTypes = {
  conversations: PropTypes.array.isRequired,
  getInterlocutor: PropTypes.func.isRequired
};

export default ConversationList;
