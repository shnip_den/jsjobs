import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPaperPlane } from '@fortawesome/free-solid-svg-icons';


export default class ChatInput extends React.Component {
  state = {
    message: ''
  }

  onChange = e => {
    this.setState({ message: e.target.value})
  }

  handleSubmit = e => {
    e.preventDefault();
    if (!this.state.message.trim()) return false;

    this.props.sendMessage(this.state.message);
    this.setState({ message: ''});
  }

  render() {
    return (
      <section className="chat__message-input">
        <form onSubmit={this.handleSubmit} className="chat__form">
          <input  type="text"
                  placeholder="Write your message..."
                  className="chat__input"
                  value={this.state.message}
                  onChange={this.onChange} />
          <button className="btn chat__btn">
            <FontAwesomeIcon icon={faPaperPlane} />
          </button>
        </form>
      </section>
    );
  }
}

ChatInput.propTypes = {
  sendMessage: PropTypes.func.isRequired
};
