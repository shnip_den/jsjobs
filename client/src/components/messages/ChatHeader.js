import React from 'react';
import PropTypes from 'prop-types';
import getFullName from '../../utils/getFullName';

const ChatHeader = (props) => {
  const { interlocutor } = props;

  return (
      <section className="chat__header">
        { interlocutor &&
          <>
            <img src={ interlocutor.user.avatar } alt="avatar"
                 className="img-circle chat__recipient-img" />
            <span className="chat__recipient">
              {getFullName(interlocutor)}
            </span>
          </>
        }
      </section>
  );
}

ChatHeader.propTypes = {
  interlocutor: PropTypes.object
};

export default ChatHeader;
