import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getOwnProfile } from '../../redux/actions/profileActions';
import io from 'socket.io-client';
import axios from 'axios';
import Preloader from '../helpers/Preloader';
import ConversationList from './ConversationList';
import ChatHeader from './ChatHeader';
import ChatContent from './ChatContent';
import ChatInput from './ChatInput';
import events from './events';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars } from '@fortawesome/free-solid-svg-icons';

const socketURL = 'http://localhost:5080';
const socket = io(socketURL);

export class Room extends React.Component {
  state = {
    socket: '',
    conversations: [],
    errors: null,
    activeConversation: {}
  }

  componentDidMount() {
    this.initialize();
  }

  componentWillUnmount() {
    socket.emit(events.USER_DISCONECTED, this.props.profile.ownProfile._id);
  }

  initialize = async () => {
    if (!this.props.profile.ownProfile) await this.props.getOwnProfile();

    await this.getConversations();
    this.initSocket();

    if(this.props.location.query && this.props.location.query.to) {
      const id = this.props.location.query.to;
      this.hasAlreadyConversation(id);
    }

    const conversations = document.querySelector('.conversations');
    conversations && conversations.addEventListener('click', this.handleClick);
  }

  initSocket = async () => {
    socket.on('connect', () => {
      const profile = this.props.profile.ownProfile;
      socket.emit(events.USER_CONNECTED, profile._id);
    })

    socket.on(events.PRIVATE_MESSAGE, (conversationId, message) => {
      this.putMessageFromSocket(conversationId, message)
    });

    this.setState({socket});
  }

  getConversations = async () => {
    const profile = this.props.profile.ownProfile;
    if (!profile) return false;

    await axios(`/conversation/all?id=${profile._id}`)
      .then(res => this.setState({ conversations: res.data }))
      .catch(e => this.setState({ errors: e.response.data}))
  }

  hasAlreadyConversation = (id) => {
    const { conversations } = this.state;
    if (conversations.length === 0) return false;

    let isExist = false;
    let index;
    conversations.forEach((item, i) => {
      if (item.sender._id === id || item.recipient._id === id) {
        isExist = true;
        return index = i;
      }
    });
    if (isExist) {
      this.setState({ activeConversation: this.state.conversations[index] });
      this.addToActiveConversationClass(index);
    } else {
      this.createConversation(id);
    }
  }

  addToActiveConversationClass = index => {
    document.querySelector(`[data-index='${index}']`).classList.add('conversations__item_active');
  }

  createConversation = (id) => {
    if (!this.props.profile.ownProfile) return false;

    const fields = {
      sender: this.props.profile.ownProfile._id,
      recipient: id
    };

    axios.post('/conversation', fields).then(res => {
      this.setState({conversations: [...this.state.conversations, res.data] });
      const index = this.state.conversations.length-1;
      this.setActiveConversation(index);
      this.addToActiveConversationClass(index)
    })
    .catch(err => this.setState({ errors: err }))
  }

  getInterlocutor = (item) => {
    if(!item || Object.keys(item).length === 0 ) return null;

    if(this.props.profile.ownProfile._id !== item.sender._id) {
      return item.sender
    } else {
      return item.recipient
    }
  }

  handleClick = (e) => {
    const activeLi = document.querySelector('.conversations__item_active');
    if(activeLi) activeLi.classList.toggle('conversations__item_active');

    const ul = e.currentTarget;
    let target = e.target;

    while (target !== ul) {
      if (target.hasAttribute('data-index')) {
        target.classList.toggle('conversations__item_active');
        const index = target.dataset.index;
        return this.setActiveConversation(index);
      }
      target = target.parentNode;
    }
  }

  setActiveConversation = (index) => {
    if (!index) return false;

    const conversations = this.state.conversations;

    this.setState({activeConversation: conversations[index]})
  }

  sendMessage = (text) => {
    const profile = this.props.profile.ownProfile;
    const { activeConversation } = this.state;
    const activeConversationId = activeConversation._id;
    const recipient = this.getInterlocutor(activeConversation);

    const message = {
      content: text,
      avatar: profile.user.avatar,
      profile_id: profile._id,
      firstname: profile.personal.firstname,
      lastname: profile.personal.lastname,
      created_at: new Date()
    }
    socket.emit(events.SEND_PRIVATE_MESSAGE, activeConversationId, recipient._id, message);
    socket.on(events.MESSAGE_ERROR, (error) => {
      this.setState({ errors: error });
    })
  }

  putMessageFromSocket = (conversationId, message) => {
    let indexConversation;
    let hasIndex = false;
    const conversations = this.state.conversations;
    conversations.forEach((item, index) => {
      if (item._id === conversationId) {
        hasIndex = true;
        indexConversation = index;
      }
    });

    if (hasIndex) {
      conversations[indexConversation].messages.push(message);
      conversations[indexConversation].updated_at = new Date();
      this.setState({conversations: conversations});
    } else {
      //todo: refactor this step;
      this.getConversations();
    }
  }

  collapse = (e) => {
    const { room__collapse: collapse, aside, content } = this.refs;

    collapse.classList.toggle('room__collapse_clicked');
    aside.classList.toggle('room__aside-collapse');
    content.classList.toggle('room__content-collapse');
  }

  render() {
    const { ownProfile: profile, isLoading } = this.props.profile;
    const { activeConversation } = this.state;
    const interlocutor = this.getInterlocutor(activeConversation);

    const preLoad = () => {
      if (profile === null || isLoading){
        return <Preloader />
      }
      return null
    }

    return (
      <>
        { preLoad() ||
          <div className="row room">
            <div className="col-sm-2 col-md-5 col-lg-4" ref="aside">
              <aside className="room__sidebar">
                <div className="room__menu" onClick={this.collapse}>
                  <FontAwesomeIcon icon={faBars} size="2x" />
                </div>
                <section className="room__collapse" ref="room__collapse">
                  <div className="room__search"></div>
                  <div className="room__conversationList">
                    <ConversationList conversations={this.state.conversations}
                                      getInterlocutor={this.getInterlocutor} />
                  </div>
                </section>
              </aside>
            </div>
            <div className="col-sm-10 col-md-7 col-lg-8" ref="content">
              <main className="room__content chat">
                <ChatHeader interlocutor={interlocutor} />

                <ChatContent messages={activeConversation.messages}
                             profile={profile} />

                <ChatInput sendMessage={this.sendMessage} />
              </main>
            </div>
          </div>
        }
      </>
    );
  }
}

Room.propTypes = {
  profile: PropTypes.object,
  getOwnProfile: PropTypes.func
};

const mapStateToProps = (state) => {
  return {
    profile: state.profile
  }
}

export default connect(mapStateToProps, {getOwnProfile})(Room);
