import React from 'react';
import PropTypes from 'prop-types';
import Field from '../helpers/forms/field';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { registerUser } from '../../redux/actions/authActions';
import { clearReduxErrors } from '../../redux/actions/errorActions';
import redirectWithCondition from '../../utils/redirectWithCondition';


export class Register extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      password_confirmation: '',
      errors: {}
    }

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e){
    this.setState({ [e.target.name]: e.target.value });
  }

  onSubmit(e){
    e.preventDefault();

    const { email, password, password_confirmation } = this.state;
    const newUser = {
      email,
      password,
      password_confirmation
    }

    this.props.registerUser(newUser, this.props.history);
  }

  componentDidMount() {
    const props = this.props;
    redirectWithCondition(props,
                          props.auth.user.profileStatus,
                          '/profile',
                          '/profile/new');
  }

  componentDidUpdate(prevProps) {
    if(prevProps.errors !== this.props.errors) {
      this.setState({ errors: this.props.errors})
    }
  }

  componentWillUnmount() {
    if(Object.keys(this.props.errors).length > 0) {
      this.props.clearReduxErrors()
    }
  }

  render() {
    const { errors, email, password, password_confirmation } = this.state;

    return (
      <div className="row">
        <div className="col-md-4 offset-md-4 col-sm-10 offset-sm-1">
          <h1 className="text-center">Register</h1>
          <form onSubmit = { this.onSubmit } className="form">

            <Field
              component = "input"
              name="email"
              id="email"
              className="input form__input"
              value = { email }
              placeholder="Your email"
              onChange = { this.onChange }
              classNamesParams={ { "input__invalid-input": errors.email } }
              required = { true }
              errors = { errors.email }
              classLabel = "label form__label"
            />

            <Field
              component = "input"
              name="password"
              id="password"
              className="input form__input"
              type="password"
              value = { password }
              placeholder="Enter your password from 5 to 30 characters"
              onChange = { this.onChange }
              classNamesParams={ { "input__invalid-input": errors.password } }
              required = { true }
              errors = { errors.password }
              classLabel = "label form__label"
            />

            <Field
              component = "input"
              name="password_confirmation"
              id="password_confirmation"
              className="input form__input"
              type="password"
              value = { password_confirmation }
              placeholder="Confirm your password"
              onChange = { this.onChange }
              classNamesParams={ { "input__invalid-input": errors.password_confirmation } }
              required = { true }
              errors = { errors.password_confirmation }
              classLabel = "label form__label"
            />

            <button type="submit" className="btn btn_pelorous-theme form__btn">Submit</button>
          </form>
        </div>
      </div>
    );
  }
}

Register.propTypes = {
  registerUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  clearReduxErrors: PropTypes.func.isRequired
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    errors: state.errors
  }
}

export default connect(mapStateToProps,
                        { registerUser, clearReduxErrors } )(withRouter(Register));
