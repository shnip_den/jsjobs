import React from 'react';
import PropTypes from 'prop-types';
import Field from '../helpers/forms/field';
import { connect } from 'react-redux';
import { loginUser } from '../../redux/actions/authActions';
import { clearReduxErrors } from '../../redux/actions/errorActions';
import redirectWithCondition from '../../utils/redirectWithCondition';


export class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      errors: {}
    }

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    const props = this.props;
    /*
      used for a logged user who is trying to get the path again '/ login'
    */
    redirectWithCondition(props,
                          props.auth.user.profileStatus,
                          '/profile',
                          '/profile/new');
  }

  componentDidUpdate(prevProps) {
    if(prevProps.errors !== this.props.errors) {
      this.setState({ errors: this.props.errors})
    }

    redirectWithCondition(this.props,
                            this.props.auth.user.profileStatus,
                            '/profile',
                            '/profile/new');
  }

  componentWillUnmount() {
    if(Object.keys(this.props.errors).length > 0) {
      this.props.clearReduxErrors()
    }
  }

  onChange(e) {
    this.setState({ [e.target.id]: e.target.value });
  }

  onSubmit(e) {
    e.preventDefault();

    const loginData = {
      email: this.state.email,
      password: this.state.password
    }

    this.props.loginUser(loginData);
  }

  render() {
    const { errors, email, password } = this.state;

    return (
      <div className="row">
        <div className="col-md-4 offset-md-4 col-sm-10 offset-sm-1">
          <h1 className="text-center">Login</h1>
          <form onSubmit = { this.onSubmit }>

            <Field
              component = "input"
              name = "email"
              id = "email"
              type = "email"
              className = "input form__input"
              value = { email }
              placeholder="Email"
              onChange = { this.onChange }
              classNamesParams={ { "input__invalid-input": errors.email } }
              required = { true }
              errors = { errors.email }
              classLabel = "label form__label"
            />

            <Field
              component = "input"
              name = "password"
              id = "password"
              className = "input form__input"
              type = "password"
              value = { password }
              placeholder = "Password"
              onChange = { this.onChange }
              classNamesParams = { { "input__invalid-input": errors.password } }
              required = { true }
              errors = { errors.password }
              classLabel = "label form__label"
            />

            <button type="submit" className="btn btn_pelorous-theme form__btn">Submit</button>
          </form>
        </div>
      </div>
    );
  }
}

Login.propTypes = {
  loginUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  clearReduxErrors: PropTypes.func.isRequired
}

const mapStateToProps = (state) => {
    return {
      auth: state.auth,
      errors: state.errors
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        loginUser: (loginData) => dispatch(loginUser(loginData)),
        clearReduxErrors: () => dispatch(clearReduxErrors())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login)
