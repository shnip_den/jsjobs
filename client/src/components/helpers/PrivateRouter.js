import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ProfileCreate from '../profile/create/ProfileCreate';


export const PrivateRouter = (props) => {
  const { component: Component, auth, profile, withProfile = true, ...rest } = props;

  const switchComponent = () => {
    if (!auth.isAuthenticated) return (<Redirect to='/login' />);

    if (withProfile) {
      if(profile.ownProfile || auth.user.profileStatus) {
        if(window.location.pathname === '/profile/new') {
          return (<Redirect to='/profile' />)
        }
        return (<Component {...props} />)
      }

      return (<ProfileCreate />)
    }

    return (<Component {...props} />)
  }

  return (
      <Route { ...rest }
              render = { switchComponent }
      />
  )
}

PrivateRouter.propTypes = {
  auth: PropTypes.object.isRequired,
  profile: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  profile: state.profile
});

export default connect(mapStateToProps, null)(PrivateRouter);
