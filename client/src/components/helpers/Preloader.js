import React from 'react';

const Preloader = (props) => {
  return (
    <div className="preloader_cube">
      <div className="cube cube-1"></div>
      <div className="cube cube-2"></div>
      <div className="cube cube-4"></div>
      <div className="cube cube-3"></div>
    </div>
  );
}

export default Preloader;
