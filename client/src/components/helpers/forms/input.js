import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";


const Input = (props) => {
  const { label = true,
          type = "text",
          id = '',
          name = '',
          value = '',
          placeholder = '',
          onChange = () => null,
          required = false,
          className = '',
          classLabel = '',
          checked = false,
          labelText = '',
          autofocus = false,
          classNamesParams = {},
          icon = "",
          errors = {}
        } = props.params;

  const { capitalize, addErrors } = props;

  return (
    <React.Fragment>
      { label &&
        <label htmlFor={id} className={ classLabel }>
          { icon }
          { capitalize(labelText) || capitalize(name) || capitalize(id) || ''}
        </label>
      }



      {
        type === "date" ?
        <DatePicker id = { id }
                    name = { name }
                    placeholderText = { placeholder }
                    selected = { value }
                    onChange = { onChange }
                    className = { classnames(className, classNamesParams) }
                    />
        :

        <input
              type = {type}
              id = {id}
              name = { name }
              value = { value }
              placeholder = { placeholder }
              onChange = { onChange }
              className = { classnames(className, classNamesParams) }
              required = { required }
              autoFocus = { autofocus }
              checked = { checked }
        />
      }

      {
        errors
        &&
        addErrors(errors)
      }
    </React.Fragment>
  );
}

Input.propTypes = {
  label: PropTypes.bool,
  id: PropTypes.string,
  type: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  required: PropTypes.bool,
  className: PropTypes.string,
  classLabel: PropTypes.string,
  classNamesParams: PropTypes.object,
  errors: PropTypes.object,
  autofocus: PropTypes.bool
}

export default Input;
