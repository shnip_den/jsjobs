import React from 'react';
import Input from './input';
import Select from './select';
import TextArea from './textarea';
import capitalize from '../../../utils/capitalize';

const Field = (props) => {
  const { component } = props;

  function addErrors(field_errors){
    return(
      Object.keys(field_errors)
            .map((errType, index) => {
              return(
                <div className="input__error-message" key={errType}>
                  { field_errors[errType] }
                </div>
              )
            })
    )
  }

  switch (component) {
    case "input":
      return (
        <Input  params = { props }
                capitalize = { capitalize }
                addErrors = { addErrors }
                />
      )
    case "select":
      return (
        <Select params = { props }
                capitalize = { capitalize }
                addErrors = { addErrors }
                />
      )
    case "textarea":
      return (
        <TextArea params = { props }
                  capitalize = { capitalize }
                  addErrors = { addErrors }
                  />
      )
    default:
      return (
        <h2>Something went wrong...</h2>
      )
  }
}

export default Field;
