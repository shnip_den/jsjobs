import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';


const Select = (props) => {
  const { label = true,
          id = '',
          name = '',
          value = '',
          placeholder = '',
          onChange = () => null,
          required = false,
          labelText = '',
          className = '',
          classLabel = '',
          classNamesParams = {},
          errors = {},
          multiple = false,
          size = "1",
          autofocus = false,
          options = [["false", "Options dont exists"]]
        } = props.params;

  const { capitalize, addErrors } = props;

  const mapedOptions = options.map( (item, index)=> {
    return (
      <option key = { index } value = { item[0] }>
        { item[1] }
      </option>
    )
  })

  return (
    <React.Fragment>
      { label &&
        <label htmlFor={id} className={ classLabel }>
          { capitalize(labelText) || capitalize(name) || capitalize(id) || ''}
        </label>
      }

      <select
            id = { id }
            name = { name }
            value = { value }
            placeholder = { placeholder }
            onChange = { onChange }
            className = { classnames(className, classNamesParams) }
            required = { required }
            multiple = { multiple }
            size = { size }
            autoFocus = { autofocus }
      >
        { mapedOptions }
      </select>

      {
        errors
        &&
        addErrors(errors)
      }
    </React.Fragment>
  );
}

Select.propTypes = {
  label: PropTypes.bool,
  id: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.string,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  required: PropTypes.bool,
  className: PropTypes.string,
  classLabel: PropTypes.string,
  classNamesParams: PropTypes.object,
  errors: PropTypes.object,
  multiple: PropTypes.bool,
  size: PropTypes.string,
  autofocus: PropTypes.bool,
  options: PropTypes.arrayOf(PropTypes.string)
}

export default Select;
