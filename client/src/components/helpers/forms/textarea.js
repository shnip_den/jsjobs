import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';


const TextArea = (props) => {
  const { label = true,
          id = '',
          name = '',
          value = '',
          placeholder = '',
          onChange = () => null,
          required = false,
          labelText = '',
          className = '',
          classLabel = '',
          classNamesParams = {},
          errors = {},
          autofocus = false,
          cols = "30",
          rows = "10",
          maxLength = "300"
        } = props.params;

  const { capitalize, addErrors } = props;

  return (
    <React.Fragment>
      { label &&
        <label htmlFor={id} className={ classLabel }>
          { capitalize(labelText) || capitalize(name) || capitalize(id) || ''}
        </label>
      }

      <textarea
            id = { id }
            name = { name }
            value = { value }
            placeholder = { placeholder }
            onChange = { onChange }
            className = { classnames(className, classNamesParams) }
            required = { required }
            autoFocus = { autofocus }
            cols = { cols }
            rows = { rows }
            maxLength = { maxLength }
      >
      </textarea>

      {
        errors
        &&
        addErrors(errors)
      }
    </React.Fragment>
  );
}

TextArea.propTypes = {
  label: PropTypes.bool,
  id: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.string,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  required: PropTypes.bool,
  className: PropTypes.string,
  classLabel: PropTypes.string,
  classNamesParams: PropTypes.object,
  errors: PropTypes.object,
  autofocus: PropTypes.bool,
  cols: PropTypes.string,
  rows: PropTypes.string,
  maxlength: PropTypes.string
}

export default TextArea;
