import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { logoutUser } from '../../redux/actions/authActions';

class Navbar extends React.Component {
  constructor(props){
    super(props);

    this.onLogout = this.onLogout.bind(this);
  }

  onLogout(e){
    e.preventDefault();

    this.props.logoutUser();
  }

  render(){
    const { isAuthenticated, user } = this.props.auth;

    const authLinks = (
      <React.Fragment>
        <li className="navbar__item">
          <Link to='/profiles'>Profiles</Link>
        </li>
        <li className="navbar__item">
          <Link to='/jobs'>Jobs</Link>
        </li>
        <li className="navbar__item navbar__avatar dropdown-menu">
          <img src={user.avatar} alt="Your avatar" className="img-circle navbar__img" />
          { user.email }
          <ul className="ul dropdown-menu__list">
            <li><Link to="/profile" className="dropdown-menu__item">
              My Profile
            </Link></li>
            <li><Link to="/profile/edit" className="dropdown-menu__item">
              Edit profile
            </Link></li>
            <li><Link to="/messages" className="dropdown-menu__item">
              My messages
            </Link></li>
            <li><Link to="/applications" className="dropdown-menu__item">
              My applications
            </Link></li>
            <li className="dropdown-menu__devider"></li>
            <li><Link to="#" onClick={ this.onLogout } className="dropdown-menu__item">
              Logout
            </Link></li>
          </ul>
        </li>
      </React.Fragment>
    );

    const guestLinks = (
      <React.Fragment>
        <li className="navbar__item">
          <Link to="/signup">signup</Link>
        </li>
        <li className="navbar__item">
          <Link to="/login">login</Link>
        </li>
      </React.Fragment>
    );

    return (
      <header className="header header_content-space-between page-wrapper__header text-white background-black">
        <div className="brand header__brand">
          <Link to="/">BRAND</Link>
        </div>
        <nav className="navbar">
          <ul className="list navbar__list">
            <li className="navbar__item">
              <Link to="/">home</Link>
            </li>
            {
              isAuthenticated ?
                authLinks
              :
                guestLinks
            }

          </ul>
        </nav>
      </header>
    );
  }
}

Navbar.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth
  }
}

export default connect(mapStateToProps, { logoutUser })(Navbar);
