import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';


export default () => {
  return (
    <footer className="footer page-wrapper__footer background-black">
      <div className="container">
        <div className="row">
          <div className="col-md-3 col-sm-12">
            <h2>BRAND</h2>
            <p> © Copyright 2018 Simpleqode. All rights reserved.</p>
          </div>
          <div className="col-md-3 col-sm-12">
            <ul className="list">
              <li className="list__title">Help</li>
              <li>For the applicant</li>
              <li>For employer</li>
              <li>Service API</li>
              <li>Support</li>
            </ul>
          </div>
          <div className="col-md-3 col-sm-12">
            <ul className="list">
              <li className="list__title">Documents</li>
              <li>User Agreement</li>
              <li>Terms of Service</li>
              <li>Our partners</li>
            </ul>
          </div>
          <div className="col-md-3 col-sm-12">
            <ul className="list">
              <li className="list__title">We are in social networks</li>
              <li>
                <ul className="list list__social-group">
                  <li className="list__social-icon">
                    <Link to="#">
                      < FontAwesomeIcon icon={["fab", "facebook"]} size="2x" />
                    </Link>
                  </li>
                  <li className="list__social-icon">
                    <Link to="#">
                      < FontAwesomeIcon icon={["fab", "vk"]} size="2x" />
                    </Link>
                  </li>
                  <li className="list__social-icon">
                    <Link to="#">
                      < FontAwesomeIcon icon={["fab", "instagram"]} size="2x" />
                    </Link>
                  </li>
                  <li className="list__social-icon">
                    <Link to="#">
                      < FontAwesomeIcon icon={["fab", "telegram"]} size="2x" />
                    </Link>
                  </li>
                  <li className="list__social-icon">
                    <Link to="#">
                      < FontAwesomeIcon icon={["fab", "twitter"]} size="2x" />
                    </Link>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
  );
}
