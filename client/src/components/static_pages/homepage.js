import React from 'react';
import { Link } from 'react-router-dom';

export default () => {
  return (
    <div className="homepage text-white">
      <div className="container">
        <div className="row">
          <div className="col-md-4 offset-md-4 col-sm-6 offset-sm-3 col-12 homepage__content">
            <h1 className="homepage__title">BRAND</h1>
            <p className="homepage__paragraph">
              Find jobs, employees and partners
            </p>
            <Link to="/signup">
              <button type="button" className="btn btn_pelorous-theme">Sign Up</button>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}
