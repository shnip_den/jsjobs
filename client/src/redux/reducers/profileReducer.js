import {  SET_CURRENT_PROFILE,
          UPDATE_PROFILE,
          PROFILE_LOADING,
          CLEAR_ALL_PROFILES,
          PROFILE_ERROR,
          GET_PROFILES,
          SET_PROFILES,
          SET_OWN_PROFILE } from '../actions/types';

const initialState = {
  ownProfile: null,
  current: null,
  profiles: null,
  isLoading: false
};

export default function( state = initialState, action) {
  switch (action.type) {
    case SET_CURRENT_PROFILE:
      return {
        ...state, current: action.payload, isLoading: false
      }
    case UPDATE_PROFILE:
      return {
        ...state, ownProfile: action.payload, isLoading: false
      }
    case PROFILE_LOADING:
      return {
        ...state, isLoading: true
      }
    case CLEAR_ALL_PROFILES:
      return {
        ...state, current: null, ownProfile: null
      }
    case PROFILE_ERROR:
      return {
        ...state, isLoading: false
      }
    case GET_PROFILES:
      return {
        ...state, isLoading: true
      }
    case SET_PROFILES:
      return {
        ...state, isLoading: false, profiles: action.payload
      }
    case SET_OWN_PROFILE:
      return {
        ...state, ownProfile: action.payload, isLoading: false
      }
    default:
      return state;
  }
}
