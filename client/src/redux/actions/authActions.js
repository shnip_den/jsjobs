import { GET_ERRORS, SET_CURRENT_USER } from './types'
import axios from 'axios';
import setAuthToken from '../../utils/setAuthToken';
import jwt_decode from 'jwt-decode';

// Register User
export const registerUser = (userData, history) => {
  return (dispatch) => {
            axios.post("/api/signup", userData)
                  .then(res => history.push('/login'))
                  .catch(e =>
                    dispatch({
                      type: GET_ERRORS,
                      payload: e.response.data
                    })
                  );
          }
}

//Login - Get User Token
export const loginUser = (loginData) => {
  return (dispatch) => {
    return axios.post("/api/login", loginData)
          .then(
            res => {
              const { token } = res.data;
              localStorage.setItem('jwt',token);

              setAuthToken(token);
              const decoded = jwt_decode(token);

              dispatch(setCurrentUser(decoded));
            }
          )
          .catch( e =>
            dispatch({
              type: GET_ERRORS,
              payload: e.response.data
            })
          )
  }
}

export const setCurrentUser = decoded => {
  return {
    type: SET_CURRENT_USER,
    payload: decoded
  }
}

export const logoutUser = () => dispatch => {
  localStorage.removeItem('jwt');
  setAuthToken(false);
  dispatch(setCurrentUser({}));
  window.location.hash = "/login";
}
