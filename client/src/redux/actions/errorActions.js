import { CLEAR_ERRORS } from './types';

export const clearReduxErrors = () => dispatch => {
  dispatch({ type: CLEAR_ERRORS })
}
