import {  GET_ERRORS,
          GET_CURRENT_PROFILE,
          GET_OWN_PROFILE,
          PROFILE_LOADING,
          CLEAR_ALL_PROFILES,
          SET_CURRENT_PROFILE,
          UPDATE_PROFILE,
          UPDATE_PERSONAL_PROFILE,
          UPDATE_ADDITIONAL_INFO_PROFILE,
          UPDATE_SPECIALIZATION_PROFILE,
          UPDATE_EXPERIANCE_PROFILE,
          UPDATE_EDUCATION_PROFILE,
          UPDATE_CONTACTS_PROFILE,
          PROFILE_ERROR,
          GET_PROFILES,
          SET_PROFILES,
          SET_OWN_PROFILE
        } from './types';
import axios from 'axios';


export const createProfile = (profileData, history) => {
  return (dispatch) => {
    return axios.post('/api/profile', profileData)
                .then(res => {
                  dispatch(setOwnProfile(res.data));
                  history.push('/profile')
                })
                .catch(e => {
                  dispatch(profileError());
                  dispatch({
                    type: GET_ERRORS,
                    payload: e.response.data
                  })
                })
  }
}

export const getCurrentProfile = () => dispatch => {
  dispatch({ type: GET_CURRENT_PROFILE });
  dispatch(profileLoading());
  return axios.get('/api/profile')
                .then( res =>
                  dispatch(setProfile(res.data))
                )
                .catch(e =>
                  dispatch({ type: GET_ERRORS, payload: e.response.data })
                );
}

export const getOwnProfile = () => dispatch => {
  dispatch({ type: GET_OWN_PROFILE });
  dispatch(profileLoading());
  return axios.get('/api/profile')
                .then( res =>
                  dispatch(setOwnProfile(res.data))
                )
                .catch(e => {
                  const error = e.response.data;
                  if(error.profile) {
                    if(window.location.pathname==="/profile/new") return null;
                  }
                  dispatch({ type: GET_ERRORS, payload: error })
                });
}

export const updatePersonal = (personalData) => dispatch => {
  dispatch({ type: UPDATE_PERSONAL_PROFILE });
  dispatch(profileLoading());
  return axios.post('/api/profile/personal', personalData)
          .then(res => {
            dispatch(updateProfile(res.data))
          })
          .catch(e => {
            dispatch(profileError());
            dispatch({
              type: GET_ERRORS,
              payload: e.response.data
            })
          })
}

export const updateAdditionalInfo = (addInfoData) => dispatch => {
  dispatch({ type: UPDATE_ADDITIONAL_INFO_PROFILE });
  dispatch(profileLoading());
  return axios.post('/api/profile/additionalInfo', addInfoData)
          .then(res => {
            dispatch(updateProfile(res.data))
          })
          .catch(e => {
            dispatch(profileError());
            dispatch({
              type: GET_ERRORS,
              payload: e.response.data
            })
          })
}

export const updateSpecialization = (specialization) => dispatch => {
  dispatch({ type: UPDATE_SPECIALIZATION_PROFILE });
  dispatch(profileLoading());
  return axios.post('/api/profile/specialization', specialization)
          .then(res => {
            dispatch(updateProfile(res.data))
          })
          .catch(e => {
            dispatch(profileError());
            dispatch({
              type: GET_ERRORS,
              payload: e.response.data
            })
          })
}

export const updateExperiance = (experiance) => dispatch => {
  dispatch({ type: UPDATE_EXPERIANCE_PROFILE });
  dispatch(profileLoading());
  return axios.post('/api/profile/experiance', experiance)
          .then(res => {
            dispatch(updateProfile(res.data))
          })
          .catch(e => {
            dispatch(profileError());
            dispatch({
              type: GET_ERRORS,
              payload: e.response.data
            })
          })
}

export const updateEducation = (education) => dispatch => {
  dispatch({ type: UPDATE_EDUCATION_PROFILE });
  dispatch(profileLoading());
  return axios.post('/api/profile/education', education)
          .then(res => {
            dispatch(updateProfile(res.data))
          })
          .catch(e => {
            dispatch(profileError());
            dispatch({
              type: GET_ERRORS,
              payload: e.response.data
            })
          })
}

export const updateContacts = (contacts) => dispatch => {
  dispatch({ type: UPDATE_CONTACTS_PROFILE });
  dispatch(profileLoading());
  return axios.post('/api/profile/contacts', contacts)
          .then(res => {
            dispatch(updateProfile(res.data))
          })
          .catch(e => {
            dispatch(profileError());
            dispatch({
              type: GET_ERRORS,
              payload: e.response.data
            })
          })
}

export const profileLoading = () => {
  return {
    type: PROFILE_LOADING
  }
}

export const clearAllProfiles = () => {
  return {
    type: CLEAR_ALL_PROFILES
  }
}

export const setProfile = (profile) => {
  return {
    type: SET_CURRENT_PROFILE,
    payload: profile
  }
}

export const setOwnProfile = profile => {
  return {
    type: SET_OWN_PROFILE,
    payload: profile
  }
}

export const updateProfile = (profile) => {
  return {
    type: UPDATE_PROFILE,
    payload: profile
  }
}

export const profileError = () => {
  return {
    type: PROFILE_ERROR
  }
}

export const setProfiles = (profiles) => {
  return {
    type: SET_PROFILES,
    payload: profiles
  }
}

export const getProfiles = () => dispatch => {
  dispatch({ type: GET_PROFILES });
  return axios('/api/profile/all').then(res => dispatch(setProfiles(res.data)))
                                .catch(e => {
                                  dispatch(profileError());
                                  dispatch({
                                    type: GET_ERRORS,
                                    payload: e.response.data
                                  });
                                });
}

export const getProfileByHandle = handle => dispatch => {
  dispatch({ type: GET_CURRENT_PROFILE });
  dispatch(profileLoading());
  return axios(`/api/profile/handle/${handle}`)
                                              .then( res =>
                                                dispatch(setProfile(res.data))
                                              )
                                              .catch(e => {
                                                dispatch(profileError());
                                                dispatch({
                                                  type: GET_ERRORS,
                                                  payload: e.response.data
                                                })
                                              });
}
