const express = require('express');
const router = express.Router();
const User = require('../models/user');

/* GET users listing. */
router.get('/', function(req, res, next) {
  User.find().then( users => {
    if(users) return res.json(users);

    error.message = "Not found";
    res.status(404).json(error);
  })
  .catch(e => res.status(404).json(error))
});

module.exports = router;
