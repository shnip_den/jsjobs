const express = require('express');
const router = express.Router();
const User = require('../models/user');
const gravatar = require('gravatar');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const keys = require('../config/keys');
const passport = require('passport');
const validateRegister = require('../validations/register');
const validateLogin = require('../validations/login');


router.post('/api/signup', function(req, res, next) {
  const { errors, isValid } = validateRegister(req.body);

  if(!isValid){
    return res.status(400).json(errors);
  }

  let { email, password } = req.body;
  email = email.toLowerCase();

  hashedPassword = User.encryptPassword(password);

  User.findOne({ email: email }).then( user => {
    if (user) {
      errors.email = { unique: "Email already exists" };
      res.status(400).json(errors);
    }else{
      const newUser = new User({
        email: email,
        password: hashedPassword,
        avatar: gravatar.url(email, {s: '200', r: 'pg', d: 'mm'})
      });

      newUser
            .save()
            .then(user => res.json(user))
            .catch(err => console.log(err));
      }
  })

});

router.post('/api/login', function(req, res, next){
  const { errors, isValid } = validateLogin(req.body);

  if(!isValid){
    return res.status(400).json(errors);
  }

  const email = req.body.email.toLowerCase();
  const password = req.body.password;

  User.findOne({email})
      .then( user => {
        if(!user){
          errors.email = { message: "User not found" };
          return res.status(404).json(errors);
        }

        bcrypt.compare(password, user.password)
              .then(match => {
                if(match){
                  const payload  ={
                    id: user.id,
                    email: user.email,
                    avatar: user.avatar,
                    profileStatus: user.profileStatus
                  }

                  jwt.sign(
                            payload,
                            keys.secret,
                            {expiresIn: "1d"},
                            (err, token) => {
                              res.json(
                                { success: true, token: 'Bearer ' + token}
                              )
                            });
                }
                else{
                  errors.password = { message: "Password incorrect" };
                  return res.status(400).json(errors);
                }

              });

      })
});

router.get('/current_user', passport.authenticate('jwt', {session: false}),
            (req, res) => res.json({
              id: req.user.id,
              email: req.user.email,
              avatar: req.user.avatar,
              profileStatus: req.user.profileStatus
            })
);

module.exports = router;
