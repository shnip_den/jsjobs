const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');
const Profile = require('../models/profile');
const User = require('../models/user');
const { validateProfile, formateMongoError } = require('../validations/profile/profile');
const validatePersonal = require('../validations/profile/personal');
const validateAdditionalInfo = require('../validations/profile/additional-information');
const validateSpecialization = require('../validations/profile/specialization');
const validateExperiance = require('../validations/profile/experiance');
const validateEducation = require('../validations/profile/education');

/*
  @route    GET /profile
  @desc     get current user profile
  @access   Private
*/
router.get('/', passport.authenticate('jwt', {session: false}), function(req, res, next) {
  const errors = {};
  Profile.findOne({user: req.user.id})
          .populate('user', 'avatar')
          .then( profile =>{
            if(!profile){
              errors.profile = 'There is no profile for this user';
              return res.status(404).json(errors);
            }
            res.json(profile);
          });
});

/*
  @route    POST /profile
  @desc     Create or update profile
  @access   Private
*/
router.post('/', passport.authenticate('jwt', {session: false}), function(req, res, next) {
  const { errors, isValid } = validateProfile(req.body);

  if(!isValid) return res.status(400).json(errors);

  const profileFields = { ...req.body, user: req.user.id };
  Profile.findOne({user: profileFields.user})
          .then( profile => {
            //update
            if (profile) {
              Profile.findOneAndUpdate({ user: profileFields.user },
                                    { $set: profileFields },
                                    { new: true,
                                      runValidators: true,
                                      context: 'query'
                                    })
                                    .then(profile => res.json(profile))
                                    .catch(e => {
                                      const errors = formateMongoError(e.errors);

                                      res.status(400).json(errors);
                                    })
            } else {
              //create
              new Profile(profileFields)
                                  .save()
                                  .then(profile => {
                                    User.findByIdAndUpdate(
                                      profileFields.user,
                                      { $set: { profileStatus: true }},
                                      { new: true }
                                    )
                                    .catch(e => next(e));
                                    profile.populate('user', 'avatar')
                                           .execPopulate()
                                           .then(profile => res.json(profile))

                                  })
                                  .catch(e => {
                                    const errors = formateMongoError(e.errors);

                                    res.status(400).json(errors);
                                  })
            }
          })
          .catch(e => next(e));
});

/*
  @route    POST /profile/personal
  @desc     Update personal
  @access   Private
*/
router.post('/personal', passport.authenticate('jwt', {session: false}),
(req, res, next) => {
  const { errors, isValid } = validatePersonal(req.body);

  if(!isValid) return res.status(400).json(errors);

  Profile.findOne({user: req.user.id}).then( profile => {
    if(!profile){
      errors.experiance = "The profile doesnt exists";
      res.status(404).json(errors);
    }

    Profile.findOneAndUpdate( { user: req.user.id },
                              { $set: {
                                  personal: req.body.personal
                                }
                              },
                              { new: true,
                                runValidators: true,
                                upsert: true,
                                context: 'query'
                              }
    )
    .populate('user', 'avatar')
    .then( profile => res.json(profile))
    .catch( e => {
      const errors = formateMongoError(e.errors) || e;

      res.status(400).json(errors);
    });
  })
  .catch( e => next(e));
});

/*
  @route    POST /profile/additionalInfo
  @desc     Update Additional information
  @access   Private
*/
router.post('/additionalInfo', passport.authenticate('jwt', {session: false}),
(req, res, next) => {
  const { errors, isValid } = validateAdditionalInfo(req.body);

  if(!isValid) return res.status(400).json(errors);

  Profile.findOne({user: req.user.id}).then( profile => {
    if(!profile){
      errors.experiance = "The profile doesnt exists";
      res.status(404).json(errors);
    }

    Profile.findOneAndUpdate( { user: req.user.id },
                              { $set: {
                                  handle: req.body.handle,
                                  account_type: req.body.account_type
                                }
                              },
                              { new: true,
                                runValidators: true,
                                upsert: true,
                                context: 'query'
                              }
    )
    .populate('user', 'avatar')
    .then( profile => res.json(profile))
    .catch( e => {
      const errors = formateMongoError(e.errors) || e;

      res.status(400).json(errors);
    });
  })
  .catch( e => next(e));
});

/*
  @route    POST /profile/specialization
  @desc     Update specialization
  @access   Private
*/
router.post('/specialization', passport.authenticate('jwt', {session: false}),
(req, res, next) => {
  const { errors, isValid } = validateSpecialization(req.body);

  if(!isValid) return res.status(400).json(errors);

  Profile.findOne({user: req.user.id}).then( profile => {
    if(!profile){
      errors.experiance = "The profile doesnt exists";
      res.status(404).json(errors);
    }

    Profile.findOneAndUpdate( { user: req.user.id },
                              { $set: {
                                  specialization: req.body.specialization
                                }
                              },
                              { new: true,
                                runValidators: true,
                                upsert: true,
                                context: 'query'
                              }
    )
    .populate('user', 'avatar')
    .then( profile => res.json(profile))
    .catch( e => {
      const errors = formateMongoError(e.errors) || e;

      res.status(400).json(errors);
    });
  })
  .catch( e => next(e));
});

/*
  @route    POST /profile/experiance
  @desc     Update experiance
  @access   Private
*/
router.post('/experiance',
            passport.authenticate('jwt', {session: false}),
            (req, res, next) => {

              const { errors, isValid } = validateExperiance(req.body);

              if(!isValid) return res.status(400).json(errors);

              Profile.findOne({user: req.user.id}).then( profile => {
                if(!profile){
                  errors.experiance = "The profile doesnt exists";
                  res.status(404).json(errors);
                }

                Profile.findOneAndUpdate( { user: req.user.id },
                                          { $set: {
                                              experiance: req.body.experiance
                                            }
                                          },
                                          { new: true,
                                            runValidators: true,
                                            upsert: true,
                                            context: 'query'
                                          }
                )
                .populate('user', 'avatar')
                .then( profile => res.json(profile))
                .catch( e => {
                  const errors = formateMongoError(e.errors) || e;

                  res.status(400).json(errors);
                });
              })
              .catch( e => next(e));
            });

/*
  @route    POST /profile/education
  @desc     Update education
  @access   Private
*/
router.post('/education',
            passport.authenticate('jwt', {session: false}),
            (req, res, next) => {
              const { errors, isValid } = validateEducation(req.body);

              if(!isValid) return res.status(400).json(errors);

              Profile.findOne({user: req.user.id}).then( profile => {
                if(!profile){
                  errors.education = "The profile doesnt exists";
                  res.status(404).json(errors);
                }

                Profile.findOneAndUpdate( { user: req.user.id },
                                          { $set: {
                                              education: req.body.education
                                            }
                                          },
                                          { new: true,
                                            runValidators: true,
                                            upsert: true
                                          }
                )
                .populate('user', 'avatar')
                .then( profile => res.json(profile))
                .catch( e => {
                  const errors = formateMongoError(e.errors) || e;

                  res.status(400).json(errors);
                });
              })
              .catch( e => next(e));
            });

/*
  @route    POST /profile/contacts
  @desc     Update Contacts
  @access   Private
*/
router.post('/contacts', passport.authenticate('jwt', {session: false}),
(req, res, next) => {

  Profile.findOne({user: req.user.id}).then( profile => {
    if(!profile){
      errors.experiance = "The profile doesnt exists";
      res.status(404).json(errors);
    }

    Profile.findOneAndUpdate( { user: req.user.id },
                              { $set: {
                                  contacts: req.body.contacts
                                }
                              },
                              { new: true,
                                runValidators: true,
                                upsert: true,
                                context: 'query'
                              }
    )
    .populate('user', 'avatar')
    .then( profile => res.json(profile))
    .catch( e => {
      const errors = formateMongoError(e.errors) || e;

      res.status(400).json(errors);
    });
  })
  .catch( e => next(e));
});

/*
  @route    GET /profile/:handle
  @desc     Get profile by handle
  @access   Public
*/
router.get('/handle/:handle', (req, res, next) => {
  Profile.findOne({handle: req.params.handle})
          .populate('user', 'avatar')
          .then( profile => {
            const errors ={};
            if (!profile) {
              errors.handle = "There is no profile for this handle";
              return res.status(400).json(errors);
            }
            res.json(profile);
          })
          .catch(e => next(e));
});

/*
  @route:   GET /profile/:id
  @desc     Get profile by user id
  @access   Public
*/
router.get('/user/:user_id', (req, res, next) => {
    Profile.findOne({ user: req.params.user_id })
            .populate('user', 'avatar')
            .then( profile => {
              const errors ={};
              if (!profile) {
                errors.user_id = "There is no profile for this id";
                return res.status(400).json(errors);
              }
              res.json(profile);
            })
            .catch(e => next(e));
});

/*
  @route:   DELETE /profile/experiance/:experiance_id
  @desc     Delete experiance
  @access   Private
*/
router.delete('/experiance/:exp_id',
            passport.authenticate('jwt', {session: false}),
            (req, res, next) => {
              const errors ={};

              Profile.findOne({user: req.user.id})
                      .then( profile => {
                        if (!profile) {
                          errors.handle = "There is no profile for this user";
                          return res.status(400).json(errors);
                        }

                        if(profile.experiance.length === 0){
                          errors.experiance = "The profile hasn't experiance fields";
                          return res.status(404).json(errors)
                        }

                        const indexForRemove = profile.experiance.findIndex(
                          (exp) => exp.id === req.params.exp_id
                        );

                        profile.experiance.splice(indexForRemove, 1);

                        profile.save().then(profile => res.json(profile))
                                      .catch(e => next(e));

                      })
                      .catch(e => next(e));
});

/*
  @route:   DELETE /profile/education/:education_id
  @desc     Get profile by user id
  @access   Private
*/
router.delete('/education/:edu_id',
            passport.authenticate('jwt', {session: false}),
            (req, res, next) => {
              const errors ={};
              Profile.findOne({user: req.user.id})
                      .then( profile => {
                        if (!profile) {
                          errors.handle = "There is no profile for this user";
                          return res.status(400).json(errors);
                        }

                        if(profile.education.length === 0){
                          errors.education = "The profile hasn't education fields";
                          return res.status(404).json(errors)
                        }

                        const indexForRemove = profile.education.findIndex(
                          (education) => education.id === req.params.edu_id
                        );

                        profile.education.splice(indexForRemove, 1);

                        profile.save().then(profile => res.json(profile))
                                      .catch(e => next(e));

                      })
                      .catch(e => next(e));
});

/*
  @route    DELETE /profile/destroy
  @desc     delete profile
  @access   Private
*/
router.delete('/destroy', passport.authenticate('jwt', {session: false}),
              (req, res, next) => {
  Profile.deleteOne({ user: req.user.id })
          .then(() => res.json("ok"))
          .catch(e => res.status(404).json(e))
});

/*
  @route    GET /profile/all
  @desc     get all profiles
  @access   Private
*/
router.get('/all', passport.authenticate('jwt', {session: false}),
  (req, res, next) => {
    Profile.find().populate('user', 'avatar')
                  .then(profiles => {
                    if (!profiles) {
                      const error = { profiles: {massage: 'Profiles do not exist yet'}}
                      res.status(400).json(error);
                    }

                    res.json(profiles);
                  })
                  .catch(e => res.status(404).json(e));
});

router.get('/check-handle',
            passport.authenticate('jwt', {session: false}),
            (req, res, next) => {
  const handle = req.query.handle;

  Profile.findOne({ handle })
          .then( profile => {
            if(profile && profile.user._id.toString() !== req.user.id) {
              return res.send({ isFree: false })
            }
            res.send({ isFree: true })
          })
          .catch(e => next(e));
});

module.exports = router;
