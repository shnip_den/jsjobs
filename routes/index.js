module.exports = function(app){
  app.use('/api/users', require('./users'));
  app.use('/', require('./auth'));
  app.use('/api/profile', require('./profile'));
  app.use('/conversation', require('./conversation'));
  app.use('/message', require('./message'));
  app.use('/job', require('./job'));
}
