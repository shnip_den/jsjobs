const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');
const Job = require('../models/job');

/*
  @route    POST /job
  @desc     create job
  @access   Private
*/
router.post('/', passport.authenticate('jwt', {session: false}),
(req, res) => {
  const fields = { ...req.body, user: req.user.id }

  Job.create(fields).then(job => res.json(job))
                      .catch(err => res.status(400).json(err))
});

/*
  @route    PATCH /job
  @desc     update job
  @access   Private
*/
router.patch('/', passport.authenticate('jwt', {session: false}),
(req, res) => {
  const { _id, ...rest } = req.body;

  if (!_id) return res.status(400).json({message: "Id is missing"});

  Job.findOneAndUpdate(_id,
                        { ...rest, updated_at: new Date() },
                        { new: true }).then(job => res.json(job))
                                      .catch(err => res.status(400).json(err))
});

/*
  @route    GET /job/user/:profile_id
  @desc     get all jobs by user id
  @access   Private
*/
router.get('/user/:profile_id', passport.authenticate('jwt', {session: false}),
(req, res) => {
  Job.find({ profile: req.params.profile_id }).populate('user', 'avatar')
                                 .then(jobs => res.json(jobs))
                                 .catch(err => res.status(400).json(err))
});

/*
  @route    GET /job/all
  @desc     get all jobs
  @access   Private
*/
router.get('/all', passport.authenticate('jwt', {session: false}),
(req, res) => {
  Job.find().populate('user', 'avatar')
            .then(jobs => res.json(jobs))
            .catch(err => res.status(400).json(err))
});

/*
  @route    GET /job/:id
  @desc     get job by user id
  @access   Private
*/
router.get('/:id', passport.authenticate('jwt', {session: false}),
(req, res) => {
  Job.findOne({ _id: req.params.id }).populate(
                                      { path: 'applications.profile',
                                        model: 'profile',
                                        select: [
                                          'personal.firstname',
                                          'personal.lastname',
                                          'personal.location',
                                          'handle',
                                          'specialization.skills',
                                          'user'
                                        ],
                                        populate: {
                                          path: 'user',
                                          model: 'users',
                                          select: 'avatar'
                                        } })
                                     .then(job => res.json(job))
                                     .catch(err => res.status(400).json(err))
});

/*
  @route    DELETE /job/:id
  @desc     delete job by user id
  @access   Private
*/
router.delete('/:id', passport.authenticate('jwt', {session: false}),
(req, res) => {
  Job.deleteOne({ _id: req.params.id })
                                   .then(job => res.json("ok"))
                                   .catch(err => res.status(400).json(err))
});

/*
  @route    POST /job/applications
  @desc     create application
  @access   Private
*/
router.post('/applications', passport.authenticate('jwt', {session: false}),
(req, res) => {
  const { _id, profile_id } = req.body;

  Job.findByIdAndUpdate({ _id },
                      {
                        $push: {
                          applications: { profile: profile_id }
                        }
                      },
                      { new: true }
                      )
                      .populate({ path: 'applications.profile',
                                  model: 'profile',
                                  select: [
                                    'personal.firstname',
                                    'personal.lastname',
                                    'personal.location',
                                    'handle',
                                    'specialization.skills',
                                    'user'
                                  ]})
                      .then(job => res.json(job))
                      .catch(err => res.status(400).json(err))
});

/*
  @route    PATCH /job/applications
  @desc     patch or clear application
  @access   Private
*/
router.patch('/applications', passport.authenticate('jwt', {session: false}),
(req, res) => {
  const { job_id, app_id } = req.body;

  Job.findById(job_id).then(job => {
    const filtred = job.applications.filter(item => {
      return item._id+'' !== app_id;
    });

    job.applications = filtred;
    job.save().then(job => res.json(job))
  })
  .catch(err => res.status(400).json(err))
});

/*
  @route    GET /job/applications
  @desc     get user applications
  @access   Private
*/
router.get('/applications/:profile_id', passport.authenticate('jwt', {session: false}),
(req, res) => {
  const { profile_id } = req.params;

  Job.find({ "applications.profile": { $in: profile_id } })
                                      .then(job => res.json(job))
                                      .catch(err => res.status(400).json(err))

});

module.exports = router;
