const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');
const Message = require('../models/message').Message;

/* it is not used yet */
router.post('/message', passport.authenticate('jwt', {session: false}),
(req, res) => {
  new Message(req.body).save()
               .then(message => res.json(message))
               .catch(e => res.status(404).json(e))
});

module.exports = router;
