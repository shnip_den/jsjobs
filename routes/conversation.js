const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');
const Conversation = require('../models/conversation');


/*
  @route    POST /conversation
  @desc     create conversation
  @access   Private
*/
router.post('/', passport.authenticate('jwt', {session: false}),
(req, res) => {

  Conversation.create(req.body)
              .then(conv => {
                conv
                .populate({ path: 'sender',
                            model: 'profile',
                            select: [
                              'personal.firstname',
                              'personal.lastname',
                              'user'
                            ],
                            populate: {
                              path: 'user',
                              model: 'users',
                              select: 'avatar'
                            } })
                .populate({ path: 'recipient',
                            model: 'profile',
                            select: [
                              'personal.firstname',
                              'personal.lastname',
                              'user'
                            ],
                            populate: {
                              path: 'user',
                              model: 'users',
                              select: 'avatar'
                            } })
                .execPopulate()
                .then(populated => {
                  res.json(populated);
                })
              })
              .catch(e => res.send(e));
});

/*
  @route    GET /conversation/all?id=1
  @desc     get all conversations
  @access   Private
*/
router.get('/all', passport.authenticate('jwt', {session: false}), (req, res) => {
  const id = req.query.id;

  Conversation.find({ $or : [{ sender: id }, { recipient: id  }] })
              .populate({ path: 'sender',
                          model: 'profile',
                          select: [
                            'personal.firstname',
                            'personal.lastname',
                            'user'
                          ],
                          populate: {
                            path: 'user',
                            model: 'users',
                            select: 'avatar'
                          } })
              .populate({ path: 'recipient',
                          model: 'profile',
                          select: [
                            'personal.firstname',
                            'personal.lastname',
                            'user'
                          ],
                          populate: {
                            path: 'user',
                            model: 'users',
                            select: 'avatar'
                          } })
              .then(conversations => {
                res.send(conversations);
              })
              .catch(e => {
                res.status(400).json(e);
              });
});

/*
  @route    POST /conversation/message
  @desc     Put message to conversation
  @access   Private
*/
router.post('/message', passport.authenticate('jwt', {session: false}),
(req, res) => {
  const { id, message } = req.body;

  Conversation.findOneAndUpdate({_id: id},
                                { $push: {
                                    messages: message
                                  },
                                  updated_at: new Date()
                                },
                                { new: true})
                                .then(conversation => {
                                  if(!conversation) {
                                     res.status(404).json("Conversation not found")
                                  }
                                  res.json(conversation)
                                })
                                .catch(e => res.status(400).json(e));
});

/*
  @route    POST /conversation/messages/clear
  @desc     Delete all messages
  @access   Private
*/
router.post('/messages/clear', passport.authenticate('jwt', {session: false}),
(req, res) => {
  const { id } = req.body;

  Conversation.findOneAndUpdate({_id: id},
                                { $set: {
                                    messages: []
                                  }
                                },
                                { new: true})
                                .then(conversation => {
                                  if(!conversation) {
                                     res.status(404).json("Conversation not found")
                                  }
                                  res.json(conversation)
                                })
                                .catch(e => res.status(400).json(e));
});

module.exports = router;
