const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const MessageSchema = new Schema({
    firstname: {
      type: String,
      required: true
    },
    lastname: {
      type: String,
      required: true
    },
    avatar: {
      type: String,
      required: true
    },
    content: {
      type: String,
      required: true
    },
    profile_id: {
      type: Schema.Types.ObjectId,
      required: true
    },
    created_at: {
      type: Date,
      default: Date.now
    }
}, {
  id: true,
  versionKey: false
});

module.exports.MessageSchema = MessageSchema;
module.exports.Message = mongoose.model('Message', MessageSchema);
