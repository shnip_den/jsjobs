const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const MessageSchema = require('./message').MessageSchema;

const ConversationSchema = new Schema({
  sender: {
    type: Schema.Types.ObjectId,
    ref: 'profile',
    required: true,
    index: true
  },
  recipient: {
    type: Schema.Types.ObjectId,
    ref: 'profile',
    required: true,
    index: true
  },
  messages: [ MessageSchema ],
  updated_at: {
    type: Date,
    default: Date.now
  }
}, {
  id: true,
  versionKey: false
});

module.exports = Conversation = mongoose.model('Conversation', ConversationSchema);
