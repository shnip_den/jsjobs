const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');


const ProfileSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },

  personal: {
    firstname: {
      type: String,
      required: true
    },
    lastname: {
      type: String,
      required: true
    },
    gender: {
      type: String,
      enum: ['male', 'female']
    },
    birthday: {
      type: Date
    },
    location: {
      type: String
    },
    bio: {
      type: String
    },
    website: {
      type: String
    }
  },

  handle: {
    type: String,
    required: true,
    trim: true,
    max: 50,
    index: true,
    unique: true,
    uniqueCaseInsensitive: true
  },

  account_type: {
    type: String,
    enum: ['company', 'personal'],
    required: true
  },

  specialization: {
    type: {
      status: {
        type: String,
        required: true
      },
      skills: {
        type: [String]
      },
      qualification: {
        type: String,
        required: true
      },
      remoteJob: {
        type: Boolean
      },
      readyToRelocate: {
        type: Boolean
      }
    }
  },

  experiance: [
    {
      position: {
        type: String,
        required: true
      },
      company: {
        type: String,
        required: true
      },
      from: {
        type: Date,
        required: true
      },
      to: {
        type: Date
      }
    }
  ],

  education:  [
    {
      schoolTitle: {
        type: String,
        required: true
      },
      location: {
        type: String,
        required: true
      },
      from: {
        type: Date,
        required: true
      },
      to: {
        type: Date
      }
    }
  ],

  contacts: {
    phone: {
      type: String
    },
    email: {
      type: String
    },
    facebook: {
      type: String
    },
    twitter: {
      type: String
    },
    instagram: {
      type: String
    },
    github: {
      type: String
    }
  }
});

ProfileSchema.plugin(uniqueValidator, { message: `The {PATH} has already exists`});

module.exports = Profile = mongoose.model('profile', ProfileSchema);
