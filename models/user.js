const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcryptjs');

const UserSchema = new Schema({
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  avatar:{
    type: String,
  },
  // filled or not
  profileStatus: {
    type: Boolean,
    default: false
  },
  created: {
    type: Date,
    default: Date.now
  }
});

UserSchema.statics.encryptPassword = function(password){
  const hashedPassword = bcrypt.hashSync(password, 10);
  return hashedPassword;
}

module.exports = User = mongoose.model('users', UserSchema);
