const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const JobSchema = new Schema({
    title: {
      type: String,
      required: true
    },
    description: {
      type: String,
      required: true
    },
    skills: {
      type: [String]
    },
    qualification: {
      type: String
    },
    location: {
      type: String
    },
    user: {
      type: Schema.Types.ObjectId,
      ref: 'users'
    },
    profile: {
      type: Schema.Types.ObjectId,
      ref: 'profile'
    },
    applications: [
      {
        profile: {
          type: Schema.Types.ObjectId,
          ref: 'profile',
          required: true
        }
      }
    ],
    created_at: {
      type: Date,
      default: Date.now
    },
    updated_at: {
      type: Date,
      default: Date.now
    }
}, {
  id: true,
  versionKey: false
});

module.exports = Job = mongoose.model('Job', JobSchema);
