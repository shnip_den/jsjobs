const validator = require('validator');
const User = require('../models/user');

module.exports = function validateRegister(data){
  let errors = {};
  const { email = "", password = "", password_confirmation = ""} = data;
  // PASSWORD VALIDATIONS

    // password
  errors.password = {};

  if (!validator.isLength(password, {min: 5, max: 30})){
    errors.password.length = "Password must be between 5 and 30 characters";
  }

  if (!validator.equals(password, password_confirmation)){
    errors.password.match = "Password and password confirmation must match"
  }
  if (Object.keys(errors.password).length === 0) delete errors.password;

  //password_confirmation

  if (validator.isEmpty(password_confirmation, {ignore_whitespace: true})){
    errors.password_confirmation = { empty: "Password confirmation cant be empty" }
  }

  // EMAIL VALIDATIONS
  errors.email = {};

  if (!validator.isEmail(email)){
    errors.email.format = "Wrong email format"
  }

  if (!validator.isLength(email, {min: 4, max: 30})){
    errors.email.length = "Email must be between 4 and 30 characters"
  }

  if (Object.keys(errors.email).length === 0) {
    delete errors.email
  };

  return { errors, isValid: Object.keys(errors).length === 0 }
};
