const validatePersonal = require('./personal');
const validateAdditionalInfo = require('./additional-information');
const validateSpecialization = require('./specialization');
const validateExperiance = require('./experiance');
const validateEducation = require('./education');

module.exports.validateProfile = (data) => {
  let errors = {};

  Object.assign(errors,
                validatePersonal(data).errors,
                validatePersonal(data).errors,
                validateAdditionalInfo(data).errors,
                validateSpecialization(data).errors,
                validateExperiance(data).errors,
                validateEducation(data).errors
              )

  return { errors, isValid: Object.keys(errors).length === 0 }
}

/* I need to change the error format from Mongoose to the format of my validations. */
module.exports.formateMongoError = (e) => {
  return Object.keys(e).reduce((errors, error_type) => {
                                errors[error_type] = { message: e[error_type].message };
                                return errors;
                              }, {})
}
