const validator = require('validator');

module.exports = function validateSpecialization(data) {
  const { specialization = {} } = data;
  let errors = {};

  if (Object.keys(specialization).length > 0) {
    const { status = "", qualification = "" } = specialization;
    errors.specialization = {};

    if (validator.isEmpty(status, {ignore_whitespace: true})) {
      errors.specialization.status = { empty: "Status cant be empty" }
    }

    if (validator.isEmpty(qualification, {ignore_whitespace: true})) {
      errors.specialization.qualification = { empty: "Qualification cant be empty"}
    }

    if (Object.keys(errors.specialization).length === 0) {
      delete errors.specialization
    }
  } else {
    errors = { specialization: "Specialization cant be empty" }
  }

  return { errors, isValid: Object.keys(errors).length === 0 }
}
