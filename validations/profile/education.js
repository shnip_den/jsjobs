const validator = require('validator');

module.exports = function validateEducation(data){
  let errors = {};
  const { education = [] } = data;

  if (education.length > 0){
    errors.education = [];

    education.forEach(function(item, index, arr){
      let local_errors = {};
      let {schoolTitle = "", location = "", from = "" } = item;

      if (validator.isEmpty(schoolTitle, {ignore_whitespace: true})){
        local_errors.schoolTitle = {index: "Title cant be empty"}
      }

      if (validator.isEmpty(location, {ignore_whitespace: true})){
        local_errors.location = {index: "Location cant be empty"}
      }

      if (validator.isEmpty(from, {ignore_whitespace: true})){
        local_errors.from = {index: "From field cant be empty"}
      }

      if (Object.keys(local_errors).length > 0) {
        errors.education[index] = local_errors;
      }
    });

    if (errors.education.length === 0) delete errors.education;
  }

  return { errors, isValid: Object.keys(errors).length === 0 }
};
