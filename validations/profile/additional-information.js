const validator = require('validator');

module.exports = function validateAdditionalInfo(data) {
  const { handle = "", account_type = "" } = data;
  let errors = {};

  if(validator.isEmpty(handle, {ignore_whitespace: true})) {
    errors.handle = { empty: "Handle cant be empty" }
  }

  if(validator.isEmpty(account_type, {ignore_whitespace: true})) {
    errors.account_type = { empty: "Account  type cant be empty" }
  }

  return { errors, isValid: Object.keys(errors).length === 0 }
}
