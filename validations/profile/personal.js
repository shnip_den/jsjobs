const validator = require('validator');

module.exports = function validatePersonal(data) {
  let errors = {};
  const { personal = {} } = data;
  const { firstname = "", lastname = "" } = personal;
  errors.personal = {};

  if(validator.isEmpty(firstname, {ignore_whitespace: true})) {
    errors.personal.firstname = { empty: "Firstname cant be empty" };
  }

  if(validator.isEmpty(lastname, {ignore_whitespace: true})) {
    errors.personal.lastname = { empty: "Lastname cant be empty" };
  }

  if (Object.keys(errors.personal).length === 0) delete errors.personal;

  return { errors, isValid: Object.keys(errors).length === 0 }
};
