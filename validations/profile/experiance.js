const validator = require('validator');

module.exports = function validateExperiance(data){
  let errors = {};
  const { experiance = [] } = data;

  if (experiance.length > 0){
    errors.experiance = [];

    experiance.forEach(function(item, index, arr){
      let { position = "", company = "", from = "" } = item;
      let local_errors = {}

      if (validator.isEmpty(position, { ignore_whitespace: true })){
        local_errors.position = { empty: "Position cant be empty" }
      }

      if (validator.isEmpty(company, { ignore_whitespace: true })){
        local_errors.company = { empty: "Company cant be empty" }
      }

      if (validator.isEmpty(from, { ignore_whitespace: true })){
        local_errors.from =  { empty: "From field cant be empty" }
      }

      if (Object.keys(local_errors).length > 0) {
        errors.experiance[index] = local_errors;
      }
    });

    if(errors.experiance.length === 0) delete errors.experiance;
  }

  return { errors, isValid: Object.keys(errors).length === 0 }
};
