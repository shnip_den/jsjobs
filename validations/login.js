const validator = require('validator');

module.exports = function validateRegister(data){
  let errors = {};
  const {email = "", password = ""} = data;

  // PASSWORD VALIDATIONS
  if(!validator.isLength(password, {min: 5, max: 30})){
    errors.password = { required: "Password must be between 5 and 30 characters" }
  }

  // EMAIL VALIDATIONS
  if (!validator.isEmail(email)){
    errors.email = { format: "Wrong email format" }
  }

  if(validator.isEmpty(email, {ignore_whitespace: true})){
    errors.email = { required: "Email is required" }
  }

  return { errors, isValid: Object.keys(errors).length === 0 }
};
