const io = require('../server').io;
const Conversation = require('../models/conversation');
const Message = require('../models/message').Message;

//todo: Redis connectedUsers
const connectedUsers = new Map();

module.exports = (socket) => {

  socket.on('USER_CONNECTED', (profileId) => {
    connectedUsers.set(profileId, socket.id);
  });

  socket.on('USER_DISCONECTED', (profileId) => {
    connectedUsers.delete(profileId);
  });

  socket.on('disconnect', () => {
    console.log('User is Disconnected');
  });

  socket.on('SEND_PRIVATE_MESSAGE', (chatId, recipientId, message) => {
    Conversation.findOneAndUpdate({_id: chatId},
                                  { $push: {
                                      messages: message
                                    }
                                  },
                                  { new: true})
                                  .then(conversation => {
                                    if(!conversation) {
                                       const error = {
                                         message: "Conversation is not found"
                                       }
                                       socket.emit('MESSAGE_ERROR', error)
                                    }
                                    socket.emit('MESSAGE_SAVED')
                                  })
                                  .catch(e => {
                                    socket.emit('MESSAGE_ERROR', e)
                                  });
    const messageWithId = new Message(message);

    socket.emit('PRIVATE_MESSAGE', chatId, messageWithId)
    if (connectedUsers.has(recipientId)) {
      socket.to(connectedUsers.get(recipientId)).emit('PRIVATE_MESSAGE',chatId, messageWithId)
    }
  });
}
