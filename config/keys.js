if (process.env.NODE_ENV === 'production') {
  module.exports = {
    mongoURI: process.env.MONGO_URI,
    secret: process.env.SECRET
  }
} else {
  module.exports = require('./keys_private')
}
